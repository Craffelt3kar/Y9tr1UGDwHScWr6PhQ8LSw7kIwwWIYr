#!/usr/bin/env bash

if [ ! -f ./mkdocs_$1.yml ]
then
    echo "File mkdocs_$1.yml doesn't exist"
    exit 1
fi

# copy all shared elements to docs
cp -ru shared/* docs_$1/

# copy all shared snippets to snippets
cp -ru shared_snippets/* snippets_$1/

# concat YAML
cat mkdocs_shared.yml mkdocs_$1.yml > mkdocs.yml

# replace %%LANG%% placeholder
sed -i -e "s/%%LANG%%/${1}/g" mkdocs.yml

# build
mkdocs build --clean --strict
