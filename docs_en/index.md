---
description: Noble 3 is an Internal product that provides you with various features along with an intuitive GUI.
icon: material/information-box
---

# Introducing Noble 3 { #head }

<img src="img/noble3.png" alt="Noble 3 logo" class="large-padding center"/>

Noble 3 supports both Intel and AMD CPUs.

All versions of Windows 10, and 11 are supported.

## Notice { #notice }

* NobleShield feature only supports Windows 10 ≥ 20H2, Windows 11 ≤ 22H2.  
  Other Windows versions may cause problems such as BSOD during injection.

* Noble 3 is <span class="important-info">Internal</span> product.  
  Thus, all logic and codes are loaded into the game process memory.  
  Contrary to popular conception, however, it doesn't affect the detection rate much.

## Security features { #security-features }

* **Auto-spoofer**  
  Prevent from getting HWID-banned by spoofing some unique information that Overwatch collects.

* **NobleShield**  
  Prevent the Overwatch anti-cheat system from detecting Noble 3 and its traces using the kernel-mode driver.

* **Run Full Spoofer**  
  Delete all 'trace' files left by Battle.net and Overwatch, and change unique identifiers that can be used to HWID-ban you. (Such as Computer Name, Serial, etc.)

## Game features { #features }

* :material-target: Aimbot
  * Supports two keys: Head, Closest Bone
  * Supports various smoothing algorithms.
  * You can switch between Easy mode and Advanced mode in settings.
  * Aimbot FoV support
  * Object Aimbot support for in-game objects such as Symmetra turret, Torbjorn turret, etc.
  * Rage aim support (Client-side silent aim)
  * Track Aimbot, Flick Aimbot, Cast Aimbot support
  * Aimbot Visible Check, Shield(Barrier) Check
  * Supports target mode: Closest to the crosshair, Smart, Closest in Distance, Lowest Health

* :svgrepo-visuals: ESP
  * :material-human: Outline ESP
  * :material-human: Box ESP
  * :fontawesome-solid-chart-bar: Health-bar
  * :material-av-timer: Skill cooldowns, ultimate charge
  * :material-target: Aimbot target ESP
  * :material-palette: All ESP colors are configurable.

* :material-share: Easy to share the settings
  * :material-content-save: Global and Per-hero Settings are saved in the `config` folder in the same folder as NobleLoader.

* :svgrepo-fist-variant: Auto Melee
  * :material-target: Auto Melee has flick aimbot integrated

* :material-target-variant: Skill Assistance
  * <img src="img/hero/hero_dva.png" alt="" class="img-line"/>D. va Auto Defense Matrix
  * <img src="img/hero/hero_doomfist.png" alt="" class="img-line"/> Doomfist Rocket Punch
  * <img src="img/hero/hero_ramattra.png" alt="" class="img-line"/> Rammatra Staff, Pummel
  * <img src="img/hero/hero_reinhardt.png" alt="" class="img-line"/> Reinhardt Auto Barrier Field, Fire Strike
  * <img src="img/hero/hero_wreckingball.png" alt="" class="img-line"/> Wreckingball Auto Adaptive Shield
  * <img src="img/hero/hero_roadhog.png" alt="" class="img-line"/> Roadhog Chain Hook, Auto Breather
  * <img src="img/hero/hero_sigma.png" alt="" class="img-line"/> Sigma Accretion, Auto Kinetic Grasp
  * <img src="img/hero/hero_orisa.png" alt="" class="img-line"/> Orisa Energy Javlin, Auto Fortify, Auto Javelin Spin, Auto Terra Surge
  * <img src="img/hero/hero_winston.png" alt="" class="img-line"/> Winston Altfire
  * <img src="img/hero/hero_zarya.png" alt="" class="img-line"/> Zarya Auto Self Bubble, Auto Ally Bubble
  * <img src="img/hero/hero_junkerqueen.png" alt="" class="img-line"/> Junkerqueen Jagged Blade
  * <img src="img/hero/hero_ashe.png" alt="" class="img-line"/> Ashe Dynamite aim, Auto Coach Gun
  * <img src="img/hero/hero_cassidy.png" alt="" class="img-line"/> Cassidy Auto Combat Roll
  * <img src="img/hero/hero_echo.png" alt="" class="img-line"/> Echo Stick Bombs, Focusing Beam
  * <img src="img/hero/hero_genji.png" alt="" class="img-line"/> Genji Auto Dash, Auto Deflect, Dragonblade
  * <img src="img/hero/hero_junkrat.png" alt="" class="img-line"/> Junkrat Auto Concussion Mine
  * <img src="img/hero/hero_mei.png" alt="" class="img-line"/> Mei Shard, Auto Cryo-Freeze
  * <img src="img/hero/hero_reaper.png" alt="" class="img-line"/> Reaper Auto Wraith
  * <img src="img/hero/hero_sojourn.png" alt="" class="img-line"/> Sojourn Auto Altfire, (Manual) Altfire
  * <img src="img/hero/hero_soldier76.png" alt="" class="img-line"/> Soldier: 76 Helix Rocket, Auto Biotic Field
  * <img src="img/hero/hero_sombra.png" alt="" class="img-line"/> Sombra Auto Hack-Virus combo, Auto EMP
  * <img src="img/hero/hero_symmetra.png" alt="" class="img-line"/> Symmetra Altfire(Orb)
  * <img src="img/hero/hero_tracer.png" alt="" class="img-line"/> Tracer Auto Melee & Pulse Combo, Auto Recall
  * <img src="img/hero/hero_baptiste.png" alt="" class="img-line"/> Baptiste Auto Regenerative Burst
  * <img src="img/hero/hero_brigitte.png" alt="" class="img-line"/> Brigitte Auto Shield Bash, Auto Whip Shot
  * <img src="img/hero/hero_lucio.png" alt="" class="img-line"/> Lucio Auto Crossfade, Auto Amp
  * <img src="img/hero/hero_zenyatta.png" alt="" class="img-line"/> Zenyatta Orb Release, Auto Harmony Orb, Auto Discord Orb

## PR { #pr }

<img src="img/noble3-1.gif" alt="Noble PR 1" alt="" class="continuous-img"/>
<img src="img/noble3-2.gif" alt="Noble PR 2" alt="" class="continuous-img"/>
<img src="img/noble3-3.png" alt="Noble PR 3" alt="" class="continuous-img"/>

## Author of this manual { #manual-author }

by RS-232C (._.headl3ss)

You can freely redistribute, use, and modify (without any permission) under CC-BY-SA 4.0 license.

The source code of this manual is available at the top right of this page.

As I'm not a native English speaker and not too good at English, this document may contain some weird typos or grammar errors.
If you find any typos or errors, please contact me with Discord, or leave an Issue or Pull Request in GitLab.

## License { #license }

<a href="https://creativecommons.org/licenses/by-sa/4.0/deed.ko" target="_blank"><img src="img/200_by-sa.png" alt="CC-BY-SA 4.0"/></a>

---

Noble 3 Manual by RS-232C (._.headl3ss)

Noble 3 Manual is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <https://creativecommons.org/licenses/by-sa/4.0/>.
