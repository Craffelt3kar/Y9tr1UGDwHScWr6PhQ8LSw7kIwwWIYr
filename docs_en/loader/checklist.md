---
icon: material/check-all
---

# Before-use Checklist { #head }

Before downloading the Noble Loader and injecting Noble 3, check all those prerequisites are met.

These prerequisites are mandatory to stably load the Noble 3 and lower the chance of getting banned.

## :video_game: Is Battle.net and Overwatch fully closed? { #battlenet }

<p class="very-important-info">DO NOT run the Noble Loader or inject Noble 3 while Battle.net or Overwatch process is running.</p>

The Noble Loader is not designed to inject while the game is running.
You must launch Battle.net and the game AFTER the cmd window is closed.

If you try to inject while the game is running, the security features are not going to work as intended, causing your account banned in a high probability.

Open the Task Manager(1), then terminate all `Battle.net.exe`, `Agent.exe`, and `Overwatch.exe` processes.
{ .annotate }

1. Task Manager is can be opened with right-clicking the task bar and then press 'Task Manager', or `Ctrl+Shift+Esc` hotkey.

## :shield: Are all Anti-virus Real-time Protection features properly disabled? { #antivirus }

The Real-time Protection feature of various Anti-virus softwares can cause those problems:

* The Noble Loader archive download is blocked, saying 'there is a virus in this file.'
* The Noble Loader archive can't be unarchived or the exe file is deleted right after unarchival.
* The Noble Loader file is deleted when you try to execute it.
* The Noble Loader exits with error message.
* The Noble Loader runs without problem, but security features do not work.
* The Noble Loader fails to inject, or reports error while injecting.
* <p class="important-info">The Noble 3 is successfully injected to the game but security features are not work, thus resulting in a fast ban.</p>

## :shield: Did you removed all anti-cheat softwares from your computor? { #anticheat }

Anti-cheat softwares are going to definitely block the Noble Loader to run or inject.

Now the Noble Loader checks the anti-cheat software existence before injection. And automatically refuses to inject if any of these are found.

???+ success "Known anti-cheat softwares"
    * EasyAntiCheat (EAC)
    * BattlEye
    * FaceIT Client
    * ESEA
    * Vanguard (Can be removed by uninstalling Valorant)

If any of these software are installed on your computor, you should uninstall them and reboot first. (Use `appwiz.cpl`)

## :locked: Did you enabled Auto-spoofer, NobleShield options in Noble Loader before injection? { #security-features }

!!! info "Auto-spoofer, NobleShield options are now enabled by default."
    Disabling these two options results in a fast ban!

Those two features are security features, which is mandatory to not get detected and banned.

## :material-account-cancel: Did your HWID properly spoofed? { #spoofing }

**After an accoun ban, re-installation of Windows is strongly recommended.** (1)
{ .annotate }

If you are not able to re-install Windows, you should at least use 'Run Full Spoofer' feature in the Noble Loader.

As an account that is associated with your HWID is banned, there is a high chance that your HWID is flagged.
And if you change the account while this state, it may make your next account also flagged too.

<p class="important-info">Recently, they started to write unique identifiers directly to the hard disk instead of just leaving trace files. These identifiers can't be changed or removed without reinstalling Windows.</p>

Also, don't try to login to banned accounts from Battle.net App of website.

1. Windows reinstallation is the perfect way to expunge all left-over traces and flags on your computor.
