---
icon: material/download-box
---

# Installing Noble 3 { #head }

## Before-use Checklist { #checklist }

Before downloading the Noble Loader and injecting Noble 3, you should review the before-use checklist to properly and securely load the Noble 3.

<div class="center" markdown>
[:material-check-all: Before-use Checklist](checklist.md){ .md-button }
</div>

## Downlaod the Noble Loader { #download }

To load and use Noble 3 in the game, you need to download Noble Loader.

<div class="center" markdown>
[:material-file-download: Download Noble Loader](https://cdn.discordapp.com/attachments/1088998470364438628/1144599801627955251/Noble3_25082023.rar?ex=65e1c122&is=65cf4c22&hm=63fcd0d16cbe354b26cc27f7d45a4ed88ea9e885f5a211efb1c207a2c28e2d24&){ .md-button }
</div>

!!! hint "Noble Loader is automatically updated if the new version is available."
    You don't need to download the new loader file after updates.

## Unarchive the Noble Loader { #unarchive }

The Noble Loader file is archived with RAR format.

There are a lot of programs that support unarchiving RAR file. (e.g. 7-zip, WinRAR)

## Run the Noble Loader _AS ADMINISTRATOR_ { #run }

??? warning "Do not change the loader file name."
    If you change the Noble Loader file name, an error message will appear at startup.

If the new version of the loader is available, it will be automatically downloaded at this phase.

A cmd window will open and close in a second. If there were any errors during initialization, an error message may appear in this cmd window.

### Common startup error messages { #startup-error }

|Message|Cause|Resolution|
|:-:|:-:|:-:|
|`Already waiting for the game`|Injection is already finished. It's already waiting for the game to launch.|Launch the game <span class="important-info">after completely closing the Noble Loader window.</span>|
|`Multiple instance not allowed`|Another instance of Noble Loader is already running.|Retry after closing all other instances of the Noble Loader. (If there are no Noble Loader instances are found, try `taskkill -f -im NobleLoader.exe` to kill all loader instances)|
|`Invalid loader file name`|The loader file name is changed.|Change the file name back to `NobleLoader.exe`.|

If an error message not referred from here appears, please contact the seller or open a ticket on the official discord.

## Enter license key and login { #login }

The login window will open. Enter your license key and press the 'Login' button.

<figure><img src="../img/loader/loader_login.png" alt=""><figcaption><p>Login Window</p></figcaption></figure>

## Inject the Noble 3 { #inject }

The injector window will open. Select the 'Auto spoofer' and the 'Use NobleShield' checkboxes and click the 'Run Cheat' button.

<figure><img src="../img/loader/loader_load.png" alt=""><figcaption><p>Injector window</p></figcaption></figure>

## Injection in process { #inject-progress }

Another cmd window will open and close. It will report the injection steps.

<figure><img src="../img/loader/loader_injectcmd.png" alt=""><figcaption><p>Injection in progress</p></figcaption></figure>

After the cmd window is fully closed, you can start Battle.net and the game.

Sometimes after the `Download License...` log, it may show an error message such as `Server Error ...`.

Most of them are just internet connection errors. You can solve it by just retrying the injection.

But if the error remains after multiple retries, it may mean that your internet connection is unstable or the Noble server is down.
Check your internet connection and if it's okay, contact the seller or open a ticket on official discord.

<p class="very-important-info">You must launch Battle.net and the game AFTER the cmd window is closed.</p>

## Launch the game { #launch-game }

Noble 3 is automatically loaded when the game process is detected.

<figure><img src="../img/loader/loader_autoloaded.png" alt=""><figcaption><p>Noble 3 loaded</p></figcaption></figure>

???+ warning "module outdated. wait for update"
    This error message means that Noble 3 is not updated for the game version. (Example: Noble supports 803602, the game version 803699)

    In this case, you should wait until the Noble Team updates Noble 3. (Usually takes 6h ~ a day; Downtimes are compensated)
