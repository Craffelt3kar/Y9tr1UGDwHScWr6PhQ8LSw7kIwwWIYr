---
description: Aimbot Types - Cast Aimbot
icon: svgrepo/cast
tags:
    - Aimbot
    - Cast
---

# Cast Aimbot { #head }

-8<- "shared_aimbot_cast_animation.ext"

=== "English"
    <img src="../../../img/menu/menu_aimbot_adv_cast_en.png" alt="Aimbot Tab (Advanced mode, Cast Aimbot)" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_aimbot_adv_cast_ko.png" alt="Aimbot Tab (Advanced mode, Cast Aimbot)" class="center"/>

The only difference between [Flick Aimbot](flick.md) is that it has 'Smooth Cast' option added.

## Smooth Cast { #smooth-cast }

Adjust aim smooth to use before 'casting'.

Before locking(casting) aim to the target, aimbot uses this smoothing value to make the aim smoother.

## [Other Settings](index.md#advanced-settings)

## Showcase Video { #showcase }

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/FS5-7oQ9vhQ" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
