---
description: Aimbot Types - Flick Aimbot
icon: svgrepo/target_2
tags:
    - Aimbot
    - Flick
---

# Flick Aimbot { #head }

-8<- "shared_aimbot_flick_animation.ext"

=== "English"
    <img src="../../../img/menu/menu_aimbot_adv_flick_en.png" alt="Aimbot Tab (Advanced mode, Flick Aimbot)" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_aimbot_adv_flick_ko.png" alt="Aimbot Tab (Advanced mode, Flick Aimbot)" class="center"/>

### Smooth Mode { #smooth-mode }

Choose the smooth algorithm to use.

* Classic - Accelerate at the start, and decelerate when the aim is getting closer to the target.
* Fast - Basically the same as Classic mode, except it has a lesser deceleration rate.
* Modern - This mode provides a simple and accurate aim style.

!!! warning "In 'Modern' smooth mode, Smooth and Smooth Accelerate settings work _exactly in reverse_. (The higher the value is, the faster the aim goes)"

### Fast Shot { #fast-shot }

Reduce the delay between shots. (Aimbot will start aiming faster than normal after previous shots)

It is recommended to enable this option when playing heroes like Cassidy or Echo.

To help to understand:

On Disabled: After firing a shot, the aimbot automatically deactivates and waits for *the entire shot delay*.

On Enabled:  After firing a shot, the aimbot automatically deactivates and waits for the shot delay, but it *re-activates before the entire shot delay elapses*.

### Smooth { #smooth }

Base aim speed for the flick.

You can adjust the Base Aim Speed by this option.

The lower the value is, the faster the flick goes.
The higher the value is, the smoother the flick goes.

!!! warning "In 'Modern' smooth mode, it works in exactly the reverse manner: the HIGHER the value is, the FASTER the flick goes."

### Smooth Accelerate { #smooth-accelerate }

Acceleration and deceleration amount for the flick.

You can adjust the Aim acceleration and deceleration amount with this option.

The lower the value is, the lesser the acceleration and deceleration applied to the Base Aim Speed.
The higher the value is, the more the acceleration and deceleration are applied to the Base Aim Speed.

If this value is high enough, the aim will gradually accelerate and decelerate.
Conversely, if the value is low enough, the aim will start to move abruptly and lock to the target.

Also, this value more influences the aim 'Acceleration' than 'Deceleration.'
If this value is too low, the average aim speed totally decreases. (Because it couldn't get enough acceleration at the start and this continues till the end)

!!! warning "In 'Modern' smooth mode, it works in _exactly the reverse manner_: the HIGHER the value is, the MORE the acceleration and deceleration the aim gets."

### Smooth Balance { #smooth-balance }

!!! info "This option is not available on 'Modern' smooth mode."

Adjust the balance between [Smooth(the Base Aim Speed)](#smooth) and [Acceleration](#smooth-accelerate).

Let's say the aim is 100%. This option lets you adjust the ratio between Smooth and Acceleration.

The value N means '(100-N)% Smooth + N% Acceleration'
(Example: '25' means '75% Smooth + 25% Acceleration')

### Smooth Clamp { #clamp }

Maximum time to aim-lock the target.

Deactivates the aimbot after a certain time. (in milliseconds)

For example, the value 100 means 'Try to aim the target for 100ms; after 100ms of aim-locking, if the triggerbot still has not fired the trigger, deactivate the aimbot and fire the shot.'

This is an option to prevent the 'tracking' phenomenon, but it can be used to do many more things.

-8<- "aimbot_0_disable.ext"

<figure><img src="../../../img/showcase/showcase_smoothclamp_high.gif" alt="High clamp value showcase"><figcaption><p>High clamp value</p></figcaption></figure>

<figure><img src="../../../img/showcase/showcase_smoothclamp_low.gif" alt="Low clamp value showcase"><figcaption><p>Low clamp value</p></figcaption></figure>

## [Other Settings](index.md#advanced-settings)

## Showcase Videos { #showcase }

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/t87w3AV8xrU" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/HtWcI4sGYtU" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
