---
description: Explaining Aimbot tab.
icon: material/target
tags:
    - Aimbot
    - Triggerbot
    - AimAssist
---

# Aimbot Tab { #head }

!!! info "Wall Check, Invisible Sombra Check"
    Noble 3 DOES NOT aim or shoot invisible Sombra or enemies behind the walls.

    Exceptionally, Rage Aimbot does aim invisible Sombra.

## Common Settings { #common-settings }

Settings that both exist on Easy mode and Advanced mode.

=== "English"
    <img src="../../img/menu/menu_aimbot_easy_en.png" alt="Aimbot Tab (Easy mode)" class="center"/>

=== "Korean"
    <img src="../../img/menu/menu_aimbot_easy_ko.png" alt="Aimbot Tab (Easy mode)" class="center"/>

### Enabled { #enabled }

Enable or disable the aimbot feature.

All other settings will collapse if the aimbot is disabled.

### Advanced Mode  { #advanced-mode }

Switch between Easy mode and [Advanced mode](#advanced-settings).

From [Advanced mode](#advanced-settings), you can configure the aimbot more precisely with various settings.

### Head Key { #head-key }

The hotkey to aim enemy head.

When this key is pressed, the aimbot will find and aim at the head of the target enemy.

-8<-
aimbot_keybind.ext
aimbot_click2flick.ext
-8<-

### Nearest Key { #nearest-key }

The hotkey to aim the closest enemy bone.

When this key is pressed, the aimbot will find and aim at the closest bone of the target enemy.

-8<-
aimbot_keybind.ext
aimbot_click2flick.ext
-8<-

### FOV { #fov }

The FoV (Field-of-View) of the aimbot.

Only target enemies who are within a certain range of the crosshair.

To help understanding, you can think it draws a circle based on the crosshair.

<figure><img src="../../img/fov_explained.jpg" alt=""><figcaption><p>Example FoV Circle</p></figcaption></figure>

Aimbot only aims at the targets who are in this circle.

The FoV option adjusts the radius of this circle.

-8<- "aimbot_fov_disable.ext"

### Smooth { #smooth }

The smooth speed of the aimbot.

The **lower** the value is, the faster and more accurate the aimbot aims to target.

The **higher** the value is, the smooth the aimbot aims to target.

## Advanced mode { #advanced-settings }

=== "English"
    <img src="../../img/menu/menu_aimbot_adv_flick_en.png" alt="Aimbot Tab (Advanced mode)" class="center"/>

=== "Korean"
    <img src="../../img/menu/menu_aimbot_adv_flick_ko.png" alt="Aimbot Tab (Advanced mode)" class="center"/>

Noble 3's aimbot is distinguished into three types and each aimbot type has some differences between them.

<span class="important-info">This paragraph only explains settings that are all common(shared) in three different aimbot types.</span>

For the difference between the three types of aimbot, see the [Types of Aimbot](types.md) page.

### Target Mode { #target-mode }

The mode of the aimbot target selector.

* Crosshair - Target the enemy who is closest to the crosshair.

* Smart - Select the target cleverly by taking the distance between the crosshair and the distance between the player and the enemy into account.

* Distance - Target the enemy who is closest to the player.

* Health - Target the enemy who has the lowest health.

### Target Objects { #target-objects }

Enable or disable the object aimbot.

If enabled, aimbot will target in-game objects such as Symmetra Turrets, Torbjorn Turret, etc.

???+ info "List of the target objects"
    * <img src="../../../img/hero/hero_roadhog.png" alt="" class="img-line"/>Roadhog Pig Pen<img src="../../../img/hero/hero_roadhog_e.png" alt="" class="img-line"/>
    * <img src="../../../img/hero/hero_torbjorn.png" alt="" class="img-line"/>Torbjorn Deploy Turret<img src="../../../img/hero/hero_torbjorn_shift.png" alt="" class="img-line"/>
    * <img src="../../../img/hero/hero_symmetra.png" alt="" class="img-line"/>Symmetra Sentry Turret<img src="../../../img/hero/hero_symmetra_shift.png" alt="" class="img-line"/>, Teleporter<img src="../../../img/hero/hero_symmetra_e.png" alt="" class="img-line"/>
    * <img src="../../../img/hero/hero_widowmaker.png" alt="" class="img-line"/>Widowmaker Venom Mine<img src="../../../img/hero/hero_widowmaker_e.png" alt="" class="img-line"/>
    * <img src="../../../img/hero/hero_baptiste.png" alt="" class="img-line"/>Baptiste Immortality Field<img src="../../../img/hero/hero_baptiste_e.png" alt="" class="img-line"/>
    * <img src="../../../img/hero/hero_illari.png" alt="" class="img-line"/>Illari Healing Pylon<img src="../../../img/hero/hero_illari_e.png" alt="" class="img-line"/>

### Shield Check { #shield-check }

Enable or disable shield and barrier check.

If enabled, the aimbot will not shoot the enemies who are behind the shield or barrier.

### Dynamic FOV { #dynamic-fov }

Enable or disable Dynamic FOV feature.

If enabled, FoV will be dynamically changed according to the distance between the target and the player.
This means it will automatically increase FoV for close targets, and decrease it for distant targets.

It can be explained as 'Drawing FoV circles from each target instead of the crosshair.' And size of the circle is changed according to the distance between the enemy.

<img src="../../img/dynamic_fov.jpg" alt="" class="center"/>

### Vertical Fix { #vertical-fix }

The additional vertical correction of the aimbot target position.

The higher the value is, the upper position the aimbot will aim.

For example, to prevent your aimbot from shooting enemy heads, you can decrease this value to *-200*.

To increase head hit accuracy, you can increase the value to *50-80*.

If you're targetting some heroes that have vertically long head hitboxes such as Widowmaker, a higher value of *120-180* will make you always hit their heads when you press Head Key.

But if you increase this value too much, for certain heroes (Crouching Ana, Mei, etc.), the aimbot will lock the aim right above the target and won't shoot, which can be *EXTREMELY SUSPICIOUS*.

### Max Range { #max-range }

Maximum distance to the aimbot target.

Aimbot will not aim the enemies farther than this range. (in metre/meter)

For example, you can set this to '30' to prevent the aimbot from aiming the targets farther than 30m.

-8<- "aimbot_0_disable.ext"

## Notice { #notice }

As Noble 3 is an internal product, the smooth algorithm of the aimbot is affected by the game framerate.

For more information, see [Game Framerate and Aimbot Smooth](../../tips/aimbot-fps-affect.md) page.
