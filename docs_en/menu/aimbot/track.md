---
description: Aimbot Types - Track Aimbot
icon: material/magnet
tags:
    - Aimbot
    - Track
---

# Track Aimbot { #head }

-8<- "shared_aimbot_track_animation.ext"

=== "English"
    <img src="../../../img/menu/menu_aimbot_adv_track_en.png" alt="Aimbot Tab (Advanced mode, Track Aimbot)" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_aimbot_adv_track_ko.png" alt="Aimbot Tab (Advanced mode, Track Aimbot)" class="center"/>

## Spray Mode { #spray-mode }

Weapon spraying mode of the aimbot.

* Always - Automatically fire the weapon if the hotkey is pressed and the target is detected.

* OnAiming - Automatically fire the weapon while the aimbot is active.

* None - Do not automatically fire the weapon.

For most of the person who are bound aim-key to LBUTTON, this option is actually does nothing.

## Acquire Target Delay { #acquire-target-delay }

The delay between switching the aimbot target.

In case of the aimbot target is killed or gone, the delay to wait before switching to the other target.

For example, if you set the value to 1000, it will wait 1000ms before searching and aiming the next target after the current target killed.

-8<- "aimbot_0_disable.ext"

## [Other Settings](index.md#advanced-settings)

## Showcase Video { #showcase }

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/gt7IW7XcxJ0" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
