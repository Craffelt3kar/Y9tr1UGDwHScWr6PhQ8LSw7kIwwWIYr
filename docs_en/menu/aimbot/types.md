---
description: Noble 3 provides three different types of aimbot.
icon: material/exclamation-thick
tags:
    - Aimbot
    - Types
---

# Types of Aimbot { #head }

## [Track Aimbot](track.md) { #track }

-8<- "shared_aimbot_track_animation.ext"

The aimbot assists you to continuously 'track' the target.

It applies to heroes who need 'track' aim style: Soldier: 76, Tracer, Bastion, Sojourn, Sombra, and so on.

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/gt7IW7XcxJ0" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

## [Flick Aimbot (a.k.a. FlickBot)](flick.md) { #flick }

-8<- "shared_aimbot_flick_animation.ext"

The aimbot helps you to aim the target. And if the crosshair touches the target, it will automatically fire the shot.

It applies to heroes who need 'flick' aim style: Cassidy, Ashe, Widowmaker, Hanzo, Ana, and so on.

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/t87w3AV8xrU" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

## [Cast Aimbot](cast.md) { #cast }

-8<- "shared_aimbot_cast_animation.ext"

Track the target continuously. And when at some point, it will hard lock to the target then fire the shot. (Which is referred as "casting.")

It applies to heroes who shoot multiple shots on one click (Genji, Baptiste, etc.) or the delay exists between the click and shot (Mei, Ana Sleep Dart, Reinhardt Fire Strike, Zenyatta Orb Release, etc.)

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/FS5-7oQ9vhQ" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
