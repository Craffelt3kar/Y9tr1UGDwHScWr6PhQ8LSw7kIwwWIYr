---
pngicon: ../../../img/hero/hero_ana.png
---

# Ana { #head }

<img src="../../../img/hero/hero_ana.png" alt="" class="hero-portrait round-edge center"/>

## Aimbot { #aimbot }

In addition to [Aimbot Tab](../aimbot/flick.md) options, 'Scoped Config' option is added.

=== "English"
    <img src="../../../img/menu/menu_ana_aimbot_en.png" alt="" class="center"/>

=== "English (Scoped Config Enabled)"
    <img src="../../../img/menu/menu_ana_aimbot_scopedconfig_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_ana_aimbot_ko.png" alt="" class="center"/>

=== "Korean (Scoped Config Enabled)"
    <img src="../../../img/menu/menu_ana_aimbot_scopedconfig_ko.png" alt="" class="center"/>

-8<- "hero_scoped_config.ext"

-8<- "hero_tab_flick_aimbot.ext"

## SmartAna { #smartana }

Use single hotkey to heal up ally heroes and heal attack enemy heroes.

Like [Aimbot Tab](#aimbot), 'Scoped Config' option is added.

=== "English"
    <img src="../../../img/menu/menu_ana_smartana_en.png" alt="" class="center"/>

=== "English (Scoped Config Enabled)"
    <img src="../../../img/menu/menu_ana_smartana_scopedconfig_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_ana_smartana_ko.png" alt="" class="center"/>

=== "Korean (Scoped Config Enabled)"
    <img src="../../../img/menu/menu_ana_smartana_scopedconfig_ko.png" alt="" class="center"/>

-8<- "hero_scoped_config.ext"

-8<- "hero_tab_flick_aimbot.ext"

## \[Shift\] SleepDart <img src="../../../img/hero/hero_ana_shift.png" alt="" class="img-line-bigger"/> `#Aimbot(Cast)` { #shift }

Aim assist for Sleep Dart.

=== "English"
    <img src="../../../img/menu/menu_ana_sleepdart_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_ana_sleepdart_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[E\] BioticGrenade <img src="../../../img/hero/hero_ana_e.png" alt="" class="img-line-bigger"/> `#Aimbot(Flick)` { #e }

Aim assist for Biotic Grenade. (Ally and Enemy)

=== "English"
    <img src="../../../img/menu/menu_ana_bioticgrenade_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_ana_bioticgrenade_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"
