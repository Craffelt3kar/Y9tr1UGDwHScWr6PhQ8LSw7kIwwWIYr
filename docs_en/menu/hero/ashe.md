---
pngicon: ../../../img/hero/hero_ashe.png
---

# Ashe { #head }

<img src="../../../img/hero/hero_ashe.png" alt="" class="hero-portrait round-edge center" />

!!! note "The aimbot supports shooting own Dynamite <img src="../../../img/hero/hero_ashe_e.png" alt="" data-size="line"/>."

## Aimbot { #aimbot }

In addition to [Aimbot Tab](../aimbot/flick.md) options, 'Scoped Config' option is added.

=== "English"
    <img src="../../../img/menu/menu_ashe_aimbot_en.png" alt="" class="center"/>

=== "English (Scoped Config Enabled)"
    <img src="../../../img/menu/menu_ashe_aimbot_scopedconfig_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_ashe_aimbot_ko.png" alt="" class="center"/>

=== "Korean (Scoped Config Enabled)"
    <img src="../../../img/menu/menu_ashe_aimbot_scopedconfig_ko.png" alt="" class="center"/>

-8<- "hero_scoped_config.ext"

-8<- "hero_tab_flick_aimbot.ext"

## \[Shift\] CoachGun <img src="../../../img/hero/hero_ashe_shift.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Aimbot(Flick)` `#Evade` { #shift-auto }

-8<- "hero_skill_autouse.ext"

If the enemy approaches within a certain range, automatically aim that enemy and use Coach Gun to increase the distance.

=== "English"
    <img src="../../../img/menu/menu_ashe_coachgun_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_ashe_coachgun_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"
