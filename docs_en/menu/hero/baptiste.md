---
pngicon: ../../../img/hero/hero_baptiste.png
---

# Baptiste { #head }

<img src="../../../img/hero/hero_baptiste.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] BioticLauncher <img src="../../../img/hero/hero_baptiste_gun.png" alt="" class="img-line-bigger"/> `#Aimbot(Flick)` { #rb-auto }

-8<- "hero_skill_manualuse.ext"

Automatically heal up the ally using Biotic Launcher.

=== "English"
    <img src="../../../img/menu/menu_baptiste_bioticlauncher_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_baptiste_bioticlauncher_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

## \[Shift\] RegenerativeBurst <img src="../../../img/hero/hero_baptiste_shift.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Support` { #shift-auto }

-8<- "hero_skill_autouse.ext"

Automatically heal nearby ally heroes using Regenerative Burst when the health goes under the threshold for at least one of them.

=== "English"
    <img src="../../../img/menu/menu_baptiste_regenerativeburst_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_baptiste_regenerativeburst_ko.png" alt="" class="center"/>

### Health Percent

Player and ally health percentage threshold to use Regenerative Burst.

If the health of myself or any ally within a range goes below this percent, automatically use Regenerative Burst to heal them.
