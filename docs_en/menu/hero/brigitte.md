---
pngicon: ../../../img/hero/hero_brigitte.png
---

# Brigitte { #head }

<img src="../../../img/hero/hero_brigitte.png" alt="" class="hero-portrait round-edge center" />

## \[Shift\] WhipShot <img src="../../../img/hero/hero_brigitte_shift.png" alt="" class="img-line-bigger"/> `#Aimbot(Cast)` { #shift }

Aim assist for Whip Shot.

=== "English"
    <img src="../../../img/menu/menu_brigitte_whipshot_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_brigitte_whipshot_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[Shift\] WhipShotAutoKill <img src="../../../img/hero/hero_brigitte_shift.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Aimbot(Cast)` `#KillSteal` { #shift-auto }

-8<- "hero_skill_autouse.ext"

Automatically eliminate low-health enemies with Whip Shot.

=== "English"
    <img src="../../../img/menu/menu_brigitte_whipshotautokill_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_brigitte_whipshotautokill_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[LB\] ShieldBash <img src="../../../img/hero/hero_brigitte_rmb_lmb.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Aimbot(Flick)` `#Counter` { #lb-auto }

!!! info "This feature works when the Shield is up and the enemy approaches a certain range."

While holding the Shield, if the enemy approaches a certain range, automatically aim the enemy and use the Shield Bash.

=== "English"
    <img src="../../../img/menu/menu_brigitte_shieldbash_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_brigitte_shieldbash_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

## \[E\] RepairPack <img src="../../../img/hero/hero_brigitte_e.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Aimbot(Flick)` `#Support` { #e-auto }

-8<- "hero_skill_autouse.ext"

Automatically throw a Repair Pack to the low-health ally.

=== "English"
    <img src="../../../img/menu/menu_brigitte_repairpack_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_brigitte_repairpack_ko.png" alt="" class="center"/>

### Max Health Percent

Ally health percentage threshold to give them Repair Pack.

If the health of the ally goes below this percent, automatically throw a Repair Pack to heal them.

-8<- "hero_tab_flick_aimbot.ext"
