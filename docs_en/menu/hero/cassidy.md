---
pngicon: ../../../img/hero/hero_cassidy.png
---

# Cassidy { #head }

<img src="../../../img/hero/hero_cassidy.png" alt="" class="hero-portrait round-edge center" />

## \[Shift\] CombatRoll <img src="../../../img/hero/hero_cassidy_shift.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Combo` { #shift-auto }

-8<- "hero_skill_autouse.ext"

Automatically reload with Combat Roll when the weapon is out-of-ammo.

=== "English"
    <img src="../../../img/menu/menu_cassidy_combatroll_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_cassidy_combatroll_ko.png" alt="" class="center"/>
