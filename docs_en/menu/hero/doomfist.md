---
pngicon: ../../../img/hero/hero_doomfist.png
---

# Doomfist { #head }

<img src="../../../img/hero/hero_doomfist.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] RocketPunch <img src="../../../img/hero/hero_doomfist_rmb.png" alt="" class="img-line-bigger"/> `#Aimbot(Flick)` { #rb }

Aim assist for Rocket Punch.

=== "English"
    <img src="../../../img/menu/menu_doomfist_rocketpunch_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_doomfist_rocketpunch_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"
