---
pngicon: ../../../img/hero/hero_dva.png
---

# D. Va { #head }

<img src="../../../img/hero/hero_dva.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] DefenseMatrix <img src="../../../img/hero/hero_dva_rmb.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Aimbot(Track)` `#Counter` { #rb-auto }

-8<- "hero_skill_autouse.ext"

???+ success "Automatically counter the following skills"
    * [x] <img src="../../../img/hero/hero_zarya.png" alt="" class="img-line-bigger round-edge"/>Zarya Graviton Surge<img src="../../../img/hero/hero_zarya_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_mei.png" alt="" class="img-line-bigger round-edge"/>Mei Blizzard<img src="../../../img/hero/hero_mei_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_cassidy.png" alt="" class="img-line-bigger round-edge"/>Cassidy Magnetic Grenade<img src="../../../img/hero/hero_cassidy_e.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_tracer.png" alt="" class="img-line-bigger round-edge"/>Tracer Pulse Bomb<img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_hanzo.png" alt="" class="img-line-bigger round-edge"/>Hanzo Dragonstrike<img src="../../../img/hero/hero_hanzo_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>Ana Sleep Dart<img src="../../../img/hero/hero_ana_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>Ana Biotic Grenade<img src="../../../img/hero/hero_ana_e.png" alt="" class="img-line-bigger"/>

Automatically eat up threatening skills and ultimates by automatically using the Defense Matrix.

=== "English"
    <img src="../../../img/menu/menu_dva_defensematrix_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_dva_defensematrix_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=0" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

## \[E\] MicroMissiles <img src="../../../img/hero/hero_dva_e.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Combo` { #e-auto }

!!! info "This feature automatically works when you attack the enemy."

Automatically fire micro missiles while aiming the enemy using the hotkeys.

=== "English"
    <img src="../../../img/menu/menu_dva_micromissiles_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_dva_micromissiles_ko.png" alt="" class="center"/>
