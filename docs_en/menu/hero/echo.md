---
pngicon: ../../../img/hero/hero_echo.png
---

# Echo { #head }

<img src="../../../img/hero/hero_echo.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] StickyBombs <img src="../../../img/hero/hero_echo_rmb.png" alt="" class="img-line-bigger"/> `#Aimbot(Cast)` { #rb }

Aim assist for Sticky Bombs.

=== "English"
    <img src="../../../img/menu/menu_echo_stickybombs_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_echo_stickybombs_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[E\] FocusingBeam <img src="../../../img/hero/hero_echo_e.png" alt="" class="img-line-bigger"/> `#Aimbot(Track)` { #e }

Aim assist for Focusing Beam.

=== "English"
    <img src="../../../img/menu/menu_echo_focusingbeam_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_echo_focusingbeam_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"
