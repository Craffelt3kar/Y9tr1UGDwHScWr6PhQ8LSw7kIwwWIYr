---
pngicon: ../../../img/hero/hero_genji.png
---

# Genji { #head }

<img src="../../../img/hero/hero_genji.png" alt="" class="hero-portrait round-edge center" />

## \[Shift\] SwiftStrike <img src="../../../img/hero/hero_genji_shift.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Aimbot(Flick)` `#KillSteal` { #shift-auto }

-8<- "hero_skill_autouse.ext"

Automatically eliminate low-health enemies within a certain range with Swift Strike. (a.k.a. AutoDash)

=== "English"
    <img src="../../../img/menu/menu_genji_swiftstrike_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_genji_swiftstrike_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

## \[E\] Deflect <img src="../../../img/hero/hero_genji_e.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Aimbot(Flick)` `#Counter` { #e-auto }

-8<- "hero_skill_autouse.ext"

???+ success "Automatically counter the following skills"
    * [x] <img src="../../../img/hero/hero_roadhog.png" alt="" class="img-line-bigger round-edge"/>Roadhog Chain Hook<img src="../../../img/hero/hero_roadhog_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_tracer.png" alt="" class="img-line-bigger round-edge"/>Tracer Pulse Bomb<img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_cassidy.png" alt="" class="img-line-bigger round-edge"/>Cassidy Magnetic Grenade<img src="../../../img/hero/hero_cassidy_e.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_soldier76.png" alt="" class="img-line-bigger round-edge"/>Soldier: 76 Helix Rocket<img src="../../../img/hero/hero_soldier76_rmb.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>Ana Sleep Dart<img src="../../../img/hero/hero_ana_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>Ana Biotic Grenade<img src="../../../img/hero/hero_ana_e.png" alt="" class="img-line-bigger"/>

Automatically deflect threatening skills or ultimates.

=== "English"
    <img src="../../../img/menu/menu_genji_deflect_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_genji_deflect_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=65" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

## \[Ult\] Dragonblade <img src="../../../img/hero/hero_genji_q.png" alt="" class="img-line-bigger"/> `#Aimbot(Cast)` { #ult }

-8<- "hero_skill_manualuse.ext"

Aim assist for Dragonblade. (automatically aim and attack nearby enemies)

=== "English"
    <img src="../../../img/menu/menu_genji_dragonblade_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_genji_dragonblade_ko.png" alt="" class="center"/>

### Use hp threshold instead of killable check { #hp-threshold }

Off: Prioritize 'killable' targets. (Targets that can be killed with a single slice)

On: Prioritize the enemy hero with the lowest health.

-8<- "hero_tab_cast_aimbot.ext"

## AimAssist { #aimassist }

Assist the user in aiming at the target after using Swift Strike to them.

=== "English"
    <img src="../../../img/menu/menu_genji_aimassist_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_genji_aimassist_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/ojhMutysrPw" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
