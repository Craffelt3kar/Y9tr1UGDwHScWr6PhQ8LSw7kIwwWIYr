---
pngicon: ../../../img/hero/hero_illari.png
---

# Illari { #head }

<img src="../../../img/hero/hero_illari.png" alt="" class="hero-portrait round-edge center" />

## \[AltFire\] AltFire <img src="../../../img/hero/hero_illari_gun.png" alt="" class="img-line-bigger"/> `#Aimbot(Track)` { #rb }

Aim assist for altfire. (healing beam)

=== "English"
    <img src="../../../img/menu/menu_illari_altfire_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_illari_altfire_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"
