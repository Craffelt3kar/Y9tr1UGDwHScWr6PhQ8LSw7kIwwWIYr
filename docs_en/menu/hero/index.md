---
description: Hero skill assistance
icon: svgrepo/gears
---

# Skill Assistance { #head }

Assists or automates the use of skills(`Shift`, `E`) and ultimates on heroes.

Noble 3 supports a variety of skill assistances, which can be categorized as:

1. Aimbot Category (If the assist doesn't have an aimbot integrated, this tag will be omitted.)
  * `#Aimbot(Track)` - [Track Aimbot](../aimbot/track.md) is included
  * `#Aimbot(Flick)` - [Flick Aimbot](../aimbot/flick.md) is included
  * `#Aimbot(Cast)` - [Cast Aimbot](../aimbot/cast.md) is included

2. Auto-skill Category (`#AutoSkill`) - Automatically use the skill in certain situations. (If the feature is just skill assist and not auto-skill, this tag will be omitted.)
  * `#Counter` - Automatically counters threatening skills or ultimates.
  * `#Evade` - Automatically evades threatening skills or ultimates.
  * `#Support` - Automatically supports the ally in danger.
  * `#Survive` - Automatically prevents from dying by using the skill if the health is below some level.
  * `#KillSteal` - Automatically eliminates the low-health enemies.
    * Mostly works while the primary aimbot hotkey is pressed.
  * `#Combo` - Automatically uses a series of skill combos. (Example: Sombra Hack-Virus Combo)
    * Mostly works while the primary aimbot hotkey is pressed.
