---
pngicon: ../../../img/hero/hero_junkerqueen.png
---

# Junkerqueen { #head }

<img src="../../../img/hero/hero_junkerqueen.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] JaggedBlade <img src="../../../img/hero/hero_junkerqueen_rmb.png" alt="" class="img-line-bigger"/> `#Aimbot(Cast)` { #rb }

Aim assist for Jagged Blade.

=== "English"
    <img src="../../../img/menu/menu_junkerqueen_jaggedblade_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_junkerqueen_jaggedblade_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"
