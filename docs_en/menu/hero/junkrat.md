---
pngicon: ../../../img/hero/hero_junkrat.png
---

# Junkrat { #head }

<img src="../../../img/hero/hero_junkrat.png" alt="" class="hero-portrait round-edge center" />

## \[Shift\] ConcussionMine <img src="../../../img/hero/hero_junkrat_shift.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Aimbot(Flick)` `#Combo`+`#KillSteal` { #shift-auto }

-8<- "hero_skill_autouse.ext"

If you are aiming at the enemy who is certainly can be eliminated with the Left-Shift combo, or there are any low-health enemies found, eliminate them by automatically throwing and exploding the Concussion Mine.

=== "English"
    <img src="../../../img/menu/menu_junkrat_concussionmine_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_junkrat_concussionmine_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"
