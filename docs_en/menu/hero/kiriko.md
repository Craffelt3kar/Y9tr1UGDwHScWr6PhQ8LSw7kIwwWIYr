---
pngicon: ../../../img/hero/hero_kiriko.png
---

# Kiriko { #head }

<img src="../../../img/hero/hero_kiriko.png" alt="" class="hero-portrait round-edge center" />

## \[LB\] HealingOfuda <img src="../../../img/hero/hero_kiriko_gun_1.png" alt="" class="img-line-bigger"/> `#Aimbot(Track)` { #lb }

Assist to heal up ally heroes using Healing Ofuda.

=== "English"
    <img src="../../../img/menu/menu_kiriko_healingofuda_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_kiriko_healingofuda_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"
