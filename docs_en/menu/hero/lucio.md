---
pngicon: ../../../img/hero/hero_lucio.png
---

# Lucio { #head }

<img src="../../../img/hero/hero_lucio.png" alt="" class="hero-portrait round-edge center" />

## \[Shift\] Crossfade <img src="../../../img/hero/hero_lucio_shift.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Support` { #shift-auto }

-8<- "hero_skill_autouse.ext"

If the health of anyone within the range (including myself) goes below the certain percentage, automatically switch the music to heal mode.

Otherwise, automatically switch back to the speed mode.

=== "English"
    <img src="../../../img/menu/menu_lucio_crossfade_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_lucio_crossfade_ko.png" alt="" class="center"/>

### Health Percent

Player and ally health percentage threshold to switch the music to the heal mode.

## \[E\] Amp <img src="../../../img/hero/hero_lucio_e.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Support` { #e-auto }

-8<- "hero_skill_autouse.ext"

Heal up nearby ally heroes using Amp it Up!

=== "English"
    <img src="../../../img/menu/menu_lucio_amp_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_lucio_amp_ko.png" alt="" class="center"/>

### Health Percent

Player and ally health percentage threshold to use Amp it Up!

If the health of anyone within the range (including myself) goes below this value, use Amp it Up! to increase the heal rate.
