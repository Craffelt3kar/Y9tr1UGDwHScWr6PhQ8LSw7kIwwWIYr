---
pngicon: ../../../img/hero/hero_mei.png
---

# Mei { #head }

<img src="../../../img/hero/hero_mei.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] Shard <img src="../../../img/hero/hero_mei_gun.png" alt="" class="img-line-bigger"/> `#Aimbot(Cast)` { #rb }

!!! info "This feature automatically works when you press the Right Click button."

=== "English"
    <img src="../../../img/menu/menu_mei_shard_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_mei_shard_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[Shift\] CryoFreeze <img src="../../../img/hero/hero_mei_shift.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Survive`+`#Evade` { #shift-auto }

-8<- "hero_skill_autouse.ext"

Use Cryo-Freeze to heal up and dodge threatening skills.

=== "English"
    <img src="../../../img/menu/menu_mei_cryofreeze_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_mei_cryofreeze_ko.png" alt="" class="center"/>

### Health Percent

Player health percentage threshold to use Cryo-Freeze.

If the health of the player goes below this value, automatically use Cryo-Freeze to prevent from dying.

### Dodge Spells

???+ success "Automatically evade the following skills"
    * [x] <img src="../../../img/hero/hero_reinhardt.png" alt="" class="img-line-bigger round-edge"/>Reinhardt Earthshatter<img src="../../../img/hero/hero_reinhardt_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_roadhog.png" alt="" class="img-line-bigger round-edge"/>Roadhog Chain Hook<img src="../../../img/hero/hero_roadhog_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_tracer.png" alt="" class="img-line-bigger round-edge"/>Tracer Pulse Bomb<img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>Ana Sleep Dart<img src="../../../img/hero/hero_ana_shift.png" alt="" class="img-line-bigger"/>

Use Cryo-Freeze to evade threatening skills and ultimates too.

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=88" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
