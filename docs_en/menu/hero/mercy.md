---
pngicon: ../../../img/hero/hero_mercy.png
---

# Mercy { #head }

<img src="../../../img/hero/hero_mercy.png" alt="" class="hero-portrait round-edge center" />

## \[LB\] CaduceusStaff <img src="../../../img/hero/hero_mercy_gun_1.png" alt="" class="img-line-bigger"/> `#Aimbot(Track)` { #lb }

Assist to aim ally heroes while healing or damage-boosting them with Caduceus Staff.

=== "English"
    <img src="../../../img/menu/menu_mercy_caduceusstaff_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_mercy_caduceusstaff_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"
