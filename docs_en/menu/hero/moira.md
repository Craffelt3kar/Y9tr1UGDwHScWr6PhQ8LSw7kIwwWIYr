---
pngicon: ../../../img/hero/hero_moira.png
---

# Moira { #head }

<img src="../../../img/hero/hero_moira.png" alt="" class="hero-portrait round-edge center" />

## \[Shift\] Fade <img src="../../../img/hero/hero_moira_shift.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Evade` { #shift-auto }

-8<- "hero_skill_autouse.ext"

???+ success "Automatically evade the following skills"
    * [x] <img src="../../../img/hero/hero_reinhardt.png" alt="" class="img-line-bigger round-edge"/>Reinhardt Earthshatter<img src="../../../img/hero/hero_reinhardt_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_roadhog.png" alt="" class="img-line-bigger round-edge"/>Roadhog Chain Hook<img src="../../../img/hero/hero_roadhog_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_tracer.png" alt="" class="img-line-bigger round-edge"/>Tracer Pulse Bomb<img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>Ana Sleep Dart<img src="../../../img/hero/hero_ana_shift.png" alt="" class="img-line-bigger"/>

Automatically evade threatening skills or ultimates by using Fade.

=== "English"
    <img src="../../../img/menu/menu_moira_fade_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_moira_fade_ko.png" alt="" class="center"/>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=113" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
