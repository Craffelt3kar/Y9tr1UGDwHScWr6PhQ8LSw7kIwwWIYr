---
pngicon: ../../../img/hero/hero_orisa.png
---

# Orisa { #head }

<img src="../../../img/hero/hero_orisa.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] EnergyJavelin <img src="../../../img/hero/hero_orisa_rmb.png" alt="" class="img-line-bigger"/> `#Aimbot(Cast)` { #rb }

Aim assist for Energy Javelin.

=== "English"
    <img src="../../../img/menu/menu_orisa_energyjavelin_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_orisa_energyjavelin_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[Shift\] Fortify <img src="../../../img/hero/hero_orisa_shift.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Evade` { #shift-auto }

-8<- "hero_skill_autouse.ext"

???+ success "Automatically counter the following skills"
    * [x] <img src="../../../img/hero/hero_reinhardt.png" alt="" class="img-line-bigger round-edge"/>Reinhardt Earthshatter<img src="../../../img/hero/hero_reinhardt_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_roadhog.png" alt="" class="img-line-bigger round-edge"/>Roadhog Chain Hook<img src="../../../img/hero/hero_roadhog_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_cassidy.png" alt="" class="img-line-bigger round-edge"/>Cassidy Magnetic Grenade<img src="../../../img/hero/hero_cassidy_e.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_tracer.png" alt="" class="img-line-bigger round-edge"/>Tracer Pulse Bomb<img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/>

Automatically withstand threatening skills and ultimates by automatically using Fortify.

=== "English"
    <img src="../../../img/menu/menu_orisa_fortify_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_orisa_fortify_ko.png" alt="" class="center"/>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=48" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

## \[E\] JavelinSpin <img src="../../../img/hero/hero_orisa_shift.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Aimbot(Flick)` `#Evade` { #e-auto }

-8<- "hero_skill_autouse.ext"

???+ success "Automatically counter the following skills"
    * [x] <img src="../../../img/hero/hero_roadhog.png" alt="" class="img-line-bigger round-edge"/>Roadhog Chain Hook<img src="../../../img/hero/hero_roadhog_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_cassidy.png" alt="" class="img-line-bigger round-edge"/>Cassidy Magnetic Grenade<img src="../../../img/hero/hero_cassidy_e.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_tracer.png" alt="" class="img-line-bigger round-edge"/>Tracer Pulse Bomb<img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>Ana Sleep Dart<img src="../../../img/hero/hero_ana_shift.png" alt="" class="img-line-bigger"/>

Automatically eat up threatening skills and ultimates by automatically using Javeline Spin.

=== "English"
    <img src="../../../img/menu/menu_orisa_javelinspin_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_orisa_javelinspin_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=48" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

## \[Ult\] TerraSurge <img src="../../../img/hero/hero_orisa_shift.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#KillSteal` { #ult }

!!! info "This feature always automatically works if you are currently using Terra Surge."

While using Terra Surge, if there are any enemies that can be eliminated at the current charge percent, kill them by automatically finishing the ultimate.

=== "English"
    <img src="../../../img/menu/menu_orisa_terrasurge_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_orisa_terrasurge_ko.png" alt="" class="center"/>
