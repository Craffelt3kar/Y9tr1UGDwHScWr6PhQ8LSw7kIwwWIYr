---
pngicon: ../../../img/hero/hero_ramattra.png
---

# Ramattra { #head }

<img src="../../../img/hero/hero_ramattra.png" alt="" class="hero-portrait round-edge center" />

Rammatra has two weapons: Void Accelerator and Pummel

Because of this, there is no Aimbot Tab.
Instead, two separate aimbot tab exist for each weapon.

## Staff <img src="../../../img/hero/hero_ramattra_1_gun.png" alt="" class="img-line-bigger"/> `#Aimbot(Track)` { #staff }

Aim assist for Void Accelerator. (Omnic Form)

=== "English"
    <img src="../../../img/menu/menu_ramattra_staff_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_ramattra_staff_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"

## Pummel <img src="../../../img/hero/hero_ramattra_2_gun.png" alt="" class="img-line-bigger"/> `#Aimbot(Flick)` { #pummel }

Aim assist for Pummel. (Nemesis Form)

Aimkey is same with [Staff](#staff) tab.

=== "English"
    <img src="../../../img/menu/menu_ramattra_pummel_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_ramattra_pummel_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"
