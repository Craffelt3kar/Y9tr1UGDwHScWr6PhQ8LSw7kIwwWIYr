---
pngicon: ../../../img/hero/hero_reaper.png
---

# Reaper { #head }

<img src="../../../img/hero/hero_reaper.png" alt="" class="hero-portrait round-edge center" />

## \[Shift\] Wraith <img src="../../../img/hero/hero_reaper_shift.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Evade` { #shift-auto }

-8<- "hero_skill_autouse.ext"

???+ success "Automatically evade the following skills"
    * [x] <img src="../../../img/hero/hero_reinhardt.png" alt="" class="img-line-bigger round-edge"/>Reinhardt Earthshatter<img src="../../../img/hero/hero_reinhardt_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_roadhog.png" alt="" class="img-line-bigger round-edge"/>Roadhog Chain Hook<img src="../../../img/hero/hero_roadhog_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_tracer.png" alt="" class="img-line-bigger round-edge"/>Tracer Pulse Bomb<img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>Ana Sleep Dart<img src="../../../img/hero/hero_ana_shift.png" alt="" class="img-line-bigger"/>

Automatically use Wraith to evade threatening skills and ultimates.

=== "English"
    <img src="../../../img/menu/menu_reaper_wraith_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_reaper_wraith_ko.png" alt="" class="center"/>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=77" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
