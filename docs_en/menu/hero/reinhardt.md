---
pngicon: ../../../img/hero/hero_reinhardt.png
---

# Reinhardt { #head }

<img src="../../../img/hero/hero_reinhardt.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] BarrierField <img src="../../../img/hero/hero_reinhardt_rmb.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Aimbot(Track)` `#Counter` { #rb-auto }

-8<- "hero_skill_autouse.ext"

???+ success "Automatically counter the following skills"
    * [x] <img src="../../../img/hero/hero_reinhardt.png" alt="" class="img-line-bigger round-edge"/>Reinhardt Earthshatter<img src="../../../img/hero/hero_reinhardt_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_roadhog.png" alt="" class="img-line-bigger round-edge"/>Roadhog Chain Hook<img src="../../../img/hero/hero_roadhog_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_tracer.png" alt="" class="img-line-bigger round-edge"/>Tracer Pulse Bomb<img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>Ana Sleep Dart<img src="../../../img/hero/hero_ana_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>Ana Biotic Grenade<img src="../../../img/hero/hero_ana_e.png" alt="" class="img-line-bigger"/>

Automatically block threatening skills and ultimates by lifting Barrier Field.

=== "English"
    <img src="../../../img/menu/menu_reinhardt_barrierfield_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_reinhardt_barrierfield_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=23" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

## \[E\] FireStrike <img src="../../../img/hero/hero_reinhardt_e.png" alt="" class="img-line-bigger"/> `#Aimbot(Cast)` { #e }

!!! info "This feature automatically works when you use Fire Strike."

=== "English"
    <img src="../../../img/menu/menu_reinhardt_firestrike_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_reinhardt_firestrike_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"
