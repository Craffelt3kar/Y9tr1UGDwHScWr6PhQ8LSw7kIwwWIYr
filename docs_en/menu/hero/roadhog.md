---
pngicon: ../../../img/hero/hero_roadhog.png
---

# Roadhog { #head }

<img src="../../../img/hero/hero_roadhog.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] Breather <img src="../../../img/hero/hero_roadhog_e.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Survive` { #rb-auto }

-8<- "hero_skill_autouse.ext"

When the health of the player goes down below a certain percentage, automatically use Take a Breather until it heals the player up to a certain percent.

=== "English"
    <img src="../../../img/menu/menu_roadhog_breather_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_roadhog_breather_ko.png" alt="" class="center"/>

### Start Health Percent

The player health percentage to start Breather.

### End Health Percent

The player health percentage to end Breather.

## \[Shift\] ChainHook <img src="../../../img/hero/hero_roadhog_shift.png" alt="" class="img-line-bigger"/> `#Aimbot(Flick)` { #shift }

Aim assist for Chain Hook.

=== "English"
    <img src="../../../img/menu/menu_roadhog_chainhook_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_roadhog_chainhook_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

## \[Ult\] WholeHog <img src="../../../img/hero/hero_roadhog_q.png" alt="" class="img-line-bigger"/> `#Aimbot(Track)` { #ult }

Aim assist for Whole Hog.

=== "English"
    <img src="../../../img/menu/menu_roadhog_wholehog_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_roadhog_wholehog_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"
