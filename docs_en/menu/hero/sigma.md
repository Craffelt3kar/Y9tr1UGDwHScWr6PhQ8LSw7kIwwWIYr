---
pngicon: ../../../img/hero/hero_sigma.png
---

# Sigma { #head }

<img src="../../../img/hero/hero_sigma.png" alt="" class="hero-portrait round-edge center" />

## \[E\] Accretion <img src="../../../img/hero/hero_sigma_e.png" alt="" class="img-line-bigger"/> `#Aimbot(Cast)` { #e }

!!! info "This feature automatically works when you use Accretion."

Aim assist for Accretion.

=== "English"
    <img src="../../../img/menu/menu_sigma_accretion_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_sigma_accretion_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[Shift\] KineticGrasp <img src="../../../img/hero/hero_sigma_shift.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Counter` { #shift-auto }

-8<- "hero_skill_autouse.ext"

???+ success "Automatically counter the following skills"
    * [x] <img src="../../../img/hero/hero_cassidy.png" alt="" class="img-line-bigger round-edge"/>Cassidy Magnetic Grenade<img src="../../../img/hero/hero_cassidy_e.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_tracer.png" alt="" class="img-line-bigger round-edge"/>Tracer Pulse Bomb<img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>Ana Sleep Dart<img src="../../../img/hero/hero_ana_shift.png" alt="" class="img-line-bigger"/>

Automatically eat up threatening skills and ultimates by automatically using Kinetic Grasp.

=== "English"
    <img src="../../../img/menu/menu_sigma_kineticgrasp_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_sigma_kineticgrasp_ko.png" alt="" class="center"/>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=37" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
