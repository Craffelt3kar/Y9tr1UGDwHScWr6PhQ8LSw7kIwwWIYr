---
pngicon: ../../../img/hero/hero_sojourn.png
---

# Sojourn { #head }

<img src="../../../img/hero/hero_sojourn.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] AltFire <img src="../../../img/hero/hero_sojourn_gun.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Aimbot(Flick)` `#KillSteal` { #rb-auto }

-8<- "hero_skill_manualuse.ext"

Automatically eliminate the low-health enemy with Railgun AltFire.

=== "English"
    <img src="../../../img/menu/menu_sojourn_altfire_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_sojourn_altfire_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

## \[RB\] AltFireManual <img src="../../../img/hero/hero_sojourn_gun.png" alt="" class="img-line-bigger"/> `#Aimbot(Flick)` { #rb }

Aim assist for Railgun alt-fire.

=== "English"
    <img src="../../../img/menu/menu_sojourn_altfiremanual_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_sojourn_altfiremanual_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"
