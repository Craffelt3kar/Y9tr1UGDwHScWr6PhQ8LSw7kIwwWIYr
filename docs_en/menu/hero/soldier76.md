---
pngicon: ../../../img/hero/hero_soldier76.png
---

# Soldier: 76 { #head }

<img src="../../../img/hero/hero_soldier76.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] HelixRockets <img src="../../../img/hero/hero_soldier76_rmb.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Aimbot(Flick)` `#KillSteal` { #rb-auto }

-8<- "hero_skill_manualuse.ext"

Automatically eliminate the low-health enemy with Helix Rocket.

=== "English"
    <img src="../../../img/menu/menu_soldier76_helixrockets_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_soldier76_helixrockets_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

## \[E\] BioticField <img src="../../../img/hero/hero_soldier76_e.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Survive` { #e-auto }

-8<- "hero_skill_autouse.ext"

Automatically heal up the player when the health is low using Biotic Field.

=== "English"
    <img src="../../../img/menu/menu_soldier76_bioticfield_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_soldier76_bioticfield_ko.png" alt="" class="center"/>

### Health Percent

Player health threshold to use Biotic Field.

If the health of the player goes below this value, automatically use Biotic Field to heal up.
