---
pngicon: ../../../img/hero/hero_sombra.png
---

# Sombra { #head }

<img src="../../../img/hero/hero_sombra.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] Hack <img src="../../../img/hero/hero_sombra_rmb.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Combo` { #rb-auto }

-8<- "hero_skill_manualuse.ext"

Before firing the weapon, if the aimbot target is not hacked yet, automatically hack them.

=== "English"
    <img src="../../../img/menu/menu_sombra_hack_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_sombra_hack_ko.png" alt="" class="center"/>

## \[Shift\] Virus <img src="../../../img/hero/hero_sombra_shift.webp" alt="" class="img-line-bigger"/> `#AutoSkill` `#Aimbot(Flick)` `#Combo` { #shift-auto }

-8<- "hero_skill_manualuse.ext"

Before firing the weapon, if Virus skill is available, automatically throw the virus to them.

=== "English"
    <img src="../../../img/menu/menu_sombra_virus_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_sombra_virus_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

## \[Ult\] EMP <img src="../../../img/hero/hero_sombra_q.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Counter` { #ult-auto }

-8<- "hero_skill_autouse.ext"

???+ success "Automatically counter the following skills"
    * [x] <img src="../../../img/hero/hero_reinhardt.png" alt="" class="img-line-bigger round-edge"/>Reinhardt Earthshatter<img src="../../../img/hero/hero_reinhardt_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_genji.png" alt="" class="img-line-bigger round-edge"/>Genji Dragonblade<img src="../../../img/hero/hero_genji_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_lucio.png" alt="" class="img-line-bigger round-edge"/>Lucio Sound Barrier<img src="../../../img/hero/hero_lucio_q.png" alt="" class="img-line-bigger"/>

Automatically use EMP to counter certain ultimates.

=== "English"
    <img src="../../../img/menu/menu_sombra_emp_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_sombra_emp_ko.png" alt="" class="center"/>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=102" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
