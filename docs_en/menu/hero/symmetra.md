---
pngicon: ../../../img/hero/hero_symmetra.png
---

# Symmetra { #head }

<img src="../../../img/hero/hero_symmetra.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] Orb <img src="../../../img/hero/hero_symmetra_gun.png" alt="" class="img-line-bigger"/> `#Aimbot(Flick)` { #rb }

Aim assist for Orb charge shot (Alt-fire).

=== "English"
    <img src="../../../img/menu/menu_symmetra_orb_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_symmetra_orb_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"
