---
pngicon: ../../../img/hero/hero_tracer.png
---

# Tracer { #head }

<img src="../../../img/hero/hero_tracer.png" alt="" class="hero-portrait round-edge center" />

## \[Shift\] Blink <img src="../../../img/hero/hero_tracer_shift.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Aimbot(Flick)` `#KillSteal` { #shift-auto }

-8<- "hero_skill_manualuse.ext"

Configure the Blink-combo automation.

=== "English"
    <img src="../../../img/menu/menu_tracer_blink_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_tracer_blink_ko.png" alt="" class="center"/>

### On Melee

Enable the Blink-Melee combo automation.

### Only Melee If No Ammo

Only use Blink-Melee combo when out-of-ammo.

### On Pulse Bomb

Enable the Blink-PulseBomb combo automation.

-8<- "hero_tab_flick_aimbot.ext"

## \[Ult\] PulseBomb <img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/> `#Aimbot(Cast)` { #ult }

Aim assist for Pulse Bomb.

=== "English"
    <img src="../../../img/menu/menu_tracer_pulsebomb_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_tracer_pulsebomb_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[E\] Recall <img src="../../../img/hero/hero_tracer_e.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Survive` { #e-auto }

-8<- "hero_skill_autouse.ext"

If the health is low, automatically use Recall to survive.

=== "English"
    <img src="../../../img/menu/menu_tracer_recall_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_tracer_recall_ko.png" alt="" class="center"/>

### Health Percent

Player health percentage threshold to use Recall.

### Health Lost Percent

Player health lost percentage threshold to use Recall.
