---
pngicon: ../../../img/hero/hero_widowmaker.png
---

# Widowmaker { #head }

<img src="../../../img/hero/hero_widowmaker.png" alt="" class="hero-portrait round-edge center" />

Widowmaker's weapon, Widowmaker's kiss, has two mode: SMG mode and Sniper mode

Because of this, there is no Aimbot Tab.
Instead, two separate aimbot tab exist for each weapon mode.

## Sniper <img src="../../../img/hero/hero_widowmaker_gun.png" alt="" class="img-line-bigger"/> `#Aimbot(Flick)` { #sniper }

Aim assist for Widowmaker's kiss Sniper mode.

=== "English"
    <img src="../../../img/menu/menu_widowmaker_sniper_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_widowmaker_sniper_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

## SMG <img src="../../../img/hero/hero_widowmaker_gun.png" alt="" class="img-line-bigger"/> `#Aimbot(Track)` { #smg }

Aim assist for Widowmaker's kiss SMG mode.

Aimkey is same with [Sniper](#sniper) tab.

=== "English"
    <img src="../../../img/menu/menu_widowmaker_smg_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_widowmaker_smg_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"
