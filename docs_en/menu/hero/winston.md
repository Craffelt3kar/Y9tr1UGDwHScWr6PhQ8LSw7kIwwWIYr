---
pngicon: ../../../img/hero/hero_winston.png
---

# Winston { #head }

<img src="../../../img/hero/hero_winston.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] AltFire <img src="../../../img/hero/hero_winston_gun.png" alt="" class="img-line-bigger"/> `#Aimbot(Flick)` { #rb }

Aim assist for Charge shot. (Alt-fire)

=== "English"
    <img src="../../../img/menu/menu_winston_altfire_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_winston_altfire_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"
