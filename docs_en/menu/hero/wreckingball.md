---
pngicon: ../../../img/hero/hero_wreckingball.png
---

# Wreckingball { #head }

<img src="../../../img/hero/hero_wreckingball.png" alt="" class="hero-portrait round-edge center" />

## \[E\] AdaptiveShield <img src="../../../img/hero/hero_wreckingball_e.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Survive` { #e-auto }

-8<- "hero_skill_autouse.ext"

If the health goes below some point and one or more enemies are in a certain range, automatically use Adaptive Shield to gain shield health.

=== "English"
    <img src="../../../img/menu/menu_wreckingball_adaptiveshield_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_wreckingball_adaptiveshield_ko.png" alt="" class="center"/>
