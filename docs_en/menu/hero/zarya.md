---
pngicon: ../../../img/hero/hero_zarya.png
---

# Zarya { #head }

<img src="../../../img/hero/hero_zarya.png" alt="" class="hero-portrait round-edge center" />

## \[Shift\] SelfBubble <img src="../../../img/hero/hero_zarya_shift.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Aimbot(Flick)` `#Survive` { #shift-auto }

-8<- "hero_skill_autouse.ext"

Automatically use self-bubble when the health goes low.

=== "English"
    <img src="../../../img/menu/menu_zarya_selfbubble_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_zarya_selfbubble_ko.png" alt="" class="center"/>

### Health Percent

Player health percentage threshold to use Self-Bubble.

### Health Lost Percent

Player health lost percentage threshold to use Self-Bubble.

## \[E\] AllyBubble <img src="../../../img/hero/hero_zarya_e.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Aimbot(Flick)` `#Support` { #e-auto }

-8<- "hero_skill_autouse.ext"

Automatically aim the low-health ally and bubble them.

=== "English"
    <img src="../../../img/menu/menu_zarya_allybubble_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_zarya_allybubble_ko.png" alt="" class="center"/>

### Health Percent

Ally health percentage threshold to give them Ally-Bubble.

### Health Lost Percent

Ally health lost percentage threshold to give them Ally-Bubble.

-8<- "hero_tab_flick_aimbot.ext"
