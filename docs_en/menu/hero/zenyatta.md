---
pngicon: ../../../img/hero/hero_zenyatta.png
---

# Zenyatta { #head }

<img src="../../../img/hero/hero_zenyatta.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] OrbsRelease <img src="../../../img/hero/hero_zenyatta_gun.png" alt="" class="img-line-bigger"/> `#Aimbot(Cast)` { #rb }

!!! info "You should charge up the orbs using Right Click before activating the aim assist."

Aim assist for Orb release shot. (Alt-fire)

=== "English"
    <img src="../../../img/menu/menu_zenyatta_orbsrelease_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_zenyatta_orbsrelease_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[Shift\] HarmonyOrb <img src="../../../img/hero/hero_zenyatta_shift.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Aimbot(Flick)` `#Support` { #shift-auto }

-8<- "hero_skill_autouse.ext"

Automatically give to the low-health ally the Orb of Harmony.

=== "English"
    <img src="../../../img/menu/menu_zenyatta_harmonyorb_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_zenyatta_harmonyorb_ko.png" alt="" class="center"/>

### Max Health Percent

Any ally having health below this value will get the Orb of Harmony.

-8<- "hero_tab_flick_aimbot.ext"

## \[E\] DiscordOrb <img src="../../../img/hero/hero_zenyatta_e.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Combo` { #e-auto }

!!! info "This feature automatically works when you attack the enemy."

When attacking the enemy with aimbot, automatically attach Orb of Discord to them.

=== "English"
    <img src="../../../img/menu/menu_zenyatta_discordorb_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_zenyatta_discordorb_ko.png" alt="" class="center"/>

## \[Ult\] Transcendence <img src="../../../img/hero/hero_zenyatta_q.png" alt="" class="img-line-bigger"/> `#AutoSkill` `#Counter` { #ult-auto }

-8<- "hero_skill_autouse.ext"

???+ success "Automatically counter the following skills"
    * [x] <img src="../../../img/hero/hero_reinhardt.png" alt="" class="img-line-bigger round-edge"/>Reinhardt Earthshatter<img src="../../../img/hero/hero_reinhardt_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_sombra.png" alt="" class="img-line-bigger round-edge"/>Sombra EMP<img src="../../../img/hero/hero_sombra_q.png" alt="" class="img-line-bigger"/>

Automatically use transcendence to counter certain ultimates.

=== "English"
    <img src="../../../img/menu/menu_zenyatta_transcendence_en.png" alt="" class="center"/>

=== "Korean"
    <img src="../../../img/menu/menu_zenyatta_transcendence_ko.png" alt="" class="center"/>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=127" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
