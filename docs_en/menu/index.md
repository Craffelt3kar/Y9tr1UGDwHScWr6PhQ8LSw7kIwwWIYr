---
description: Noble 3 menu lets you configure various features with a simple and intuitive UI.
icon: svgrepo/sliders2
---

# Noble Menu { #head }

## Appearance { #appearance }

<figure><img src="../../../img/menu/menu_en.png" alt="Appearance of Noble Menu"><figcaption><p>Appearance of Noble Menu</p></figcaption></figure>

First loaded, Overwatch icon is shown at the top of the menu.

After entering the game and picking the hero, the icon of the currently picked hero is shown at the top of the menu.

The settings in the menu are split into corresponding tabs. Most heroes are have those 4 tabs: [Visuals Tab](visuals.md), [Aimbot Tab](aimbot/index.md), [Melee Tab](melee.md), and [Rageaim Tab](rageaim.md)

If the skill assistance is available for the current playing hero, the corresponding skill assistance tab is shown between Aimbot Tab and Melee Tab.

If the weapon of the hero you're playing has multiple weapons or weapon modes, Aimbot Tab may be disappeared/renamed to the weapon name or mode name. (e.g. Widowmaker, Ramattra)

See [Per-hero Settings](hero/index.md) category for more such cases.

### Session Time { #session-time }

The remaining time of your Noble license.

???+ warning "When your session time ends, the game automatically closes itself."
    Along the line, any kind of error or crash message may appear. But it's not a real error.

???+ info "If your license is longer than 24 hours, it only shows 24 hours."
    The time passes from 24 hours, and if it reaches 0, it will reset to 24 hours and continue to pass.

### Language { #language }

The UI language to display the Noble Menu.

* EN: English
* CN: Chinese
* KR: Korean

## Toggling Menu { #toggle-menu }

The menu toggle key is `Insert` and `F3`.

<figure><img src="../../../img/menu/menu_key.png" alt="104-key US Keyboard Layout. Insert and F3 key is highlighted."><figcaption><p>A 104-key US keyboard layout, largely standardized since the IBM Personal System/2. (Drawn by Mysid in CorelDRAW.)</p></figcaption></figure>

## Manually change the value of sliders { #set-slider-value }

By default, you can change slider values by dragging it left or right.
But sometimes may want to specify its value manually.

For those situations, there are two methods:

* Press the slider while pressing the `Ctrl` key.

* Choose the slider to edit the value by pressing the `Tab` and `Shift+Tab` key.
    Press the `Tab` key to select the next slider, and `Shift+Tab` to select the previous slider.

    <img src="../../../img/menu/menu_set_slider_value.gif" alt="Snap between sliders using TAB and SHIFT+TAB" class="center"/>

## Guide and Showcase Video { #showcase }

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/64H2cX6o7Rk" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
