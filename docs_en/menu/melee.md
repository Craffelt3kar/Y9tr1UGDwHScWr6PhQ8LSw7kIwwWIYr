---
description: Melee Tab - Auto-Melee configuration.
icon: svgrepo/fist-variant
tags:
    - Melee
    - AutoMelee
    - AutoAttack
---

# Melee Tab { #head }

From Melee tab, you can configure the Auto-Melee feature.

=== "English"
    <img src="../../img/menu/menu_melee_en.png" alt="Melee Tab" class="center"/>

=== "Korean"
    <img src="../../img/menu/menu_melee_ko.png" alt="Melee Tab" class="center"/>

Settings are basically the same as [Aimbot Tab](aimbot/index.md) and [Flick Aimbot](aimbot/flick.md).

It automatically aims and melee attacks nearby enemies whose health is lower than 30.

## Showcase Video { #showcase }

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/O6N3bEzwmbY" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
