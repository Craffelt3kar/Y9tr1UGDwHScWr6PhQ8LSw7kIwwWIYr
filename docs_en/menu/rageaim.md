---
description: Rageaim Tab - Rage Aimbot configuration.
icon: svgrepo/headshot
tags:
    - Rage
    - Silent Aim
    - pSilent
    - RageBot
---

# Rageaim Tab { #head }

??? danger "This feature is clearly an unsafe feature."
    <p class="very-important-info">This feature is clearly an unsafe feature. It's recommended to use this feature only on burner accounts.</mark>

    This also applies to Custom Game, Practice Range, PvE, etc.

    _The account used to record showcase videos below is beamed the next day._

You can configure Noble 3's hidden power, Rage Aimbot from this tab.

It is a client-side silent aim.
This means, it automatically aims and shoots nearby enemies, *without actually looking at them*. (It's only client-side)

Especially, using Rage Aim with track aim heroes (e.g. Soldier: 76, Tracer) result in much faster ban than flick heroes. (e.g. Cassidy, Ashe)

<p class="very-important-info">There is no 'safe ragebot' thing. Such thing is fundamentally impossible.</p>

!!! hint "Weird movement direction while using silent aim"
    For track aimbot heroes (such as Soldier: 76, Tracer, etc.) the character would move in some weird direction while using the Rage Aimbot feature.

    This is a completely normal phenomenon. The aimbot is aim-locking to the targets and only just not showing it on the client side.

=== "English"
    <img src="../../img/menu/menu_rageaim_en.png" alt="Rageaim Tab" class="center"/>

=== "Korean"
    <img src="../../img/menu/menu_rageaim_ko.png" alt="Rageaim Tab" class="center"/>

## Enabled { #enabled }

Enable or disable the Rage Aimbot.

Even if it's enabled, Rage Aimbot will not work until you press the hotkey.

## Key { #key }

The hotkey to activate the rage aimbot.

The Rage Aimbot won't work unless you press this key, even if it is enabled.

-8<- "aimbot_keybind.ext"

## FoV { #fov }

FoV of the Rage Aimbot to search targets to aim.

-8<- "aimbot_fov_disable.ext"

## Showcase Videos { #showcase }

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/XyXJA6Z-vJU" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/3orksYgpsyo" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/JOJyJBTQ7-g" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
