---
description: Visuals Tab - Configuration of visual features such as ESP, Cooldowns, and Aimbot Target.
icon: svgrepo/visuals
tags:
    - Visuals
    - ESP
    - Skeleton
    - Health
    - Bone
    - Outline
    - Cooldowns
    - Color
---

# Visuals Tab { #head }

In Visuals tab, you can configure various visual features such as ESP, Cooldowns, and so on.

!!! info "Visual feature settings are saved globally."
    Visual feature settings are saved globally instead of per-hero.

    For example if you enable ESP on Cassidy, you can still have ESP enabled even after changing hero to Widowmaker.

=== "English"
    <img src="../../img/menu/menu_visuals_en.png" alt="Visuals Tab" class="center"/>

=== "Korean"
    <img src="../../img/menu/menu_visuals_ko.png" alt="Visuals Tab" class="center"/>

## :material-human: ESP { #esp }

### Draw Skeleton { #draw-skeleton }

Enable or disable enemy hero Skeleton ESP. Their head is highlighted as a big dot.

??? example "Showcase"
    <figure><img src="../../img/showcase/showcase_skeleton_visible.png" alt="Draw Skeleton visible enemy hero"><figcaption><p>Visible enemy hero</p></figcaption></figure>
    <figure><img src="../../img/showcase/showcase_skeleton_invisible.png" alt="Draw Skeleton invisible enemy hero"><figcaption><p>Invisible enemy hero</p></figcaption></figure>

### Draw Name { #draw-name }

Show or hide the enemy hero's name to the right below their position.

??? example "Showcase"
    <img src="../../img/showcase/showcase_name.png" alt="Draw Name Showcase" class="center"/>

### Draw HealthBar { #draw-healthbar }

Show or hide the health bar to the right above their position.

Unlike the original in-game enemy health bar, this health bar ESP is always displayed no matter if they're attacked.

Armor, Shield, Extra Health and Purple state(Ana <img src="../../img/hero/hero_ana_e.png" alt="" class="img-line"/>) is displayed as its corresponding color.

??? example "Showcase"
    <img src="../../img/showcase/showcase_healthbar.png" alt="Draw Healthbar Showcase" class="center"/>

### Draw Box { #draw-box }

Enable or disable 2D Box ESP.

??? example "Showcase"
    <figure><img src="../../img/showcase/showcase_box_visible.png" alt="Draw Box visible enemy hero"><figcaption><p>Visible enemy hero</p></figcaption></figure>
    <figure><img src="../../img/showcase/showcase_box_invisible.png" alt="Draw Box invisible enemy hero"><figcaption><p>Invisible enemy hero</p></figcaption></figure>

### Draw Outline { #draw-outline }

Enable or disable outline ESP.

??? example "Showcase"
    <figure><img src="../../img/showcase/showcase_outline_visible.png" alt="Draw Outline visible enemy hero"><figcaption><p>Visible enemy hero</p></figcaption></figure>
    <figure><img src="../../img/showcase/showcase_outline_invisible.png" alt="Draw Outline invisible enemy hero"><figcaption><p>Invisible enemy hero</p></figcaption></figure>

## :material-av-timer: Cooldown and Ultimate Charge display { #skill-cooldowns }

### Draw Cooldowns { #draw-cooldowns }

Show or hide enemy hero skill cooldowns and ultimate charges to the right below their position. (sticky)

??? example "Showcase"
    <img src="../../img/showcase/showcase_cooldowns.png" alt="Draw Cooldowns Showcase" class="center"/>

### Draw Cooldowns Scoreboard { #draw-cooldowns-scoreboard }

Show or hide enemy hero skill cooldowns and ultimate charges to Scoreboard. (the overlay that shows if you press the `Tab` key in mid-game)

Skills in cooldown are shown as grey, and the cooldown timer (or ultimate charge percentage) is drawn as vertical orange-colored bar.

??? example "Showcase"
    <img src="../../img/showcase/showcase_cooldowns_scoreboard.jpg" alt="Draw Cooldowns Scoreboard Showcase" class="center"/>

## :material-target: Aimbot Target ESP { #aimbot-targets }

### Draw Targets { #draw-targets }

If enabled, show the current target of aimbot as 2D Box ESP.

??? example "Showcase"
    <img src="../../img/showcase/showcase_target.png" alt="Draw Targets Showcase" class="center"/>

### Draw Target Lines { #draw-target-lines }

!!! info "This option is only available if the 'Draw Targets' option is enabled."

If enabled, draw a line between the crosshair and the bone where the aimbot is going to aim.

The destination of the aimbot is marked as a big dot.

??? example "Showcase"
    <img src="../../img/showcase/showcase_target+line.png" alt="Draw Target Lines Showcase" class="center"/>

## :material-palette: ESP Color { #esp-color }

Click the color box to open the color palette.

Or you can just manually enter the RGB value by clicking each value field.

On color palette, you pick the color using HSV slider or specify RGB, HSV, or HTML color code.

??? example "Color palette"
    <img src="../../img/menu/menu_visuals_colorsel_ko.png" alt="Color palette" class="center"/>

### Color Visible { #color-visible }

Color of ESP for visible enemies.

### Color Not Visible { #color-not-visible }

Color of ESP for not visible enemies. (Behind wall or Behind Shield if Shield Check is enabled)

### Target Color { #target-color }

!!! info "This option is only available if the 'Draw Targets' option is enabled."

Color of Aimbot Target ESP and its line. (If enabled)
