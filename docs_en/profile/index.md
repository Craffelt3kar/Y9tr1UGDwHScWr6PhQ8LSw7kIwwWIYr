---
icon: material/share
tags:
  - Share
  - Config
  - Xml
---

# Profile File { #head }

!!! notice "Visuals settings are saved globally to: `Global.xml`"

Noble 3 saves all hero profiles(settings) into the `config` folder, which is located at the same folder as Noble Loader.

All Aimbot, Skill Assistance, Melee, and Rageaim related settings are saved in the file named corresponding hero, in XML format.
(For example, Widowmaker settings are saved to `Widowmaker.xml`)

???+ warning "Some hero settings file name may not match the real hero name."
    For example, Cassidy's settings are saved to `Mccree.xml`, which is his old name.

You can freely backup, restore, and share per-hero settings using these XML files.

## How to apply other user's settings { #apply }

If you're going to download and apply other user's settings, the first thing <span class="important-info">you should make sure is that Noble 3 is not running and not injected into the game.</span>

Then you have just overwrite the original setting XML file located in `config` folder with the downloaded setting XML file.

For example, let's assume you're going to apply other user's Widowmaker settings.
You've downloaded `Widowmaker.xml`. Then you should make sure that Noble 3 is not injected and the game is not running.

If you've already injected it into the game, overwriting the settings file never applies the settings.
As soon as you change your settings in In-Game Menu the file will be automatically re-written and all changes are reverted.

After that, all you have to do is just overwrite `Widowmaker.xml` in `config` folder with your downloaded `Widowmaker.xml`.

<img src="../img/profile_file_overwrite.png" alt="" class="center"/>
