---
icon: material/robot
tags:
  - Share
  - Config
  - Settings Bot
---

# Noble Setting Bot Usage { #head }

## Show and download other user settings { #show }

{% hint style="info" %}
You can see the list of heroes by just typing `/show`
{% endhint %}

Enter `/show <Hero Name>`

```
1. AAAA#0 (ana) - 2 👍 / 0 👎
2. BBBB#0 (ana) - 0 👍 / 0 👎
3. CCCC#0 (ana) - 0 👍 / 0 👎
4. DDDD#0 (ana) - 0 👍 / 0 👎
```

Settings list will appear just like this.
If the result list is too large and get paginated, you can use :fontawesome-solid-arrow-left: and :fontawesome-solid-arrow-right: button to navigate.

Enter the number of settings to chat to show the specified settings.
In here, we will enter `1` to show and download the settings of 'AAAA#0'

You can review the user's settings by reacting with 👍 or 👎 emoji.

To apply the downloaded settings, see [Profile File](index.md) page.

## Upload my settings { #upload }

Type `/upload <Hero Name>` to chat, then attach the settings file to the message, then send the message.
