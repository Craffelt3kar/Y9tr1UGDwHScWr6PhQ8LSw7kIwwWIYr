---
icon: svgrepo/fps-240
tips:
  - Aimbot
---

# Game Framerate and Aimbot Smooth Algorithm { #head }

## What happens if your game frame rate is too *HIGH* { #fps-too-high }

1. As Noble 3 is an internal product, the smooth algorithm is applied **each frame** of the game while the aimbot is active.  
    Thus, the game frame rate increases, the many times the smooth algorithm applies, **resulting in fast and sharp aim at the same smooth setting.**  
    You can check this phenomenon in [Showcase videos](#fps-affect-showcase).

 * If you completely disable the frame rate limit or set it too high, resulting in the frame rate fluctuating by game mode or game player count and so on, the aimbot aim speed will fluctuate too.  
   For example, let's say that you got 400 fps from Practice Range, 600 fps from Custom Game, and 200 fps from Quick Play.
   In this case, you need to find different setting values for each game mode. Which is very annoying and troublesome work.

2. If the game frame rate is too high, the smooth algorithm starts to act weird.

 * By default, the smooth algorithm should act in a 'Accelerate at the start - Maintain speed - Decelerate when it's getting close to the target' manner.  
   But if your game frame rate goes too high, the 'Accelerate' and 'Decelerate' step starts to fade out.
   <figure><img src="../../../img/fps_smooth_graph.png" alt=""><figcaption><p>Red: Frame rate is normal, Blue: Frame rate is too high</p></figcaption></figure>

 * But it also means that you can use this characteristic to tweak the smooth algorithm in a detailed manner.
   For example, you can make your aimbot shoot more sharply and accurately by increasing the game frame rate limit.

## What happens if your game framerate is too *LOW* { #fps-too-low }

A lower frame rate makes your aimbot smoother. But if the frame rate is too low it may break your triggerbot.

It's because Noble's triggerbot does the ray check for *each frame*.

To help understanding, let's see the following example images:

<figure><img src="../../../img/fps_affect_lowfps_high.gif" alt=""><figcaption><p>Normal</p></figcaption></figure>

The blue circle means the target and the box means your crosshair.
When the color of the box is turned green, it means that the triggerbot just automatically pushed the trigger of your gun.

<figure><img src="../../../img/fps_affect_lowfps_low.gif" alt=""><figcaption><p>Frame rate is too low</p></figcaption></figure>

If your frame rate is too low, the target has *passed* the crosshair but not actually *collided* with it. Resulting in the trigger-bot to not shoot the target.

However, this problem only occurs if your frame rate is TOO low, under 60fps.

## Showcase Video { #fps-affect-showcase }

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/SuJoDBBa16Q" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/nJOn-OG3Aac" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/Cv_aLxWWwlg" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
