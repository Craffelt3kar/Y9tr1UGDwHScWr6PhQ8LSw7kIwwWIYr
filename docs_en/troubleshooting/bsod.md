---
icon: svgrepo/bsod
tags:
  - Crash
  - BSOD
  - BlueScreen
  - Hang
---

# Blue Screen of Death { #head }

## Reason 1: Anti-virus or third-party anti-cheat softwares { #anticheat }

Thinking about the fundamentals of Noble 3, clash with anti-virus or anti-cheat software is inevitable.

Those anti-cheat programs are known as causing inference with Noble 3. Please remove them before using Noble 3.

* EasyAntiCheat (EAC)
* BattlEye
* FaceIT Client
* ESEA
* Vanguard

## Reason 2: NobleShield { #nobleshield }

The NobleShield feature works on Windows kernel level, using driver to bypass and spoof the game's anticheat system.
As this nature, trivial errors or problems in NobleShield may cause the whole system to crash.

NobleShield feature only supports Windows 10, 2004, ≥ 20H2 and Windows 11 ≤ 22H2.

Unsupported Windows version may cause NobleShield to crash the system.

???+ tip "How to check the current Windows version"
    You can check your Windows version by using `winver.exe` tool.

    As it's integrated in Windows installation, you can run this tool by just opening Run(`Win+R`) utility and enter`winver` then press enter.
