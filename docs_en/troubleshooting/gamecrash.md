---
icon: svgrepo/crash
tags:
  - Crash
  - Hang
  - BlackScreen
---

# Game Crash { #head }

???+ danger "Blizzard Error"
    <figure><img src="../../img/crash_error.png" alt=""><figcaption><p>Image from https://imgur.com/ArKbXMb</p></figcaption></figure>

    When the game crashes and then Blizzard Error window is shown up,  
    <span class="very-important-info">you should UNCHECK the 'Send to Blizzard' checkbox before closing the window!</span>

    As Noble 3 is a internal product, when the game is crashed and the crash dump file (process memory dump file) is made, chances are that there are some 'weird' information that can be used to identify the existence of Noble 3.

    If you won't uncheck the 'Send to Blizzard' checkbox, the dump file is sent to Blizzard and they might ban your account using that information!

!!! warning "Noble 3 automatically 'crashes' when the session time is ended. This is a perfectly normal behavior."

## Cause analysis { #analysis }

If the game is crashed during usage mostly its cause is the session time is ended.

But for some users, the game may crash during usage, even if the session time has not ended.

As Noble 3 modifies game process memory, the chance of something going wrong with its internal code is can't be excluded.

## Resolution that is NOT recommended { #not-recommended }

The high road way to check the crash reason is to compare between Noble 3 injected and not injected into the game.

But to use this way you should launch the game without Noble 3 injected, which may allow the game to find the traces of Noble 3 <span class="important-info">because there are no security features loaded.</span>
And it may eventually result in an account ban or flag.

## Recommended resolution { #recommended }

The best way is, to contact the seller or open a ticket on the official Discord.

You can help the developer to address the problem by attaching error log and crash dump file when reporting to the staff. (1)
{ .annotate }

1. The error log(`.txt` file) and crash dump files(`.dmp` file) are stored in `This PC\Documents\Overwatch\Logs\Errors`[^1] folder by default.

<figure><img src="../../img/crash_dumpfile.png" alt=""><figcaption><p>Image from https://imgur.com/3gjnEMl</p></figcaption></figure>

[^1]: Absolute path: `%UserProfile%\Documents\Overwatch\Logs\Error`
