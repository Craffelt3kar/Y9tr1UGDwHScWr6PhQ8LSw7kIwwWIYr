---
description: 노블 3은 깔끔한 UI와 매우 다양한 기능들, 뛰어난 바이패스를 자랑하는 인터널 치트입니다.
icon: material/information-box
---

# Noble 3 소개 { #head }

!!! info "노블 한국 공식 판매처"
    한국에서의 구매는 [여기 서버로!](https://discord.gg/Qr2FdWFaSd)

    구매 문의는 도도(hayang._.) 님께 DM!

<img src="img/noble3.png" alt="Noble 3 logo" class="large-padding center"/>

노블 3은 Intel, AMD CPU를 모두 지원합니다.

Windows 10, 11 전 버전이 지원됩니다! 단 NobleShield 기능의 경우 Windows 10 20H2 이상 버전, Windows 11 22H2 이하 버전만이 지원됩니다.

## 알아두세요 { #notice }

* 노블 쉴드 (NobleShield) 기능의 경우, Windows 10의 경우 20H2 이상 전 버전, Windows 11의 경우 22H2 이하 버전만을 지원합니다!  
  이외의 버전 사용 시 인젝트 과정에서 블루스크린이 뜨는 등 문제가 발생할 수 있습니다!

* 노블은 <span class="important-info">인터널 방식 치트</span>입니다. 즉, 모든 치트 로직은 게임 프로세스 메모리 상에 직접 인젝트됩니다.  
  이 덕분에 노블은 더 정확한 에임봇, 사일런트 에임 기능 등 여러 강점들을 가지고 있습니다.  
  또한, 일반적인 인터널 치트에 대한 인식과는 달리 노블은 우회가 매우 뛰어나 풀 익스터널급의 보안성 및 안전성을 가지고 있습니다.

## 보안 기능 { #security-features }

* **오토 스푸퍼 (Auto-spoofer)**  
  오버워치가 수집하는 몇몇 고유 정보들을 속이거나 제거하여 하드밴을 피할 수 있도록 돕습니다.

* **노블 쉴드 (NobleShield)**  
  커널 모드 드라이버를 통해 오버워치의 안티 치트가 노블의 존재를 감지하지 못하도록 합니다.

* **전역 스푸퍼 실행 (Run Full Spoofer)**  
  Battle.net과 오버워치가 컴퓨터에 남겨놓은 고유 ID 파일들을 삭제하고 하드밴과 관련된 여러 ID들이나 이름들을 바꿉니다 (컴퓨터 이름, 하드 시리얼 등...)

## 지원 기능 { #features }

* :material-target: 에임봇
  * 2가지의 보조키 지원: 헤드, 가까운 히트박스
  * 여러 가지 스무스 알고리즘 지원
  * 간단한 설정, 고급 설정 선택 가능
  * 에임봇 FoV 지정 가능 (십자선에서 일정한 거리 내의 적들만 조준)
  * 시메트라 포탑, 토르비욘 포탑 등 오브젝트 조준 지원
  * 레이지 모드 지원 (유도탄)
  * 끌어치기, 트래킹 에임 모두 지원
  * 벽 검사, 방벽 검사 지원
  * 목표 모드 설정 지원: 십자선에 가장 가까운 적, 거리가 가장 가까운 적, 체력이 가장 적은 적 등

* :svgrepo-visuals: ESP
  * :material-human: 아웃라인 ESP (테두리 ESP, 흔히 테두리 월핵이란 뜻에서 테월이라 불림)
  * :material-human: 박스 ESP
  * :fontawesome-solid-chart-bar: 체력바 ESP
  * :material-av-timer: 스킬 쿨타임, 궁극기 충전률 ESP
  * :material-target: 에임봇 조준 목표 ESP
  * :material-palette: 모든 ESP 요소들은 직접 색상 설정 가능합니다.

* :material-share: 쉬운 설정 프로필 공유
  * :material-content-save: 로더와 동일한 폴더에 각 영웅 이름별로 설정 파일이 XML로 저장됩니다.

* :svgrepo-fist-variant: 자동 근접 공격
  * :material-target: 자동 근접 공격에 에임봇 내장

* :material-target-variant: 스킬 도우미
  * <img src="img/hero/hero_dva.png" alt="" class="img-line"/> D. va 자동 방어 매트리스
  * <img src="img/hero/hero_doomfist.png" alt="" class="img-line"/> 둠피스트 로켓 펀치
  * <img src="img/hero/hero_ramattra.png" alt="" class="img-line"/> 라마트라 평타, 네메시스 형태 지원
  * <img src="img/hero/hero_reinhardt.png" alt="" class="img-line"/> 라인하르트 자동 방벽, 화염 강타
  * <img src="img/hero/hero_wreckingball.png" alt="" class="img-line"/> 레킹볼 자동 적응형 보호막
  * <img src="img/hero/hero_roadhog.png" alt="" class="img-line"/> 로드호그 갈고리, 자동 숨돌리기
  * <img src="img/hero/hero_sigma.png" alt="" class="img-line"/> 시그마 강착, 자동 키네틱 손아귀
  * <img src="img/hero/hero_orisa.png" alt="" class="img-line"/> 오리사 투창, 자동 방어 강화
  * <img src="img/hero/hero_winston.png" alt="" class="img-line"/> 윈스턴 우클릭
  * <img src="img/hero/hero_zarya.png" alt="" class="img-line"/> 자리야 자동 자기방벽, 주는방벽
  * <img src="img/hero/hero_junkerqueen.png" alt="" class="img-line"/> 정커퀸 톱니칼
  * <img src="img/hero/hero_genji.png" alt="" class="img-line"/> 겐지 자동 질풍참, 자동 튕겨내기, 용검
  * <img src="img/hero/hero_reaper.png" alt="" class="img-line"/> 리퍼 자동 망령화
  * <img src="img/hero/hero_mei.png" alt="" class="img-line"/> 메이 우클릭, 자동 급속 빙결
  * <img src="img/hero/hero_sojourn.png" alt="" class="img-line"/> 소전 (수동)우클릭, 자동 우클릭
  * <img src="img/hero/hero_soldier76.png" alt="" class="img-line"/> 솔저: 76 나선 로켓, 자동 생체장
  * <img src="img/hero/hero_sombra.png" alt="" class="img-line"/> 솜브라 자동 해킹-바이러스 콤보, 자동 EMP
  * <img src="img/hero/hero_symmetra.png" alt="" class="img-line"/> 시메트라 우클릭
  * <img src="img/hero/hero_ashe.png" alt="" class="img-line"/> 애쉬 다이너마이트 조준, 자동 충격 샷건
  * <img src="img/hero/hero_echo.png" alt="" class="img-line"/> 에코 점착 폭탄, 광선 집중
  * <img src="img/hero/hero_junkrat.png" alt="" class="img-line"/> 정크랫 자동 충격 지뢰
  * <img src="img/hero/hero_cassidy.png" alt="" class="img-line"/> 캐서디 자동 구르기
  * <img src="img/hero/hero_tracer.png" alt="" class="img-line"/> 트레이서 자동 근접공격 콤보(트풍참) 및 점멸 부착 콤보, 자동 역행
  * <img src="img/hero/hero_lucio.png" alt="" class="img-line"/> 루시우 자동 분위기 전환
  * <img src="img/hero/hero_baptiste.png" alt="" class="img-line"/> 바티스트 자동 치유 파동
  * <img src="img/hero/hero_brigitte.png" alt="" class="img-line"/> 브리기테 자동 밀치기, 자동 도리깨
  * <img src="img/hero/hero_zenyatta.png" alt="" class="img-line"/> 젠야타 우클릭 모아쏘기, 자동 조화 및 부조화

## 공식 영문 홍보 자료 { #pr }

<img src="img/noble3-1.gif" alt="Noble PR 1" alt="" class="continuous-img"/>
<img src="img/noble3-2.gif" alt="Noble PR 2" alt="" class="continuous-img"/>
<img src="img/noble3-3.png" alt="Noble PR 3" alt="" class="continuous-img"/>

## 설명서 제작자 { #manual-author }

by RS-232C (._.headl3ss)

자유롭게 퍼가고, 사용하고, 수정하셔도 됩니다.
단, 라이센스인 CC-BY-SA 4.0에 규정된 내용을 준수해 주세요.
수정 후 재배포 시 DM 한번씩 부탁드립니다.

설명서 소스는 화면 왼쪽 위 레포지터리에서 볼 수 있습니다.

설명서에서 잘못된 정보나 오류, 오타 등을 발견 시 디스코드로 DM을 주시거나, 위 소스 코드 레포지터리에서 Issue 또는 Merge Request를 제출해 주세요.

## 설명서 라이센스 { #license }

<a href="https://creativecommons.org/licenses/by-sa/4.0/deed.ko" target="_blank"><img src="img/200_by-sa.png" alt="CC-BY-SA 4.0"/></a>

---

Noble 3 설명서 by RS-232C (._.headl3ss)

Noble 3 설명서 is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <https://creativecommons.org/licenses/by-sa/4.0/>.
