---
icon: material/download-box
---

# 노블 설치하기 { #head }

## 사용 전 체크리스트 { #checklist }

노블 로더를 다운로드받고, 실행하고, 노블을 인젝트하기 전에 반드시 체크리스트를 읽고 모든 사항을 제대로 처리하였는지 확인하여야 합니다.

<div class="center" markdown>
[:material-check-all: 설치 및 사용 전 체크리스트](checklist.md){ .md-button }
</div>

## 노블 로더 다운로드 { #download }

노블을 게임에 로드해 사용하기 위해서는 먼저 노블 로더를 다운로드받으셔야 합니다.

<div class="center" markdown>
[:material-file-download: 노블 로더 다운로드](https://cdn.discordapp.com/attachments/1088998470364438628/1144599801627955251/Noble3_25082023.rar?ex=65e1c122&is=65cf4c22&hm=63fcd0d16cbe354b26cc27f7d45a4ed88ea9e885f5a211efb1c207a2c28e2d24&){ .md-button }
</div>

!!! hint "노블 로더는 신규 버전이 사용 가능할 시 자동으로 업데이트됩니다."
    이 덕분에 타 제품들과 달리 매 패치마다 신규 파일을 다시 다운로드받지 않아도 됩니다.

## 노블 로더 압축 해제 { #unarchive }

로더 파일은 RAR 포맷으로 압축되어 있습니다.
압축 해제를 위해서는 반디집, 7-zip, WinRAR 등과 같은 프로그램을 사용하시면 됩니다.

## 노블 로더를 *관리자 권한으로* 실행 { #run }

??? warning "노블 로더 파일 이름을 변경하면 안 됩니다!"
    파일 이름 변경 시, 노블 로더를 실행하면 오류 메시지가 나타납니다.

만약 새로운 버전의 로더가 사용 가능하다면, 이 때 자동으로 다운로드되며, 다운로드가 완료되면 자동으로 로더가 다시 시작됩니다.

커맨드 프롬프트 창이 잠깐 떴다가 사라질 것입니다. 만약 로딩 과정에서 문제가 있었다면 이 창에 오류 메시지가 나타납니다.

### 시작 오류 메시지와 해결 방법 { #startup-error }

|메시지|발생 원인|해결 방법|
|:-:|:-:|:-:|
|`Already waiting for the game`|이미 인젝트가 완료되었고, 게임이 실행되기롤  기다리고 있는 상태입니다.|로더 창을 완전히 닫고 나서 게임을 실행합니다.|
|`Multiple instance not allowed`|이미 노블 로더가 실행 중인 상태에서 로더를 다시 한번 더 실행시키려고 시도하였습니다.|이미 실행 중인 노블 로더 창을 닫고 나서 다시 시도해 봅니다. (만약 로더 창이 보이지 않는다면 `taskkill -f -im NobleLoader.exe` 명령으로 강제종료를 시도해 봅니다)|
|`Invalid loader file name`|로더 파일 이름이 `NobleLoader.exe`이 아닙니다.|로더 파일 이름을 `NobleLoader.exe`로 다시 변경합니다.|

이외의 오류 메시지가 뜰 시 또는 위에 제시된 해결 방법으로 오류가 해결이 되지 않는 경우, 판매자에게 문의를 남기거나 티켓을 열어 주세요.

## 라이센스 키를 입력하여 로그인 { #login }

아래와 같은 로그인 창이 뜨면 라이센스 키를 입력하고 Login 버튼을 클릭합니다

<figure><img src="../img/loader/loader_login.png" alt=""><figcaption><p>로그인 창</p></figcaption></figure>

## 노블 인젝트 { #inject }

아래와 같은 인젝트 창이 나타나면 Auto spoofer, Use NobleShield 체크박스를 체크하고 Run Cheat 버튼을 클릭합니다

<figure><img src="../img/loader/loader_load.png" alt=""><figcaption><p>인젝트 창</p></figcaption></figure>

## 노블 인젝트 진행 중... { #inject-progress }

또 다시 아래와 같이 커맨드 프롬프트 창이 떴다가 잠시 후 사라질 것입니다.

<figure><img src="../img/loader/loader_injectcmd.png" alt=""><figcaption><p>인젝션 진행 중</p></figcaption></figure>

해당 창 `Download License...` 문장 다음에 `Server Error ...` 하면서 오류가 나타나는 경우가 있습니다.

대부분의 경우는 인터넷 연결 관련 문제이며, 몇 번 다시 인젝트를 시도하면 보통 해결됩니다.  
그러나 여러 번 시도 후에도 오류가 유지될 경우, 본인의 인터넷 연결이 불안정하거나 노블 측의 서버에 문제가 생겼을 확률이 큽니다.  
이 경우에도 판매자에게 문의를 남기거나 티켓을 열어 주세요.

<p class="very-important-info">해당 커맨드 창이 완전히 닫힌 것을 확인한 후, Battle.net과 오버워치를 실행해야 합니다.</p>

## 게임 실행 { #launch-game }

게임 실행 시 자동으로 노블이 로드됩니다.

<figure><img src="../img/loader/loader_autoloaded.png" alt=""><figcaption><p>자동으로 노블이 로드된 모습</p></figcaption></figure>

???+ warning "module outdated. wait for update"
    게임 버전이 업데이트되었지만, 노블이 아직 이에 발맞추어 업데이트되지 않은 상태인 경우 게임 실행 시 `module outdated. wait for update`라는 오류 메시지와 함께 게임이 종료됩니다.

    일반적으로 업데이트에는 6시간 ~ 1일 사이의 시간이 소요되며, 업데이트 완료 시 그 동안 사용하지 못했던 시간은 자동으로 보상됩니다.
