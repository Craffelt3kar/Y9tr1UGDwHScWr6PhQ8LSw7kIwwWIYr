---
description: Aimbot 탭 - 캐스트 에임봇
icon: svgrepo/cast
tags:
    - Aimbot
    - Cast
---

# 캐스트 에임봇 { #head }

-8<- "shared_aimbot_cast_animation.ext"

=== "영어"
    <img src="../../../img/menu/menu_aimbot_adv_cast_en.png" alt="캐스트 에임봇 탭 (고급 설정 모드)" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_aimbot_adv_cast_ko.png" alt="캐스트 에임봇 탭 (고급 설정 모드)" class="center"/>

'Smooth Cast'라는 옵션이 하나 더 추가되었다는 점을 제외하면, 나머지 옵션들은 모두 [플릭 에임봇](flick.md)과 동일합니다.

## Smooth Cast (스무스 캐스트) { #smooth-cast }

캐스트 이전 에임봇 속도(스무스) 조정값입니다.
'빡' 하고 고정하기 전에 천천히 목표를 따라갈 때 에임의 속도를 지정할 수 있습니다.

## [나머지 설정들은 '고급 옵션' 단락 참고](index.md#advanced-settings)

## 참고영상 { #showcase }

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/FS5-7oQ9vhQ" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
