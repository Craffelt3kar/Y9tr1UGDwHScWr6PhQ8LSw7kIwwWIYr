---
description: Aimbot 탭 - 플릭 에임봇
icon: svgrepo/target_2
tags:
    - Aimbot
    - Flick
---

# 플릭 에임봇 { #head }

-8<- "shared_aimbot_flick_animation.ext"

=== "영어"
    <img src="../../../img/menu/menu_aimbot_adv_flick_en.png" alt="플릭 에임봇 탭 (고급 설정 모드)" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_aimbot_adv_flick_ko.png" alt="플릭 에임봇 탭 (고급 설정 모드)" class="center"/>

### Smooth Mode (스무스 모드) { #smooth-mode }

사용할 스무스 알고리즘을 선택할 수 있습니다.

* Classic - 처음에 천천히 가속, 목표에 가까워지면 감속하는 전형적인 스무스 알고리즘입니다.
* Fast - 처음에 가속, 목표에 가까워져도 거의 감속하지 않는 스무스 알고리즘입니다.
* Modern - 깔끔하면서도 정확한 조준을 지원하는 스무스 알고리즘입니다.

!!! warning "'Modern' 스무스 모드 사용 시 Smooth, Smooth Accelerate 값이 _높을수록_ 조준 속도가 빨라집니다!"

### Fast Shot (더 빠른 발사) { #fast-shot }

샷을 쏘고 나서 다음 샷을 쏠 수 있을 때까지 기다리는 시간을 단축할지 말지 여부를 선택하는 옵션입니다.

캐서디나 에코 같은 캐릭터 할 때 의심받지 않으려면 활성화하는 것을 추천합니다.

이렇게 이해하면 됩니다:

Off - 샷을 쏘고 나서 다음 샷을 쏠 수 있을 때까지 기다린 후, 마침내 다음 샷을 쏠 수 있게 되었을 때가 되어서야 다시 에임봇이 작동하기 시작합니다.

On - 샷을 쏘고 나서 다음 샷을 쏠 수 있을 때까지 기다릴 때, 다음 샷을 쏠 수 있게 되기 0.2초 정도 전부터 에임봇이 다시 작동하기 시작합니다.

### Smooth (스무스) { #smooth }

베이스 에임 속도 조정값입니다.
값이 _낮을수록_ 기본 조준 속도가 빨라지고 _높을수록_ 기본 조준 속도가 느려집니다.

!!! warning "단, 'Modern' 스무스 모드에서는 값이 _높을수록_ 기본 조준 속도가 빨라집니다."

이 '기본 조준 속도'라는 것은 에임의 일정한 베이스 속도를 말하는 것이며, 이 일정한 베이스 속도에 가감속이 더해지고, 거리에 따른 추가 속도 보정값이 더해지면서 스무스 알고리즘이 완성됩니다.

### Smooth Accelerate (스무스 가속력) { #smooth-accelerate }

에임 가/감속 조정값입니다.
값이 _낮을수록_ 가속과 감속이 없어 에임 움직이는 속도가 전반적으로 일정해지고 값이 _높을수록_ 에임이 가속되고 감속되며 부드러워집니다.

이 때, 이 값은 에임의 감속보다는 가속에 더 큰 영향을 미칩니다. 즉, 이 값이 크면 클수록 시작할 때 에임이 천천히 가속되며, 작을수록 에임이 멈춰있다가 갑자기 움직이는 것처럼 보입니다.

이 값이 많이 클 경우, 에임의 전반적인 속도가 감소합니다. (처음 시작할 때 가속을 많이 못 받고 시작한 것이 쭉 이어지기 때문입니다)

!!! warning "단, 'Modern' 스무스 모드에서는 값이 _높을수록_ 가속의 정도가 강해집니다."

### Smooth Balance (스무스 발란스) { #smooth-balance }

!!! info "이 옵션은 'Modern' 스무스 모드에서는 비활성화됩니다."

스무스와 가속 사이의 비율 조정값입니다.
이 때 스무스는 [Smooth 옵션](flick.md#smooth---에임-속도)을 말하고, 가속은 [Smooth Accelerate 옵션](flick.md#smooth-accelerate---에임-가속값)을 말합니다.

즉, 전체 에임에서 가속이 몇 %를 차지하는지를 정하는 것이라 생각하면 됩니다.

기본적으로 값 N은 스무스 알고리즘이 'N% 가속 + (100-N)% 스무스'를 이용하는 것을 의미합니다.

예시: 값 25 = 25% 가속 + 75% 스무스

### Smooth Clamp (스무스 클램프) { #clamp }

에임봇을 최대 주어진 시간(밀리초 단위)까지만 작동시킵니다.

예를 들어, 값이 100이면 목표를 최대 100ms동안 조준하려 시도한 후 여전히 에임이 목표에 닿지 않았다면 그 자리에서 에임봇을 비활성화하고 총을 발사합니다.

기본적으로 트래킹 현상(총을 쏘지 않고 에임이 끊임없이 목표를 따라가는 현상)이 일어나는 것을 방지하기 위한 옵션이나, 이외에도 아른 여러 용도로 사용될 수 있습니다.

-8<- "aimbot_0_disable.ext"

<figure><img src="../../../img/showcase/showcase_smoothclamp_high.gif" alt=""><figcaption><p>Clamp값이 높을 때</p></figcaption></figure>

<figure><img src="../../../img/showcase/showcase_smoothclamp_low.gif" alt=""><figcaption><p>Clamp값이 낮을 때</p></figcaption></figure>

## [나머지 설정들은 '고급 옵션' 단락 참고](index.md#advanced-settings)

## 참고영상 { #showcase }

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/t87w3AV8xrU" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/HtWcI4sGYtU" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
