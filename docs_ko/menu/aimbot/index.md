---
description: 이 탭에서는 주 무기 에임봇을 상세하게 설정할 수 있습니다.
icon: material/target
tags:
    - Aimbot
    - Triggerbot
    - AimAssist
---

# Aimbot 탭 { #head }

!!! info "벽뒤 체크, 은신 솜브라 체크"
    노블의 기본 에임봇은 절대로 벽 뒤에 있는 적이나 은신 중인 솜브라를 조준하거나 쏘지 않습니다.

    단, 레이지 에임봇의 경우는 예외적으로 은신 상태인 솜브라도 조준합니다.

## 공통 설정 { #common-settings }

쉬운 설정, 고급 설정 모드에 모두 공통적으로 적용되는 옵션들에 대한 설명입니다.

=== "영어"
    <img src="../../img/menu/menu_aimbot_easy_en.png" alt="에임봇 탭 (쉬운 설정 모드)" class="center"/>

=== "한국어"
    <img src="../../img/menu/menu_aimbot_easy_ko.png" alt="에임봇 탭 (쉬운 설정 모드)" class="center"/>

### Enabled (켜짐) { #enabled }

에임봇 활성화 여부를 지정합니다.

### Advanced Mode (고급 옵션)  { #advanced-mode }

[고급 설정 모드](#advanced-settings)를 사용할지, 쉬운 설정 모드를 사용할지 여부를 선택합니다.

[고급 설정 모드](#advanced-settings)에서는 더 많은 옵션들을 통해 에임봇을 더욱 정교하게 설정할 수 있습니다.

### Head Key (머리 에임봇) { #head-key }

헤드 조준 키입니다.
이 키를 누를 시 에임봇이 현재 목표의 헤드 부위를 조준해 줍니다.

-8<-
aimbot_keybind.ext
aimbot_click2flick.ext
-8<-

### Nearest Key (가까운뼈 에임봇) { #nearest-key }

가까운 히트박스 조준 키입니다.
이 키를 누를 시 에임봇이 현재 목표에서 십자선과 가장 가까운 부위(헤드, 몸통, 팔, 다리, ...)를 골라서 조준해 줍니다.

-8<-
aimbot_keybind.ext
aimbot_click2flick.ext
-8<-

### FOV (에임봇 범위) { #fov }

에임봇의 시야각(FoV) 제한값입니다.
에임봇은 십자선으로부터 주어진 거리 안에 들어온 대상들만 조준 대상으로 삼습니다.

이해를 돕자면, 십자선을 중심으로 하는 원을 그린다고 생각하면 됩니다.

<img src="../../img/fov_explained.jpg" alt="FOV 옵션 설명 사진" class="center"/>

이 원 안에 들어온 대상들만 조준의 대상이 됩니다.
이 옵션은 이 원의 *반지름*을 지정하는 것이라 생각하면 됩니다.

슬라이더를 완전히 오른쪽으로 밀어 값을 360으로 지정하면 FoV 제한을 완전히 끌 수 있습니다.

### Smooth (스무스) { #smooth }

에임봇 속도(스무스) 조정값입니다.
값이 **낮을수록** 빠르고 정확하게 목표를 조준하며, 값이 **높을수록** 천천히 부드럽게 목표를 조준합니다.

## Advanced Mode: 고급 옵션 { #advanced-settings }

=== "영어"
    <img src="../../img/menu/menu_aimbot_adv_flick_en.png" alt="에임봇 탭 (고급 설정 모드)" class="center"/>

=== "한국어"
    <img src="../../img/menu/menu_aimbot_adv_flick_ko.png" alt="에임봇 탭 (고급 설정 모드)" class="center"/>

노블의 에임봇은 크게 3 가지 종류로 나뉘며, 각 에임봇마다 설정 페이지에 약간의 차이가 존재합니다.

<span class="important-info">이 페이지에서는 이 3가지 에임봇들에 대해 공통으로 적용되는 옵션들만을 다룹니다.</span> 각 에임봇의 차이점은 [에임봇 타입](types.md) 페이지를, 각 에임봇별로 적용되는 고유 옵션들은 각 에임봇에 대한 설명 문서를 참고하세요.

### Target Mode (타겟 모드) { #target-mode }

에임봇이 목표를 선정할 선정 기준을 지정합니다.

* Crosshair - 십자선에 가장 가까운 대상을 목표로 선택합니다.

* Smart - 십자선과의 거리, 목표와의 직접적인 거리를 종합적으로 고려하여 능동적으로 목표 대상을 선정합니다.

* Distance - 직접적인 거리가 가장 가까운 대상을 목표로 선택합니다.

* Health - 체력이 가장 낮은 대상을 목표로 선택합니다.

### Target Objects (오브젝트 조준) { #target-objects }

활성화 시 토르비욘의 포탑, 시메트라의 포탑과 같은 오브젝트들을 조준 대상에 포함시킵니다.

???+ info "조준 대상 목록"
    * <img src="../../../img/hero/hero_roadhog.png" alt="" class="img-line"/>로드호그 돼지우리<img src="../../../img/hero/hero_roadhog_e.png" alt="" class="img-line"/>
    * <img src="../../../img/hero/hero_torbjorn.png" alt="" class="img-line"/>토르비욘 포탑<img src="../../../img/hero/hero_torbjorn_shift.png" alt="" class="img-line"/>
    * <img src="../../../img/hero/hero_symmetra.png" alt="" class="img-line"/>시메트라 감시 포탑<img src="../../../img/hero/hero_symmetra_shift.png" alt="" class="img-line"/>, 텔레포터<img src="../../../img/hero/hero_symmetra_e.png" alt="" class="img-line"/>
    * <img src="../../../img/hero/hero_widowmaker.png" alt="" class="img-line"/>위도우메이커 맹독 지뢰<img src="../../../img/hero/hero_widowmaker_e.png" alt="" class="img-line"/>
    * <img src="../../../img/hero/hero_baptiste.png" alt="" class="img-line"/>바티스트 불사 장치<img src="../../../img/hero/hero_baptiste_e.png" alt="" class="img-line"/>
    * <img src="../../../img/hero/hero_illari.png" alt="" class="img-line"/>일리아리 태양석<img src="../../../img/hero/hero_illari_e.png" alt="" class="img-line"/>

### Shield Check (보호막 구분) { #shield-check }

방벽 검사를 활성화할지의 여부입니다.

활성화 시 단순히 목표가 벽 뒤에 있는지 검사할 뿐만 아니라 목표가 방벽 뒤에 있는지의 여부도 검사합니다.
만약 목표가 방벽 뒤에 있다면, 벽 뒤에 있는 것과 똑같이 취급하고 조준 대상으로 삼지 않습니다.

### Dynamic FOV (다이나믹 포브) { #dynamic-fov }

FoV를 능동적으로 조정할 지의 여부입니다.

활성화 시 가까이 있는 목표는 십자선과의 거리가 멀더라도 조준하고, 멀리 있는 목표는 십자선과의 거리가 가까워야지만 조준 대상으로 삼습니다.

'십자선으로부터 원을 그려서 그 안에 들어온 목표를 조준'하는 대신, '모든 목표의 위치에 원을 그리고, 그 중 십자선이 범위 안에 들어가 있는 원이 있다면 그 대상을 조준'한다고 보면 됩니다.

당연히, 해당 '목표 위치에 그려진 원'은 목표와 플레이어 간 거리에 따라 크기가 변합니다.

<img src="../../img/dynamic_fov.jpg" alt="Dynamic FOV 옵션 설명 사진" class="center"/>

목표와의 거리에 따라 FOV가 유동적으로 적용된다고 생각하면 이해하기 편합니다.

### Vertical Fix (버티컬 픽스) { #vertical-fix }

조준 위치 수직 보정값입니다.
값이 커질수록 목표의 더 윗부분을, 작아질수록 더 아랫부분을 조준합니다.

예시로, 의심을 피하기 위해 헤드를 맞추는 것을 피하고 싶다면 값을 *-200* 정도로 낮게 설정하면 됩니다.

또, 헤드를 잘 맞추고 싶다면 값을 50-80 정도로 높게 설정해 주면 됩니다.
위도우메이커와 같이 헤드 히트박스가 상하로 길쭉한 형태를 띨 경우, 120-180 정도의 높은 값을 넣어주면 거의 헤드만을 때리게 됩니다.

단, 헤드를 잘 맞추기 위해 값을 올릴 때, *100* 이상으로 과하게 값을 높일 시 몇몇 영웅들에 대해(앉아 있는 아나, 메이 등) 목표의 헤드를 조준하고 쏘는 대신 헤드 위 허공에 에임이 고정된 채 쏘지도 않고 계속 따라가 의심을 받을 수 있습니다.

### Max Range (최대 범위) { #max-range }

에임봇이 목표로 삼을 적과의 최대 거리입니다.
이 거리 밖에 있는 목표들은 조준 대상으로 삼지 않습니다.

예시로, 값이 *30*이라면, 30m보다 멀리 있는 목표들은 조준하지 않습니다.

-8<- "aimbot_0_disable.ext"

## 참고 사항 { #notice }

노블은 인터널 치트이기에, 에임봇의 스무스 알고리즘이 게임 fps에 영향을 받습니다.

자세한 사항은 [fps가 에임봇에 주는 영향](../../tips/aimbot-fps-affect.md)을 참고하세요.
