---
description: Aimbot 탭 - 트래킹 에임봇
icon: material/magnet
tags:
    - Aimbot
    - Track
---

# 트래킹 에임봇 { #head }

-8<- "shared_aimbot_track_animation.ext"

=== "영어"
    <img src="../../../img/menu/menu_aimbot_adv_track_en.png" alt="트래킹 에임봇 탭 (고급 설정 모드)" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_aimbot_adv_track_ko.png" alt="트래킹 에임봇 탭 (고급 설정 모드)" class="center"/>

## Spray Mode (탄퍼짐 모드) { #spray-mode }

*'탄퍼짐' 과는 상관이 없고 (오역),* 조준 도중 총 발사(스프레이) 모드를 선택하는 옵션입니다.

대개 키를 좌클릭에 할당하고 사용하는 경우가 많기에 잘 쓰이지 않는 옵션이나,
키를 마우스 옆쪽의 보조키에 할당하고 사용하는 경우와 같이 발사 버튼과 에임봇 키가 다른 경우 자동으로 조준과 동시에 자동으로 총을 지속적으로 발사해 주는 역할을 하는 옵션입니다.

* Always - 보조키를 누른 상태에서 목표를 감지하면 항상 발사해 줍니다.

* OnAiming - 에임봇이 조준 중일 때 항상 발사해 줍니다.

* None - 총을 자동으로 발사해 주지 않습니다.

## Acquire Target Delay (새 대상 탐지 딜레이) { #acquire-target-delay }

에임봇 목표가 사라진 상태에서, 다음 목표를 조준하기 전까지 기다릴 시간을 정합니다. (밀리초 단위)

예를 들어, 값이 1000ms이라면 한 목표를 처치한 후 1초가 지나서야 그 옆에 다른 목표를 조준하기 시작합니다.

-8<- "aimbot_0_disable.ext"

## [나머지 설정들은 '고급 옵션' 단락 참고](index.md#advanced-settings)

## 참고영상 { #showcase }

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/gt7IW7XcxJ0" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
