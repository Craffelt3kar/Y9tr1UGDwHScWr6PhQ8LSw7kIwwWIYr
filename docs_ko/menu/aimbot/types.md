---
description: 노블 3이 지원하는 에임봇은 크게 3 가지 종류로 나눌 수 있습니다.
icon: material/exclamation-thick
tags:
    - Aimbot
    - Types
---

# 에임봇의 종류 { #head }

## [트래킹 에임봇](track.md) - '트래킹 에임법' 에임봇 { #track }

-8<- "shared_aimbot_track_animation.ext"

보조키를 누르고 있는 상태에서 에임봇이 지속적으로 에임을 목표에 가져다 대 줍니다.

솔저: 76, 트레이서, 바스티온, 소전, 솜브라 등과 같은 '트래킹 에임법'이 필요한 영웅들의 에임봇이 대표적입니다.

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/gt7IW7XcxJ0" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

## [플릭 에임봇](flick.md) - '끌어치기 에임법' 에임봇 { #flick }

-8<- "shared_aimbot_flick_animation.ext"

보조키를 누르면 자동으로 목표를 조준한 후 에임이 목표에 닿으면 쏴 줍니다.

캐서디, 애쉬, 위도우메이커, 한조, 아나 등과 같은 '끌어치기 에임법'이 필요한 영웅들의 에임봇이 대표적입니다.

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/t87w3AV8xrU" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

## [캐스트 에임봇](cast.md) { #cast }

-8<- "shared_aimbot_cast_animation.ext"

일정 시간동안 에임으로 천천히 목표를 따라가다가 한 순간 확실히 '빡' 고정하며 쏩니다.

겐지, 바티스트 평타, 젠야타 우클릭 차징샷, 아나 수면총과 같은 선딜레이가 있거나, 점사 공격인 기술들의 에임봇이 대표적입니다.

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/FS5-7oQ9vhQ" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
