---
pngicon: ../../../img/hero/hero_ana.png
---

# 아나 { #head }

<img src="../../../img/hero/hero_ana.png" alt="" class="hero-portrait round-edge center"/>

## Aimbot (생체 소총 에임봇) { #aimbot }

원래의 [Aimbot 탭](../aimbot/flick.md) 설정에 더해 'Scoped Config (조준 모드 설정 키기)' 옵션이 추가되었습니다.

=== "영어"
    <img src="../../../img/menu/menu_ana_aimbot_en.png" alt="" class="center"/>

=== "영어 (Scoped Config 켜짐)"
    <img src="../../../img/menu/menu_ana_aimbot_scopedconfig_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_ana_aimbot_ko.png" alt="" class="center"/>

=== "한국어 (조준 모드 설정 켜짐)"
    <img src="../../../img/menu/menu_ana_aimbot_scopedconfig_ko.png" alt="" class="center"/>

-8<- "hero_scoped_config.ext"

-8<- "hero_tab_flick_aimbot.ext"

## SmartAna (스마트 아나 에임봇) { #smartana }

에임키 하나로 생체 소총을 이용하여 가까운 아군을 치료하고, 가까운 적을 공격합니다. (기본 에임봇은 아군 힐 키와 적 공격 키가 따로 나뉘어 있습니다)

이 탭 역시 기본 에임봇 탭과 같이 [Scoped Config (조준 모드 설정 키기)](#scoped-config) 옵션을 지원합니다

=== "영어"
    <img src="../../../img/menu/menu_ana_smartana_en.png" alt="" class="center"/>

=== "영어 (Scoped Config 켜짐)"
    <img src="../../../img/menu/menu_ana_smartana_scopedconfig_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_ana_smartana_ko.png" alt="" class="center"/>

=== "한국어 (조준경 설정 켜짐)"
    <img src="../../../img/menu/menu_ana_smartana_scopedconfig_ko.png" alt="" class="center"/>

-8<- "hero_scoped_config.ext"

-8<- "hero_tab_flick_aimbot.ext"

## \[Shift\] SleepDart (수면총) <img src="../../../img/hero/hero_ana_shift.png" alt="" class="img-line-bigger"/> `#에임봇(캐스트)` { #shift }

적응 향해 수면총을 쏘는 것을 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_ana_sleepdart_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_ana_sleepdart_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[E\] BioticGrenade (생체 수류탄) <img src="../../../img/hero/hero_ana_e.png" alt="" class="img-line-bigger"/> `#에임봇(플릭)` { #e }

아군 또는 적을 향해 생체 수류탄을 사용합니다.

=== "영어"
    <img src="../../../img/menu/menu_ana_bioticgrenade_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_ana_bioticgrenade_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"
