---
pngicon: ../../../img/hero/hero_ashe.png
---

# 애쉬 { #head }

<img src="../../../img/hero/hero_ashe.png" alt="" class="hero-portrait round-edge center" />

!!! note "다이너마이트 <img src="../../../img/hero/hero_ashe_e.png" alt="" class="img-line"/> 조준 지원됩니다."

## Aimbot (바이퍼 에임봇) { #aimbot }

원래의 [Aimbot 탭](../aimbot/flick.md) 설정에 더해 'Scoped Config (조준 모드 설정 키기)' 옵션이 추가되었습니다.

=== "영어"
    <img src="../../../img/menu/menu_ashe_aimbot_en.png" alt="" class="center"/>

=== "영어 (Scoped Config 켜짐)"
    <img src="../../../img/menu/menu_ashe_aimbot_scopedconfig_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_ashe_aimbot_ko.png" alt="" class="center"/>

=== "한국어 (조준 모드 설정 켜짐)"
    <img src="../../../img/menu/menu_ashe_aimbot_scopedconfig_ko.png" alt="" class="center"/>

-8<- "hero_scoped_config.ext"

-8<- "hero_tab_flick_aimbot.ext"

## \[Shift\] CoachGun (충격 샷건) <img src="../../../img/hero/hero_ashe_shift.png" alt="" class="img-line-bigger"/> `#자동스킬` `#에임봇(플릭)` `#회피형` { #shift-auto }

-8<- "hero_skill_autouse.ext"

적이 일정 거리 안으로 접근해 오면 자동으로 해당 적을 향해 조준하고 충격 샷건을 발사하여 거리를 벌립니다.

=== "영어"
    <img src="../../../img/menu/menu_ashe_coachgun_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_ashe_coachgun_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"
