---
pngicon: ../../../img/hero/hero_baptiste.png
---

# 바티스트 { #head }

<img src="../../../img/hero/hero_baptiste.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] BioticLauncher (생체탄 발사기) <img src="../../../img/hero/hero_baptiste_gun.png" alt="" class="img-line-bigger"/> `#에임봇(플릭)` { #rb-auto }

-8<- "hero_skill_manualuse.ext"

바라보고 있는 아군을 자동으로 조준하여 치유합니다. 에임키는 주 에임봇 (Aimbot 탭)과 공유합니다.

=== "영어"
    <img src="../../../img/menu/menu_baptiste_bioticlauncher_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_baptiste_bioticlauncher_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

## \[Shift\] RegenerativeBurst (치유의 파동) <img src="../../../img/hero/hero_baptiste_shift.png" alt="" class="img-line-bigger"/> `#자동스킬` `#케어형` { #shift-auto }

-8<- "hero_skill_autouse.ext"

범위 내의 나 자신을 포함한 아군들 중 한 명이라도 체력이 지정된 임계값보다 낮아질 경우 자동으로 치유 파동을 사용하여 치유합니다.

=== "영어"
    <img src="../../../img/menu/menu_baptiste_regenerativeburst_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_baptiste_regenerativeburst_ko.png" alt="" class="center"/>

### Health Percent (체력 퍼센트)

치유의 파동을 사용할 아군 체력의 백분율 임계값입니다.
체력이 이보다 낮아지면 자동으로 치유의 파동을 사용합니다.
