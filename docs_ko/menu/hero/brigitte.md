---
pngicon: ../../../img/hero/hero_brigitte.png
---

# 브리기테 { #head }

<img src="../../../img/hero/hero_brigitte.png" alt="" class="hero-portrait round-edge center" />

## \[Shift\] WhipShot (도리깨 투척) <img src="../../../img/hero/hero_brigitte_shift.png" alt="" class="img-line-bigger"/> `#에임봇(캐스트)` { #shift }

적을 향해 도리깨 투척을 맞추는 것을 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_brigitte_whipshot_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_brigitte_whipshot_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[Shift\] WhipShotAutoKill (자동 도리깨 투척) <img src="../../../img/hero/hero_brigitte_shift.png" alt="" class="img-line-bigger"/> `#자동스킬` `#에임봇(캐스트)` `#킬캐치형` { #shift-auto }

-8<- "hero_skill_autouse.ext"

체력이 얼마 남지 않은 적을 자동으로 도리깨를 투척해 처치합니다.

=== "영어"
    <img src="../../../img/menu/menu_brigitte_whipshotautokill_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_brigitte_whipshotautokill_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[LB\] ShieldBash (방패 밀치기) <img src="../../../img/hero/hero_brigitte_rmb_lmb.png" alt="" class="img-line-bigger"/> `#자동스킬` `#에임봇(플릭)` `#카운터형` { #lb-auto }

!!! info "방패를 들고 있는 상태에서 적이 일정 거리 안으로 접근 시 자동으로 작동됩니다."

방패를 들고 있는 상태에서 적이 방패 밀치기가 닿는 거리 안으로 진입할 경우, 자동으로 해당 적을 조준하고 방패 밀치기를 사용합니다.

=== "영어"
    <img src="../../../img/menu/menu_brigitte_shieldbash_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_brigitte_shieldbash_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

## \[E\] RepairPack (수리팩) <img src="../../../img/hero/hero_brigitte_e.png" alt="" class="img-line-bigger"/> `#자동스킬` `#에임봇(플릭)` `#케어형` { #e-auto }

-8<- "hero_skill_autouse.ext"

체력이 낮은 아군을 향해 자동으로 수리팩을 지급합니다.

=== "영어"
    <img src="../../../img/menu/menu_brigitte_repairpack_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_brigitte_repairpack_ko.png" alt="" class="center"/>

### Max Health Percent (최대 체력 퍼센트)

수리팩을 지급할 체력 백분율 임계값입니다.
이 값 이하로 체력이 내려간 팀원에게 자동으로 수리팩을 지급합니다.

-8<- "hero_tab_flick_aimbot.ext"
