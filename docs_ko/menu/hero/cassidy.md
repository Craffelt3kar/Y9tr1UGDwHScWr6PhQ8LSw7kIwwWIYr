---
pngicon: ../../../img/hero/hero_cassidy.png
---

# 캐서디 { #head }

<img src="../../../img/hero/hero_cassidy.png" alt="" class="hero-portrait round-edge center" />

## \[Shift\] CombatRoll (구르기) <img src="../../../img/hero/hero_cassidy_shift.png" alt="" class="img-line-bigger"/> `#자동스킬` `#콤보형` { #shift-auto }

-8<- "hero_skill_autouse.ext"

총의 탄환이 다 떨어질 시, 자동으로 구르기를 사용해 재장전합니다.

=== "영어"
    <img src="../../../img/menu/menu_cassidy_combatroll_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_cassidy_combatroll_ko.png" alt="" class="center"/>
