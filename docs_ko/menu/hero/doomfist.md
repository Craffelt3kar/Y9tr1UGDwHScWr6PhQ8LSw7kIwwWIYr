---
pngicon: ../../../img/hero/hero_doomfist.png
---

# 둠피스트 { #head }

<img src="../../../img/hero/hero_doomfist.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] RocketPunch (로켓 펀치) <img src="../../../img/hero/hero_doomfist_rmb.png" alt="" class="img-line-bigger"/> `#에임봇(플릭)` { #rb }

적을 향해 로켓 펀치를 사용하는 것을 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_doomfist_rocketpunch_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_doomfist_rocketpunch_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"
