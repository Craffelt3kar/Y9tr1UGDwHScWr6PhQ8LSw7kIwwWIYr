---
pngicon: ../../../img/hero/hero_dva.png
---

# D. Va { #head }

<img src="../../../img/hero/hero_dva.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] DefenseMatrix (방어 매트리스) <img src="../../../img/hero/hero_dva_rmb.png" alt="" class="img-line-bigger"/> `#자동스킬` `#에임봇(트래킹)` `#카운터형` { #rb-auto }

-8<- "hero_skill_autouse.ext"

???+ success "카운터 대상 스킬 목록"
    * [x] <img src="../../../img/hero/hero_zarya.png" alt="" class="img-line-bigger round-edge"/>자리야 중력자탄<img src="../../../img/hero/hero_zarya_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_mei.png" alt="" class="img-line-bigger round-edge"/>메이 눈보라<img src="../../../img/hero/hero_mei_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_cassidy.png" alt="" class="img-line-bigger round-edge"/>캐서디 자력 수류탄<img src="../../../img/hero/hero_cassidy_e.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_tracer.png" alt="" class="img-line-bigger round-edge"/>트레이서 펄스 폭탄<img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_hanzo.png" alt="" class="img-line-bigger round-edge"/>한조 용의 일격<img src="../../../img/hero/hero_hanzo_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>아나 수면총<img src="../../../img/hero/hero_ana_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>아나 생체 수류탄<img src="../../../img/hero/hero_ana_e.png" alt="" class="img-line-bigger"/>

자동으로 위협적인 스킬 또는 궁극기 시전자를 향해 방어 매트리스를 펼쳐 스킬 또는 궁극기를 먹습니다. (궁먹방)

=== "영어"
    <img src="../../../img/menu/menu_dva_defensematrix_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_dva_defensematrix_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=0" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

## \[E\] MicroMissiles (마이크로 미사일) <img src="../../../img/hero/hero_dva_e.png" alt="" class="img-line-bigger"/> `#자동스킬` `#콤보형` { #e }

!!! info "적을 공격하면 자동으로 작동됩니다."

적을 조준하고 공격하는 도중 자동으로 마이크로 미사일을 사용합니다.

=== "영어"
    <img src="../../../img/menu/menu_dva_micromissiles_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_dva_micromissiles_ko.png" alt="" class="center"/>
