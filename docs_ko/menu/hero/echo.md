---
pngicon: ../../../img/hero/hero_echo.png
---

# 에코 { #head }

<img src="../../../img/hero/hero_echo.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] StickyBombs (점착 폭탄) <img src="../../../img/hero/hero_echo_rmb.png" alt="" class="img-line-bigger"/> `#에임봇(캐스트)` { #rb }

적에게 점착 폭탄을 붙이는 것을 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_echo_stickybombs_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_echo_stickybombs_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[E\] FocusingBeam (광선 집중) <img src="../../../img/hero/hero_echo_e.png" alt="" class="img-line-bigger"/> `#에임봇(트래킹)` { #e }

적을 향해 광선 집중을 사용하는 것을 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_echo_focusingbeam_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_echo_focusingbeam_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"
