---
pngicon: ../../../img/hero/hero_genji.png
---

# 겐지 { #head }

<img src="../../../img/hero/hero_genji.png" alt="" class="hero-portrait round-edge center" />

## \[Shift\] SwiftStrike (질풍참) <img src="../../../img/hero/hero_genji_shift.png" alt="" class="img-line-bigger"/> `#자동스킬` `#에임봇(플릭)` `#킬캐치형` { #shift-auto }

-8<- "hero_skill_autouse.ext"

체력이 얼마 남지 않은 적을 자동으로 질풍참을 사용하여 처치합니다.

=== "영어"
    <img src="../../../img/menu/menu_genji_swiftstrike_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_genji_swiftstrike_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

## \[E\] Deflect (튕겨내기) <img src="../../../img/hero/hero_genji_e.png" alt="" class="img-line-bigger"/> `#자동스킬` `#에임봇(플릭)` `#카운터형` { #e-auto }

-8<- "hero_skill_autouse.ext"

???+ success "카운터 대상 스킬 목록"
    * [x] <img src="../../../img/hero/hero_roadhog.png" alt="" class="img-line-bigger round-edge"/>로드호그 사슬 갈고리<img src="../../../img/hero/hero_roadhog_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_tracer.png" alt="" class="img-line-bigger round-edge"/>트레이서 펄스 폭탄<img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_cassidy.png" alt="" class="img-line-bigger round-edge"/>캐서디 자력 수류탄<img src="../../../img/hero/hero_cassidy_e.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_soldier76.png" alt="" class="img-line-bigger round-edge"/>솔저: 76 나선 로켓<img src="../../../img/hero/hero_soldier76_rmb.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>아나 수면총<img src="../../../img/hero/hero_ana_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>아나 생체 수류탄<img src="../../../img/hero/hero_ana_e.png" alt="" class="img-line-bigger"/>

자동으로 위협적인 스킬 시전자를 향해 튕겨내기를 사용해 스킬이나 궁극기를 튕겨냅니다.

=== "영어"
    <img src="../../../img/menu/menu_genji_deflect_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_genji_deflect_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=65" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

## \[Ult\] Dragonblade (용검) <img src="../../../img/hero/hero_genji_q.png" alt="" class="img-line-bigger"/> `#에임봇(캐스트)` { #ult }

-8<- "hero_skill_manualuse.ext"

용검을 사용해 적을 써는 것을 돕습니다.
용검 사용 도중 키를 누를 시 가까운 적을 자동으로 조준합니다.

=== "영어"
    <img src="../../../img/menu/menu_genji_dragonblade_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_genji_dragonblade_ko.png" alt="" class="center"/>

### Use hp threshold instead of killable check { #hp-threshold }

비활성화 시, 확실하게 한 번 썰어서 잡을 수 있는 적들을 우선하여 조준합니다.

활성화 시 체력이 일정 이하로 낮은 적들을 우선하여 조준합니다.

-8<- "hero_tab_cast_aimbot.ext"

## AimAssist (질풍참 후 에임 어시스트) { #aimassist }

적에게 질풍참을 그은 후 해당 목표를 조준하는 것을 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_genji_aimassist_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_genji_aimassist_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/ojhMutysrPw" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
