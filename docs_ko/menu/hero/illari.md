---
pngicon: ../../../img/hero/hero_illari.png
---

# 일리아리 { #head }

<img src="../../../img/hero/hero_illari.png" alt="" class="hero-portrait round-edge center" />

## \[AltFire\] AltFire (치유 광선) <img src="../../../img/hero/hero_illari_gun.png" alt="" class="img-line-bigger"/> `#에임봇(트래킹)` { #rb }

아군을 조준하고 자동으로 치유 광선을 발사해 치유합니다.

=== "영어"
    <img src="../../../img/menu/menu_illari_altfire_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_illari_altfire_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"
