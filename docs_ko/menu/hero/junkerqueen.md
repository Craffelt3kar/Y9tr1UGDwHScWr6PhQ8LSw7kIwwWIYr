---
pngicon: ../../../img/hero/hero_junkerqueen.png
---

# 정커퀸 { #head }

<img src="../../../img/hero/hero_junkerqueen.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] JaggedBlade (톱니칼) <img src="../../../img/hero/hero_junkerqueen_rmb.png" alt="" class="img-line-bigger"/> `#에임봇(캐스트)` { #rb }

적에게 톱니칼을 맞추는 것을 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_junkerqueen_jaggedblade_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_junkerqueen_jaggedblade_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"
