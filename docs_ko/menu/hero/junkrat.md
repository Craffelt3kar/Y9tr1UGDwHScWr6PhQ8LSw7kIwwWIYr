---
pngicon: ../../../img/hero/hero_junkrat.png
---

# 정크랫 { #head }

<img src="../../../img/hero/hero_junkrat.png" alt="" class="hero-portrait round-edge center" />

## \[Shift\] ConcussionMine (충격 지뢰) <img src="../../../img/hero/hero_junkrat_shift.png" alt="" class="img-line-bigger"/> `#자동스킬` `#에임봇(플릭)` `#콤보형`+`#킬캐치형` { #shift-auto }

-8<- "hero_skill_autouse.ext"

평타+지뢰 콤보를 통해 잡을 수 있는 적을 조준 중이거나, 지뢰 한 번 맞으면 죽을 정도로 피가 적은 적이 있다면 자동으로 지뢰를 던져 해당 적을 처치합니다.

=== "영어"
    <img src="../../../img/menu/menu_junkrat_concussionmine_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_junkrat_concussionmine_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"
