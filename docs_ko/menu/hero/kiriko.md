---
pngicon: ../../../img/hero/hero_kiriko.png
---

# 키리코 { #head }

<img src="../../../img/hero/hero_kiriko.png" alt="" class="hero-portrait round-edge center" />

## \[LB\] HealingOfuda (치유의 부적) <img src="../../../img/hero/hero_kiriko_gun_1.png" alt="" class="img-line-bigger"/> `#에임봇(트래킹)` { #lb }

치유의 부적으로 아군을 조준하여 치유하는 것을 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_kiriko_healingofuda_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_kiriko_healingofuda_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"
