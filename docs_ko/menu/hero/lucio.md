---
pngicon: ../../../img/hero/hero_lucio.png
---

# 루시우 { #head }

<img src="../../../img/hero/hero_lucio.png" alt="" class="hero-portrait round-edge center" />

## \[Shift\] Crossfade (분위기 전환) <img src="../../../img/hero/hero_lucio_shift.png" alt="" class="img-line-bigger"/> `#자동스킬` `#케어형` { #shift-auto }

-8<- "hero_skill_autouse.ext"

음악 범위 내 나 자신을 포함한 아군들 중 체력이 일정량 이하로 낮은 이가 있다면 자동으로 치유 증폭을 틉니다.

음악 범위 내 모든 아군의 체력이 일정량 이상이라면 자동으로 속도 증폭을 틉니다.

=== "영어"
    <img src="../../../img/menu/menu_lucio_crossfade_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_lucio_crossfade_ko.png" alt="" class="center"/>

### Health Percent (체력 퍼센트)

나 자신 또는 범위 내 아군들 중 한 명이라도 체력의 백분율이 이 임계값보다 낮다면 자동으로 치유의 음악을 틉니다.

## \[E\] Amp (볼륨을 높여라!) <img src="../../../img/hero/hero_lucio_e.png" alt="" class="img-line-bigger"/> `#자동스킬` `#케어형` { #e-auto }

-8<- "hero_skill_autouse.ext"

음악 범위 내 나 자신을 포함한 아군들 중 체력이 일정량 이하로 낮은 이가 있을 시 볼륨을 높여라를 사용합니다.

=== "영어"
    <img src="../../../img/menu/menu_lucio_amp_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_lucio_amp_ko.png" alt="" class="center"/>

### Health Percent (체력 퍼센트)

나 자신 또는 범위 내 아군들 중 한 명이라도 체력의 백분율이 이 임계값보다 낮다면 자동으로 볼륨을 높혀라를 사용합니다.
