---
pngicon: ../../../img/hero/hero_mei.png
---

# 메이 { #head }

<img src="../../../img/hero/hero_mei.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] Shard (고드름) <img src="../../../img/hero/hero_mei_gun.png" alt="" class="img-line-bigger"/> `#에임봇(캐스트)` { #rb }

!!! info "우클릭을 눌러 고드름을 발사 시 자동으로 작동됩니다."

=== "영어"
    <img src="../../../img/menu/menu_mei_shard_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_mei_shard_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[Shift\] CryoFreeze (급속 빙결) <img src="../../../img/hero/hero_mei_shift.png" alt="" class="img-line-bigger"/> `#자동스킬` `#생존형`+`#회피형` { #shift-auto }

-8<- "hero_skill_autouse.ext"

체력이 적을 시 (또는 위협적인 스킬 및 궁극기를 피하기 위해) 자동으로 급속 빙결을 사용합니다.

=== "영어"
    <img src="../../../img/menu/menu_mei_cryofreeze_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_mei_cryofreeze_ko.png" alt="" class="center"/>

### Health Percent (체력 퍼센트)

플레이어 체력의 백분율이 이 임계값보다 낮아지면 자동으로 급속 빙결을 사용합니다.

### Dodge Spells (스킬 씹기)

???+ success "회피 대상 스킬 목록"
    * [x] <img src="../../../img/hero/hero_reinhardt.png" alt="" class="img-line-bigger round-edge"/>라인하르트 대지분쇄<img src="../../../img/hero/hero_reinhardt_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_roadhog.png" alt="" class="img-line-bigger round-edge"/>로드호그 사슬 갈고리<img src="../../../img/hero/hero_roadhog_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_tracer.png" alt="" class="img-line-bigger round-edge"/>트레이서 펄스 폭탄<img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>아나 수면총<img src="../../../img/hero/hero_ana_shift.png" alt="" class="img-line-bigger"/>

단순히 체력이 떨어졌을 때 뿐만 아니라 위협적인 스킬들을 피하기 위해서도 급속 빙결을 사용합니다.

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=88" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
