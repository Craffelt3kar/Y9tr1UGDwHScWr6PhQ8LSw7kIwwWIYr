---
pngicon: ../../../img/hero/hero_mercy.png
---

# 메르시 { #head }

<img src="../../../img/hero/hero_mercy.png" alt="" class="hero-portrait round-edge center" />

## \[LB\] CaduceusStaff (카두세우스 지팡이) <img src="../../../img/hero/hero_mercy_gun_1.png" alt="" class="img-line-bigger"/> `#에임봇(트래킹)` { #lb }

카두세우스 지팡이로 아군을 치유하는 도중 자동으로 해당 아군을 조준하는 것을 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_mercy_caduceusstaff_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_mercy_caduceusstaff_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"
