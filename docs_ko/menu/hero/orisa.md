---
pngicon: ../../../img/hero/hero_orisa.png
---

# 오리사 { #head }

<img src="../../../img/hero/hero_orisa.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] EnergyJavelin (투창) <img src="../../../img/hero/hero_orisa_rmb.png" alt="" class="img-line-bigger"/> `#에임봇(캐스트)` { #rb }

적에게 창을 던쳐 맞추는 것을 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_orisa_energyjavelin_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_orisa_energyjavelin_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[Shift\] Fortify (방어 강화) <img src="../../../img/hero/hero_orisa_shift.png" alt="" class="img-line-bigger"/> `#자동스킬` `#회피형` { #shift-auto }

-8<- "hero_skill_autouse.ext"

???+ success "카운터 대상 스킬 목록"
    * [x] <img src="../../../img/hero/hero_reinhardt.png" alt="" class="img-line-bigger round-edge"/>라인하르트 대지분쇄<img src="../../../img/hero/hero_reinhardt_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_roadhog.png" alt="" class="img-line-bigger round-edge"/>로드호그 사슬 갈고리<img src="../../../img/hero/hero_roadhog_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_cassidy.png" alt="" class="img-line-bigger round-edge"/>캐서디 자력 수류탄<img src="../../../img/hero/hero_cassidy_e.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_tracer.png" alt="" class="img-line-bigger round-edge"/>트레이서 펄스 폭탄<img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/>

자동으로 방어 강화를 사용해 위협적인 스킬이나 궁극기에 대응합니다.

=== "영어"
    <img src="../../../img/menu/menu_orisa_fortify_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_orisa_fortify_ko.png" alt="" class="center"/>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=48" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

## \[E\] JavelinSpin (창 돌리기) <img src="../../../img/hero/hero_orisa_shift.png" alt="" class="img-line-bigger"/> `#자동스킬` `#에임봇(플릭)` `#회피형` { #e-auto }

-8<- "hero_skill_autouse.ext"

???+ success "카운터 대상 스킬 목록"
    * [x] <img src="../../../img/hero/hero_roadhog.png" alt="" class="img-line-bigger round-edge"/>로드호그 사슬 갈고리<img src="../../../img/hero/hero_roadhog_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_cassidy.png" alt="" class="img-line-bigger round-edge"/>캐서디 자력 수류탄<img src="../../../img/hero/hero_cassidy_e.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_tracer.png" alt="" class="img-line-bigger round-edge"/>트레이서 펄스 폭탄<img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>아나 수면총<img src="../../../img/hero/hero_ana_shift.png" alt="" class="img-line-bigger"/>

자동으로 창 돌리기를 사용해 위협적인 스킬이나 궁극기를 먹습니다. (궁먹방)

=== "영어"
    <img src="../../../img/menu/menu_orisa_javelinspin_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_orisa_javelinspin_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=48" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

## \[Ult\] TerraSurge (대지의 창) <img src="../../../img/hero/hero_orisa_shift.png" alt="" class="img-line-bigger"/> `#자동스킬` `#킬캐치형` { #ult-auto }

!!! info "따로 키를 누르고 있지 않아도 _대지의 창 사용 중이라면_ 자동으로 작동됩니다."

대지의 창 사용 중, 현재의 창 차징율로 처치할 수 있는 적이 대지의 창 범위 내에 있다면 자동으로 창을 꽃아 처치합니다.

=== "영어"
    <img src="../../../img/menu/menu_orisa_terrasurge_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_orisa_terrasurge_ko.png" alt="" class="center"/>
