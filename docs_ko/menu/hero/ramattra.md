---
pngicon: ../../../img/hero/hero_ramattra.png
---

# 라마트라 { #head }

<img src="../../../img/hero/hero_ramattra.png" alt="" class="hero-portrait round-edge center" />

라마트라는 기본 무기가 공허 가속기, 응징으로 총 2 가지입니다.

이 때문에 Aimbot 탭은 존재하지 않으며, 공허 가속기 에임봇과 응징 에임봇이 따로 나뉘어져 있습니다.

두 에임봇의 에임키는 서로 통일되어 있습니다.

## Staff (공허 가속기) <img src="../../../img/hero/hero_ramattra_1_gun.png" alt="" class="img-line-bigger"/> `#에임봇(트래킹)` { #staff }

적을 자동으로 조준하여 공허 가속기(옴닉 형태 기본 공격) 사용을 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_ramattra_staff_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_ramattra_staff_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"

## Pummel (응징) <img src="../../../img/hero/hero_ramattra_2_gun.png" alt="" class="img-line-bigger"/> `#에임봇(플릭)` { #pummel }

적을 자동으로 조준하여 응싱(네메시스 형태 기본 공격) 사용을 돕습니다.

에임키는 공허 가속기 에임봇과 동일합니다.

=== "영어"
    <img src="../../../img/menu/menu_ramattra_pummel_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_ramattra_pummel_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"
