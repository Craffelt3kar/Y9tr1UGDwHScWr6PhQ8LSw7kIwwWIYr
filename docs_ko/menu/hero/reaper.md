---
pngicon: ../../../img/hero/hero_reaper.png
---

# 리퍼 { #head }

<img src="../../../img/hero/hero_reaper.png" alt="" class="hero-portrait round-edge center" />

## \[Shift\] Wraith (망령화) <img src="../../../img/hero/hero_reaper_shift.png" alt="" class="img-line-bigger"/> `#자동스킬` `#회피형` { #shift }

-8<- "hero_skill_autouse.ext"

???+ success "회피 대상 스킬 목록"
    * [x] <img src="../../../img/hero/hero_reinhardt.png" alt="" class="img-line-bigger round-edge"/>라인하르트 대지분쇄<img src="../../../img/hero/hero_reinhardt_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_roadhog.png" alt="" class="img-line-bigger round-edge"/>로드호그 사슬 갈고리<img src="../../../img/hero/hero_roadhog_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_tracer.png" alt="" class="img-line-bigger round-edge"/>트레이서 펄스 폭탄<img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>아나 수면총<img src="../../../img/hero/hero_ana_shift.png" alt="" class="img-line-bigger"/>

자동으로 망령화를 사용해 위협적인 스킬이나 궁극기를 회피합니다.

=== "영어"
    <img src="../../../img/menu/menu_reaper_wraith_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_reaper_wraith_ko.png" alt="" class="center"/>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=77" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
