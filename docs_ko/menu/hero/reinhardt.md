---
pngicon: ../../../img/hero/hero_reinhardt.png
---

# 라인하르트 { #head }

<img src="../../../img/hero/hero_reinhardt.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] BarrierField (방벽 방패) <img src="../../../img/hero/hero_reinhardt_rmb.png" alt="" class="img-line-bigger"/> `#자동스킬` `#에임봇(트래킹)` `#카운터형` { #rb-auto }

-8<- "hero_skill_autouse.ext"

???+ success "카운터 대상 스킬 목록"
    * [x] <img src="../../../img/hero/hero_reinhardt.png" alt="" class="img-line-bigger round-edge"/>라인하르트 대지분쇄<img src="../../../img/hero/hero_reinhardt_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_roadhog.png" alt="" class="img-line-bigger round-edge"/>로드호그 사슬 갈고리<img src="../../../img/hero/hero_roadhog_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_tracer.png" alt="" class="img-line-bigger round-edge"/>트레이서 펄스 폭탄<img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>아나 수면총<img src="../../../img/hero/hero_ana_shift.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>아나 생체 수류탄<img src="../../../img/hero/hero_ana_e.png" alt="" class="img-line-bigger"/>

자동으로 방벽을 들어 위협적인 스킬이나 궁극기를 막습니다.

=== "영어"
    <img src="../../../img/menu/menu_reinhardt_barrierfield_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_reinhardt_barrierfield_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=23" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

## \[E\] FireStrike (화염 강타) <img src="../../../img/hero/hero_reinhardt_e.png" alt="" class="img-line-bigger"/> `#에임봇(캐스트)` { #e }

!!! info "`E` 키를 눌러 화염 강타 사용 시 자동으로 작동됩니다."

=== "영어"
    <img src="../../../img/menu/menu_reinhardt_firestrike_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_reinhardt_firestrike_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"
