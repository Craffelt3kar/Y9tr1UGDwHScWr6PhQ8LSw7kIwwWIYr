---
pngicon: ../../../img/hero/hero_roadhog.png
---

# 로드호그 { #head }

<img src="../../../img/hero/hero_roadhog.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] Breather (숨 돌리기) <img src="../../../img/hero/hero_roadhog_e.png" alt="" class="img-line-bigger"/> `#자동스킬` `#생존형` { #rb-auto }

-8<- "hero_skill_autouse.ext"

체력이 일정 이하로 낮아질 경우 일정 이상이 될 때까지 숨 돌리기를 자동으로 사용합니다.

=== "영어"
    <img src="../../../img/menu/menu_roadhog_breather_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_roadhog_breather_ko.png" alt="" class="center"/>

### Start Health Percent (시작 체력 퍼센트)

숨 돌리기를 자동으로 사용하기 시작할 체력의 백분율입니다.

### End Health Percent (종료 체력 퍼센트)

숨 돌리기 사용을 자동으로 멈출 체력의 백분율입니다.

## \[Shift\] ChainHook (사슬 갈고리) <img src="../../../img/hero/hero_roadhog_shift.png" alt="" class="img-line-bigger"/> `#에임봇(플릭)` { #shift }

사슬 갈고리를 통해 적을 끄는 것을 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_roadhog_chainhook_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_roadhog_chainhook_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

## \[Ult\] WholeHog (돼재앙) <img src="../../../img/hero/hero_roadhog_q.png" alt="" class="img-line-bigger"/> `#에임봇(트래킹)` { #ult }

적을 향해 돼재앙을 사용하는 것을 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_roadhog_wholehog_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_roadhog_wholehog_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"
