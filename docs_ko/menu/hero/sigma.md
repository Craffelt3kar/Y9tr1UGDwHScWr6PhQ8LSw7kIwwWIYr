---
pngicon: ../../../img/hero/hero_sigma.png
---

# 시그마 { #head }

<img src="../../../img/hero/hero_sigma.png" alt="" class="hero-portrait round-edge center" />

## \[E\] Accretion (강착) <img src="../../../img/hero/hero_sigma_e.png" alt="" class="img-line-bigger"/> `#에임봇(캐스트)` { #e }

!!! info "`E` 키를 눌러 강착 사용시 자동으로 작동됩니다."

강착으로 적을 맞추는 것을 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_sigma_accretion_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_sigma_accretion_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[Shift\] KineticGrasp (키네틱 손아귀) <img src="../../../img/hero/hero_sigma_shift.png" alt="" class="img-line-bigger"/> `#자동스킬` `#카운터형` { #shift-auto }

-8<- "hero_skill_autouse.ext"

???+ success "카운터 대상 스킬 목록"
    * [x] <img src="../../../img/hero/hero_cassidy.png" alt="" class="img-line-bigger round-edge"/>캐서디 자력 수류탄<img src="../../../img/hero/hero_cassidy_e.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_tracer.png" alt="" class="img-line-bigger round-edge"/>트레이서 펄스 폭탄<img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_ana.png" alt="" class="img-line-bigger round-edge"/>아나 수면총<img src="../../../img/hero/hero_ana_shift.png" alt="" class="img-line-bigger"/>

자동으로 키네틱 손아귀를 사용해 위협적인 스킬이나 궁극기를 먹습니다. (궁먹방)

=== "영어"
    <img src="../../../img/menu/menu_sigma_kineticgrasp_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_sigma_kineticgrasp_ko.png" alt="" class="center"/>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=37" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
