---
pngicon: ../../../img/hero/hero_sojourn.png
---

# 소전 { #head }

<img src="../../../img/hero/hero_sojourn.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] AltFire (레일건) <img src="../../../img/hero/hero_sojourn_gun.png" alt="" class="img-line-bigger"/> `#자동스킬` `#에임봇(플릭)` `#킬캐치형` { #rb-auto }

-8<- "hero_skill_manualuse.ext"

주 에임봇 키를 누르고 있는 상태에서, 현재 레일건 우클릭 차징률로 처치할 수 있는 적이 있다면 자동으로 해당 적을 향해 레일건을 발사하여 처치합니다.

=== "영어"
    <img src="../../../img/menu/menu_sojourn_altfire_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_sojourn_altfire_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

## \[RB\] AltFireManual (수동 레일건) <img src="../../../img/hero/hero_sojourn_gun.png" alt="" class="img-line-bigger"/> `#에임봇(플릭)` { #rb }

=== "영어"
    <img src="../../../img/menu/menu_sojourn_altfiremanual_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_sojourn_altfiremanual_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"
