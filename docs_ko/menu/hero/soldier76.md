---
pngicon: ../../../img/hero/hero_soldier76.png
---

# 솔저: 76 { #head }

<img src="../../../img/hero/hero_soldier76.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] HelixRockets (나선 로켓) <img src="../../../img/hero/hero_soldier76_rmb.png" alt="" class="img-line-bigger"/> `#자동스킬` `#에임봇(플릭)` `#킬캐치형` { #rb-auto }

-8<- "hero_skill_manualuse.ext"

나선 로켓으로 처치할 수 있을 정도로 체력이 적은 적을 자동으로 나선 로켓을 발사하여 처치합니다.

=== "영어"
    <img src="../../../img/menu/menu_soldier76_helixrockets_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_soldier76_helixrockets_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

## \[E\] BioticField (생체장) <img src="../../../img/hero/hero_soldier76_e.png" alt="" class="img-line-bigger"/> `#자동스킬` `#생존형` { #e-auto }

-8<- "hero_skill_autouse.ext"

플레이어의 체력이 적을 시 자동으로 생체장을 설치하여 치유합니다.

=== "영어"
    <img src="../../../img/menu/menu_soldier76_bioticfield_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_soldier76_bioticfield_ko.png" alt="" class="center"/>

### Health Percent (체력 퍼센트)

이 값 이하로 체력이 내려가면 생체장을 자동으로 설치합니다.
