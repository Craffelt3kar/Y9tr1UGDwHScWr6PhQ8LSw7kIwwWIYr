---
pngicon: ../../../img/hero/hero_sombra.png
---

# 솜브라 { #head }

<img src="../../../img/hero/hero_sombra.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] Hack (해킹) <img src="../../../img/hero/hero_sombra_rmb.png" alt="" class="img-line-bigger"/> `#자동스킬` `#콤보형` { #rb-auto }

-8<- "hero_skill_manualuse.ext"

에임봇 조준 목표가 해킹되지 않은 상태라면, 총을 발사하기 전 먼저 목표를 해킹합니다.

=== "영어"
    <img src="../../../img/menu/menu_sombra_hack_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_sombra_hack_ko.png" alt="" class="center"/>

## \[Shift\] Virus (바이러스) <img src="../../../img/hero/hero_sombra_shift.webp" alt="" class="img-line-bigger"/> `#자동스킬` `#에임봇(플릭)` `#콤보형` { #shift-auto }

-8<- "hero_skill_manualuse.ext"

바이러스 스킬을 사용할 수 있다면, 총을 발사하기 전 목표에게 바이러스를 투척합니다.

=== "영어"
    <img src="../../../img/menu/menu_sombra_virus_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_sombra_virus_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

## \[Ult\] EMP (자동 EMP) <img src="../../../img/hero/hero_sombra_q.png" alt="" class="img-line-bigger"/> `#자동스킬` `#카운터형` { #ult-auto }

-8<- "hero_skill_autouse.ext"

???+ success "카운터 대상 스킬 목록"
    * [x] <img src="../../../img/hero/hero_reinhardt.png" alt="" class="img-line-bigger round-edge"/>라인하르트 대지분쇄<img src="../../../img/hero/hero_reinhardt_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_genji.png" alt="" class="img-line-bigger round-edge"/>겐지 용검<img src="../../../img/hero/hero_genji_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_lucio.png" alt="" class="img-line-bigger round-edge"/>루시우 소리 방벽<img src="../../../img/hero/hero_lucio_q.png" alt="" class="img-line-bigger"/>

자동으로 EMP를 사용하여 위협적인 궁극기들을 카운터칩니다.

=== "영어"
    <img src="../../../img/menu/menu_sombra_emp_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_sombra_emp_ko.png" alt="" class="center"/>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=102" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
