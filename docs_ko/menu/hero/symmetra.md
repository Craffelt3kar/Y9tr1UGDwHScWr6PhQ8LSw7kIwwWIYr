---
pngicon: ../../../img/hero/hero_symmetra.png
---

# 시메트라 { #head }

<img src="../../../img/hero/hero_symmetra.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] Orb (구체) <img src="../../../img/hero/hero_symmetra_gun.png" alt="" class="img-line-bigger"/> `#에임봇(플릭)` { #rb }

광자 발사기 우클릭 차징샷(구체)를 적에게 맞추는 것을 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_symmetra_orb_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_symmetra_orb_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"
