---
pngicon: ../../../img/hero/hero_tracer.png
---

# 트레이서 { #head }

<img src="../../../img/hero/hero_tracer.png" alt="" class="hero-portrait round-edge center" />

## \[Shift\] Blink (점멸) <img src="../../../img/hero/hero_tracer_shift.png" alt="" class="img-line-bigger"/> `#자동스킬` `#에임봇(플릭)` `#킬캐치형` { #shift-auto }

-8<- "hero_skill_manualuse.ext"

점멸 근접공격 콤보(트풍참) 및 점멸 펄스폭탄 콤보(점멸부착)를 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_tracer_blink_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_tracer_blink_ko.png" alt="" class="center"/>

### On Melee

활성화 시 체력이 30 이하인 적들에 대해 점멸 근접공격 콤보(트풍참)을 자동으로 수행합니다.

### Only Melee If No Ammo

활성화 시 펄스 쌍권총의 탄환이 다 떨어졌을 때만 점멸 근접공격 콤보를 사용합니다.

### On Pulse Bomb

활성화 시 점멸 펄스폭탄 콤보(점멸부착)을 자동으로 수행합니다.

-8<- "hero_tab_flick_aimbot.ext"

## \[Ult\] PulseBomb (펄스 폭탄) <img src="../../../img/hero/hero_tracer_q.png" alt="" class="img-line-bigger"/> `#에임봇(캐스트)` { #ult }

적에게 펄스 폭탄을 부착하는 것을 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_tracer_pulsebomb_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_tracer_pulsebomb_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[E\] Recall (시간 역행) <img src="../../../img/hero/hero_tracer_e.png" alt="" class="img-line-bigger"/> `#자동스킬` `#생존형` { #e-auto }

-8<- "hero_skill_autouse.ext"

체력이 낮아지면 자동으로 역행을 사용해 생존 및 회피합니다.

=== "영어"
    <img src="../../../img/menu/menu_tracer_recall_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_tracer_recall_ko.png" alt="" class="center"/>

### Health Percent (체력 퍼센트)

역행을 사용할 체력 임계값입니다.

### Health Lost Percent (체력 손실 퍼센트)

역행을 사용할 체력의 감소 백분율 임계값입니다.
