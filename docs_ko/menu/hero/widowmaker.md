---
pngicon: ../../../img/hero/hero_widowmaker.png
---

# 위도우메이커 { #head }

<img src="../../../img/hero/hero_widowmaker.png" alt="" class="hero-portrait round-edge center" />

위도우메이커의 기본 무기인 죽음의 입맞춤은 전자동 돌격 소총, 저격 모드의 2 가지 모드를 지원합니다.

이 때문에 Aimbot 탭은 존재하지 않으며, 저격 모드와 전자동 돌격 소총 모드에 대한 에임봇이 따로 나뉘어져 있습니다.

두 에임봇의 에임키는 서로 통일되어 있습니다.

## Sniper (저격 모드) <img src="../../../img/hero/hero_widowmaker_gun.png" alt="" class="img-line-bigger"/> `#에임봇(플릭)` { #sniper }

=== "영어"
    <img src="../../../img/menu/menu_widowmaker_sniper_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_widowmaker_sniper_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"

## SMG (전자동 돌격 소총) <img src="../../../img/hero/hero_widowmaker_gun.png" alt="" class="img-line-bigger"/> `#에임봇(트래킹)` { #smg }

에임키는 저격 모드 에임봇과 동일합니다.

=== "영어"
    <img src="../../../img/menu/menu_widowmaker_smg_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_widowmaker_smg_ko.png" alt="" class="center"/>

-8<- "hero_tab_track_aimbot.ext"
