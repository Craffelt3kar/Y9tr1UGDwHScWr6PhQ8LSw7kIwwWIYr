---
pngicon: ../../../img/hero/hero_winston.png
---

# 윈스턴 { #head }

<img src="../../../img/hero/hero_winston.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] AltFire (테슬라 캐논 보조 발사) <img src="../../../img/hero/hero_winston_gun.png" alt="" class="img-line-bigger"/> `#에임봇(플릭)` { #rb }

테슬라 캐논 보조 발사(차징샷)으로 적들을 맞히는 것을 돕습니다.

=== "영어"
    <img src="../../../img/menu/menu_winston_altfire_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_winston_altfire_ko.png" alt="" class="center"/>

-8<- "hero_tab_flick_aimbot.ext"
