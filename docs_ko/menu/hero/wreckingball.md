---
pngicon: ../../../img/hero/hero_wreckingball.png
---

# 레킹볼 { #head }

<img src="../../../img/hero/hero_wreckingball.png" alt="" class="hero-portrait round-edge center" />

## \[E\] AdaptiveShield (적응형 보호막) <img src="../../../img/hero/hero_wreckingball_e.png" alt="" class="img-line-bigger"/> `#자동스킬` `#생존형` { #e-auto }

-8<- "hero_skill_autouse.ext"

체력이 일정량 이하이고 범위 안에 한 명 이상 적이 있어 추가 생명력을 얻을 수 있는 경우, 즉시 적응형 보호막을 사용합니다.

=== "영어"
    <img src="../../../img/menu/menu_wreckingball_adaptiveshield_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_wreckingball_adaptiveshield_ko.png" alt="" class="center"/>
