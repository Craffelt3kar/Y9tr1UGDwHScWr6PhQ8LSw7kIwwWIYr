---
pngicon: ../../../img/hero/hero_zarya.png
---

# 자리야 { #head }

<img src="../../../img/hero/hero_zarya.png" alt="" class="hero-portrait round-edge center" />

## \[Shift\] SelfBubble (자기방벽) <img src="../../../img/hero/hero_zarya_shift.png" alt="" class="img-line-bigger"/> `#자동스킬` `#에임봇(플릭)` `#생존형` { #shift-auto }

-8<- "hero_skill_autouse.ext"

체력이 일정량 이상 떨어지면 자동으로 입자 방벽을 사용합니다.

=== "영어"
    <img src="../../../img/menu/menu_zarya_selfbubble_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_zarya_selfbubble_ko.png" alt="" class="center"/>

### Health Percent (체력 퍼센트)

자기방벽을 사용할 체력 임계값입니다.

### Health Lost Percent (체력 손실 퍼센트)

자기방벽을 사용할 체력의 감소 백분율 임계값입니다.

## \[E\] AllyBubble (주는방벽) <img src="../../../img/hero/hero_zarya_e.png" alt="" class="img-line-bigger"/> `#자동스킬` `#에임봇(플릭)` `#케어형` { #e-auto }

-8<- "hero_skill_autouse.ext"

체력이 일정량 이상 떨어진 아군을 자동으로 조준하고 방벽을 씌웁니다.

=== "영어"
    <img src="../../../img/menu/menu_zarya_allybubble_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_zarya_allybubble_ko.png" alt="" class="center"/>

### Health Percent (체력 퍼센트)

방벽을 줄 아군의 체력 임계값입니다.

### Health Lost Percent (체력 손실 퍼센트)

방벽을 줄 아군의 체력의 감소 백분율 임계값입니다.

-8<- "hero_tab_flick_aimbot.ext"
