---
pngicon: ../../../img/hero/hero_zenyatta.png
---

# 젠야타 { #head }

<img src="../../../img/hero/hero_zenyatta.png" alt="" class="hero-portrait round-edge center" />

## \[RB\] OrbsRelease (파괴의 구슬 모아쏘기) <img src="../../../img/hero/hero_zenyatta_gun.png" alt="" class="img-line-bigger"/> `#에임봇(캐스트)` { #rb }

!!! info "우클릭을 꾹 눌러 구슬을 차징시켜 놓은 상태에서 에임키를 누르면 목표를 조준하고 구슬들을 발사합니다."

=== "영어"
    <img src="../../../img/menu/menu_zenyatta_orbsrelease_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_zenyatta_orbsrelease_ko.png" alt="" class="center"/>

-8<- "hero_tab_cast_aimbot.ext"

## \[Shift\] HarmonyOrb (조화의 구슬) <img src="../../../img/hero/hero_zenyatta_shift.png" alt="" class="img-line-bigger"/> `#자동스킬` `#에임봇(플릭)` `#케어형` { #shift-auto }

-8<- "hero_skill_autouse.ext"

체력이 일정 이하로 떨어진 아군들을 자동으로 조준하여 조화의 구슬을 달아 회복시킵니다.

=== "영어"
    <img src="../../../img/menu/menu_zenyatta_harmonyorb_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_zenyatta_harmonyorb_ko.png" alt="" class="center"/>

### Max Health Percent (최대 체력 퍼센트)

이 이하로 체력이 떨어진 아군에게 자동으로 조화의 구슬을 달아줍니다.

-8<- "hero_tab_flick_aimbot.ext"

## \[E\] DiscordOrb (부조화의 구슬) <img src="../../../img/hero/hero_zenyatta_e.png" alt="" class="img-line-bigger"/> `#자동스킬` `#콤보형` { #e-auto }

-8<- "hero_skill_autouse.ext"

에임봇을 통해 적을 공격할 때, 자동으로 해당 대상에게 부조화의 구슬을 부착합니다.

=== "영어"
    <img src="../../../img/menu/menu_zenyatta_discordorb_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_zenyatta_discordorb_ko.png" alt="" class="center"/>

## \[Ult\] Transcendence (초월) <img src="../../../img/hero/hero_zenyatta_q.png" alt="" class="img-line-bigger"/> `#자동스킬` `#카운터형` { #ult-auto }

-8<- "hero_skill_autouse.ext"

???+ success "카운터 대상 스킬 목록"
    * [x] <img src="../../../img/hero/hero_reinhardt.png" alt="" class="img-line-bigger round-edge"/>라인하르트 대지분쇄<img src="../../../img/hero/hero_reinhardt_q.png" alt="" class="img-line-bigger"/>
    * [x] <img src="../../../img/hero/hero_sombra.png" alt="" class="img-line-bigger round-edge"/>솜브라 EMP<img src="../../../img/hero/hero_sombra_q.png" alt="" class="img-line-bigger"/>

자동으로 초월을 사용하여 위협적인 궁극기들을 카운터칩니다.

=== "영어"
    <img src="../../../img/menu/menu_zenyatta_transcendence_en.png" alt="" class="center"/>

=== "한국어"
    <img src="../../../img/menu/menu_zenyatta_transcendence_ko.png" alt="" class="center"/>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/qZ3uN6Nc8G8?start=127" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
