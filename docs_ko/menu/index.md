---
description: 노블 3의 설정 창은 깔끔한 디자인을 자랑하면서도 자세한 설정을 지원합니다
icon: svgrepo/sliders2
---

# 노블 메뉴 { #head }

## 메뉴 모습 { #appearance }

<figure><img src="../../../img/menu/menu_en.png" alt="Appearance of Noble Menu"><figcaption><p>설정 창의 모습</p></figcaption></figure>

처음 로드되었을 때는 설정 창의 맨 위에 오버워치 아이콘이 나타납니다.

게임에 들어가서 영웅을 픽한 후에는 맨 위에 현재 플레이 중인 영웅의 초상화가 나타납니다.

***

메뉴의 설정들은 각 탭으로 나뉘어져 있으며, 일반적으로 [Visuals 탭](visuals.md) - [Aimbot 탭](aimbot/index.md) - [Melee 탭](melee.md) - [Rageaim 탭](rageaim.md) 순서로 나타냅니다.

만약 현재 플레이 중인 영웅의 기술들에 대해 스킬 도우미가 지원된다면, Aimbot 탭과 Melee 탭 사이에 해당 기술들에 대응되는 스킬 도우미 탭들이 나타납니다.

만약 현재 플레이 중인 영웅의 기본 무기가 2개 이상이거나 무기가 지원하는 모드가 2개 이상인 경우, Aimbot 탭이 없어지고 해당 무기 모드 이름에 대응되는 에임봇 탭이 나타납니다. (위도우메이커, 라마트라 등)

위 두 경우와 같은 상황에서 각 메뉴에 대한 설명은 [영웅 별 메뉴](hero/index.md) 문서와 그 하위 범주의 문서들을 참고하세요.

### Session Time { #session-time }

남은 노블 사용 가능 시간이 표시됩니다.

???+ warning "사용 시간이 다 떨어지면 게임이 자동으로 꺼집니다."
    그 과정에서 오류 메시지나 게임 크래시 알림이 뜰 수 있지만, 이는 정상적인 현상입니다.

???+ info "24시간 이상의 시간이 남았을 때는 24시간까지만 표시됩니다."
    예를들어, 일주일 키를 입력했을 경우 24시간에서부터 시간이 점점 줄어들며, 시간이 0이 되었을 때 게임이 꺼지는 대신 다시 24시간부터 정상적으로 흘러갑니다.

### Language { #language }

노블 메뉴를 표시할 언어를 설정합니다.

* EN: 영어
* CN: 중국어
* KR: 한국어

## 메뉴 여닫기 { #toggle-menu }

설정 창을 여닫는 단축키는 `Insert` 키와 `F3` 키 입니다.

<figure><img src="../../../img/menu/menu_key.png" alt="104-key US Keyboard Layout. Insert and F3 key is highlighted."><figcaption><p>A 104-key US keyboard layout, largely standardized since the IBM Personal System/2. (Drawn by Mysid in CorelDRAW.)</p></figcaption></figure>

## 슬라이더의 설정 값 직접 입력하기 { #set-slider-value }

기본적으로 노블 설정 창의 값들은 슬라이더를 왼쪽, 오른쪽으로 드래그하여 조절할 수 있습니다.
그러나 때때로는 이러한 슬라이더를 이용해 값을 조절하는 것이 아닌, 정확한 설정 값을 사용자가 직접 입력해야 할 상황이 존재할 수 있습니다.

이 때는 크게 두 가지 방법을 통해 슬라이더의 값을 직접 입력하는 모드로 넘어갈 수 있습니다:

1. `Ctrl` 키를 누른 채로 슬라이더를 클릭합니다.

2. `Tab` 키를 이용하여 값을 수정하려는 슬라이더를 선택합니다.
    `Tab` 키를 통해 다음 설정을, `Shift+Tab` 키를 통해 이전 설정을 선택할 수 있습니다.

    값을 지정한 후에는 반드시 `Tab`키로 다음 설정을 선택해 주거나, 엔터 키를 눌러 주어야지 변경된 값이 적용됩니다.
    `ESC` 버튼을 누르거나 그냥 설정 창을 닫아 버리면 다시 수정 이전의 값으로 돌아갑니다.

    <img src="../../../img/menu/menu_set_slider_value.gif" alt="Snap between sliders using TAB and SHIFT+TAB" class="center"/>

## 설정 시연 영상 (영문) { #showcase }

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/64H2cX6o7Rk" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
