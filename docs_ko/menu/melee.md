---
description: 이 탭에서는 자동 근접 공격 기능에 대해 자세하게 설정할 수 있습니다.
icon: svgrepo/fist-variant
tags:
    - Melee
    - AutoMelee
    - AutoAttack
---

# Melee 탭 { #head }

=== "영어"
    <img src="../../img/menu/menu_melee_en.png" alt="Melee 탭" class="center"/>

=== "한국어"
    <img src="../../img/menu/menu_melee_ko.png" alt="Melee 탭" class="center"/>

기본적인 설정은 모두 [에임봇 설정](aimbot/index.md), [플릭 에임봇](aimbot/flick.md)과 동일합니다.

남은 체력이 30 미만인 적이 근접 공격이 닿는 거리 안에 존재할 경우, 자동으로 조준하고 근접 공격을 사용해 확정타를 날립니다.

## 참고영상 { #showcase }

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/O6N3bEzwmbY" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
