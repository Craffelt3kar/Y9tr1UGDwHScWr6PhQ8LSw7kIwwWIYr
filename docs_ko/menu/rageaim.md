---
description: 이 탭에서는 노블의 레이지 에임봇, 유도탄에 관해 설정할 수 있습니다.
icon: svgrepo/headshot
tags:
    - Rage
    - Silent Aim
    - pSilent
    - RageBot
---

# Rageaim 탭 { #head }

??? danger "유도탄 기능은 안전하지 않은 기능입니다."
    <p class="very-important-info">유도탄 기능은 엄연히 안전하지 않은 기능에 속하며, 한 번이라도 사용 시 빠른 시간 안에 정지를 먹습니다. 진짜로 버릴 계정에서만 사용하시기를 권장합니다.</mark>

    이는 일반 게임에서 뿐만 아니라 사설방, PvE 등에도 모두 적용되는 이야기입니다.

    _심지어 훈련장에서 혼자 유도탄 쓰는것과, 혼자 비공개 사설방 만들어서 AI들 추가해서 사용하는 경우마저도 포함됩니다._

    _밑에 참고 영상 찍은 계정, 다음 날 바로 날라갔습니다._

노블의 레이지 에임봇은 클라이언트 측 사일런트 에임봇으로 구현되어 있습니다.

관전 시 트래킹 영웅들에 대해서는 에임 빡고정, 플릭 영웅들에 대해서는 리플릭하는 모습을 볼 수 있습니다.
단지 이 모든 과정이 플레이어 화면에 보이지만 않을 뿐입니다.

하지만 역시 빡고는 빡고이기에 빠른 시간 안에 정지를 먹습니다.
특히 트래킹 영웅들의 유도탄은 실제로는 대놓고 빡고정이기에, 플릭 영웅들의 리플릭보다 더 빠른 시간 안에 정지를 먹습니다.

<p class="very-important-info">빡고 무정지란 애초에 불가능합니다. 아무리 보안이 좋은 핵이라도 빡고 사용 시 무조건 정지를 먹습니다.</p>

!!! hint "사용 중 이동 방향이 이상해지는 문제"
    트래킹 영웅(솔저: 76, 트레이서 등) 유도탄 사용 중에 WASD로 움직일 시 캐릭터 움직이는 방향이 이상해지는 현상은 정상입니다.

    단지 사용자에게 화면에만 정면을 바라보고 있는 것으로 나타나는 것이고, 실제로는 적에게 에임이 고정되어 있으며, 움직이는 방향도 실제 에임을 따라가면서 발생하는 현상입니다.

=== "영어"
    <img src="../../img/menu/menu_rageaim_en.png" alt="Rageaim 탭" class="center"/>

=== "한국어"
    <img src="../../img/menu/menu_rageaim_ko.png" alt="Rageaim 탭" class="center"/>

## Enabled (켜짐) { #enabled }

유도탄 기능을 사용할지의 여부입니다.

## Key (키 설정) { #key }

유도탄 기능 활성화 버튼입니다. 해당 키를 꾹 누르고 있으면 자동으로 FoV 내의 적들에게 총알이 날아갑니다.

반대로 말하면, 유도탄 기능이 사용 설정되어 있더라도 유도탄 키를 누르지 않으면 유도탄 기능은 동작하지 않습니다.

Set 버튼을 누른 후 키보드 키 또는 마우스 클릭을 입력함으로써 키를 지정할 수 있으며, 이미 지정된 키는 Unbind 버튼으로 지정 해제할 수 있습니다.

## FoV (에임봇 범위) { #fov }

지정된 FoV 내에 들어온 대상들에게만 총알이 날아갑니다.

슬라이더를 완전히 오른쪽으로 밀어 값을 360으로 지정하면 FoV 제한을 완전히 끌 수 있습니다.

## 참고영상 { #showcase }

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/XyXJA6Z-vJU" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/3orksYgpsyo" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/JOJyJBTQ7-g" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
