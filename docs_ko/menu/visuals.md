---
description: 이 탭에서는 ESP 및 적 스킬 쿨타임, 궁극기 충전률 등 시각 요소들과 관련된 기능들을 설정할 수 있습니다.
icon: svgrepo/visuals
tags:
    - Visuals
    - ESP
    - Skeleton
    - Health
    - Bone
    - Outline
    - Cooldowns
    - Color
---

# Visuals 탭 { #head }

ESP, 적 스킬 쿨타임 및 궁극기 충전률과 같은 시각 요소들에 관련된 설정이 가능한 탭입니다.

!!! info "시각 정보와 관련된 설정들은 모두 공통적으로 설정되고 저장됩니다."
    에임봇을 비롯한 다른 설정들은 영웅별로 따로 저장되고 적용되는 것과 달리, 시각 정보와 관련된 설정들은 모두 전역적으로 저장되고 적용됩니다.

    예시로, 위도우메이커를 들고 ESP를 켰다면 영웅을 한조로 바꾸어도 여전히 ESP가 켜져 있는 상태가 유지됩니다.

=== "영어"
    <img src="../../img/menu/menu_visuals_en.png" alt="Visuals 탭" class="center"/>

=== "한국어"
    <img src="../../img/menu/menu_visuals_ko.png" alt="Visuals 탭" class="center"/>

## :material-human: ESP { #esp }

### Draw Skeleton (스켈레톤) { #draw-skeleton }

활성화 시 적 영웅의 골격 구조를 보여줍니다. 머리에 해당하는 부분은 굵은 점으로 강조 표시되어 나타납니다.

??? example "사용 예시"
    <figure><img src="../../img/showcase/showcase_skeleton_visible.png" alt="Draw Skeleton 보이는 적 영웅"><figcaption><p>보이는 적 영웅</p></figcaption></figure>
    <figure><img src="../../img/showcase/showcase_skeleton_invisible.png" alt="Draw Skeleton 보이지 않는 적 영웅"><figcaption><p>보이지 않는 적 영웅</p></figcaption></figure>

### Draw Name (영웅 이름) { #draw-name }

활성화 시 적 영웅의 이름을 적 영웅 바로 아래 위치에 항상 표시합니다.

??? example "사용 예시"
    <img src="../../img/showcase/showcase_name.png" alt="Draw Name 사용 예시" class="center"/>

### Draw HealthBar (체력바) { #draw-healthbar }

활성화 시 적 영웅 머리 위에 체력바를 보여줍니다.

이는 게임이 자체적으로 제공하는 체력바와는 달리 항상 적 영웅 머리 위에 붙어 있으며, 사라지지 않습니다.

또한 이 체력바의 크기는 적과의 거리에 따라 유동적으로 변하여 시인성을 유지합니다.

방어구 체력, 보호막 체력, 추가 체력 및 힐밴 여부(아나 생체 수류탄) 등도 대응되는 색상으로 나타내집니다.

??? example "사용 예시"
    <img src="../../img/showcase/showcase_healthbar.png" alt="Draw Healthbar 사용 예시" class="center"/>

### Draw Box (박스) { #draw-box }

2D 박스 ESP입니다.
활성화 시 적 영웅의 위치에 2D 박스 ESP를 그립니다.

??? example "사용 예시"
    <figure><img src="../../img/showcase/showcase_box_visible.png" alt="Draw Box 보이는 적 영웅"><figcaption><p>보이는 적 영웅</p></figcaption></figure>
    <figure><img src="../../img/showcase/showcase_box_invisible.png" alt="Draw Box 보이지 않는 적 영웅"><figcaption><p>보이지 않는 적 영웅</p></figcaption></figure>

### Draw Outline (테두리) { #draw-outline }

테두리 ESP입니다.
흔히 '테두리 월핵'이라는 뜻의 '테월'이라고 많이 불립니다.

??? example "사용 예시"
    <figure><img src="../../img/showcase/showcase_outline_visible.png" alt="Draw Outline 보이는 적 영웅"><figcaption><p>보이는 적 영웅</p></figcaption></figure>
    <figure><img src="../../img/showcase/showcase_outline_invisible.png" alt="Draw Outline 보이지 않는 적 영웅"><figcaption><p>보이지 않는 적 영웅</p></figcaption></figure>

## :material-av-timer: 스킬 쿨타임 및 궁극기 충전률 표시 { #skill-cooldowns }

### Draw Cooldowns (스킬 쿨타임) { #draw-cooldowns }

활성화 시 적 영웅의 초상화, 스킬 쿨타임과 궁극기 충전률을 해당 적 영웅 바로 아래에 표시합니다.

사용 불가능한 스킬은 회색으로 드러나며, 남은 쿨타임 또는 차징률이 아래에서부터 올라오는 주황색 진행률 막대를 통해 표시됩니다.

??? example "사용 예시"
    <img src="../../img/showcase/showcase_cooldowns.png" alt="Draw Cooldowns 사용 예시" class="center"/>

### Draw Cooldowns Scoreboard (스킬 쿨타임 스코어보드 모드) { #draw-cooldowns-scoreboard }

활성화 시 점수판(`Tab`키 누르면 보이는 그것)에 아군 및 적 영웅의 초상화, 스킬 쿨타임과 궁극기 충전률을 표시합니다.

사용 불가능한 스킬은 회색으로 드러나며, 남은 쿨타임 또는 차징률이 아래에서부터 올라오는 주황색 진행률 막대를 통해 표시됩니다.

??? example "사용 예시"
    <img src="../../img/showcase/showcase_cooldowns_scoreboard.jpg" alt="Draw Cooldowns Scoreboard 사용 예시" class="center"/>

## :material-target: 에임봇 조준 목표 표시 관련 { #aimbot-targets }

### Draw Targets (타겟 표시) { #draw-targets }

활성화 시 현재 에임봇의 조준 목표를 2D 박스 ESP로 나타냅니다.

??? example "사용 예시"
    <img src="../../img/showcase/showcase_target.png" alt="Draw Targets 사용 예시" class="center"/>

### Draw Target Lines (타겟 연결선 표시) { #draw-target-lines }

!!! info "이 옵션은 Draw Targets 옵션이 활성화되었을 때만 보입니다."

활성화 시 현재 에임봇의 조준 목표 부위와 현재 십자선 위치 사이에 선을 그려 에임이 움직일 예상 궤적을 보여줍니다.

에임봇의 조준 목표 부위는 큰 점으로 강조 표시됩니다.

??? example "사용 예시"
    <img src="../../img/showcase/showcase_target+line.png" alt="Draw Target Lines 사용 예시" class="center"/>

## :material-palette: ESP 색상 설정 { #esp-color }

모든 ESP 색상은 RGB값을 직접 입력하거나, 색 박스를 클릭하여 색상 팔레트를 열어 설정할 수 있습니다.

색상 팔레트에서는 RGB값, HSV값, 또는 HTML 색상 코드를 직접 입력하거나 HSV 슬라이더를 통해 색을 지정할 수 있습니다.

??? example "색 선택 팔레트"
    <img src="../../img/menu/menu_visuals_colorsel_ko.png" alt="색 선택 팔레트" class="center"/>

### Color Visible (보이는 적 색상) { #color-visible }

보이는(벽 뒤에 있지 않은) 적 영웅의 ESP 색상을 설정합니다.

### Color Not Visible (보이지 않는 적 색상) { #color-not-visible }

보이지 않는(벽 뒤에 있는) 적 영웅의 ESP 색상을 설정합니다.

### Target Color (타겟 색상) { #target-color }

!!! info "이 옵션은 Draw Targets 옵션이 활성화되었을 때만 보입니다."

에임봇 조준 목표 ESP의 색상을 설정합니다.
