---
icon: material/share
tags:
  - Share
  - Config
  - Xml
---

# 설정 파일 { #head }

!!! notice "ESP에 관한 설정들은 `Global.xml` 설정 파일에 전역적으로 저장됩니다."

노블은 전역 설정과 모든 영웅 별 설정들을 노블 로더와 동일한 폴더의 `config` 폴더 내에 XML 파일의 형태로 저장합니다.

모든 설정값들은 `config` 폴더 내에 각 영웅의 이름에 해당하는 XML 파일 안에 저장되어 있습니다.
(예시: 위도우메이커의 설정값들은 `Windowmaker.xml` 파일 내에 저장되어 있습니다)

???+ warning "몇몇 영웅들은 파일명과 실제 영웅 이름이 일치하지 않을 수 있습니다."
    대표적으로 캐서디에 대한 설정값들은 캐서디의 옛 이름인 맥크리에 해당하는 `Mccree.xml`에 저장됩니다.

이 XML 파일들을 복사하여 공유함으로써 설정값들을 공유할 수 있습니다.

만일에라도 설정값이 초기화되는 경우를 방지하기 위해 항상 이 설정 파일들을 따로 백업해 놓는 것을 추천합니다.

## 다른 사용자의 설정 파일 적용 방법 { #apply }

다른 사람의 설정 파일을 다운로드해 적용하여 사용하고자 한다면, 먼저 게임이 실행 중이지 않아야 합니다. (노블이 게임상에 로드되어 있으면 안 됩니다)

만약 노블이 이미 게임에 로드된 상태라면 설정 파일을 덮어씌우더라도 새로운 설정이 자동으로 적용되지 않습니다.
오히려 게임 속 노블 메뉴에서 설정값을 바꾸는 순간 덮어씌운 파일 속 설정들이 다시 원래 값들로 돌아가 버리는 일이 발생합니다.

게임이 완전히 꺼진 것을 확인하고 나서 노블 로더와 동일한 폴더의 `config` 폴더 속 파일들 중 새로 다운로드받은 설정 파일과 동일한 영웅 이름을 가진 파일을 새로 다운로드받은 설정 파일로 덮어씁니다.

예를 들어 위도우메이커 설정 파일 `Widowmaker.xml`을 다운로드받았다고 가정하면, `config` 폴더 내의 원래 존재하던 `Widowmaker.xml` 파일을 새로 다운로드받은 파일로 덮어쓰면 됩니다.

<img src="../img/profile_file_overwrite.png" alt="" class="center"/>
