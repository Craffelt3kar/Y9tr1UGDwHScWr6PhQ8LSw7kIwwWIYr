---
icon: material/robot
tags:
  - Share
  - Config
  - Settings Bot
---

# Noble Settings 봇 { #head }

Noble Settings 봇은 노블 디스코드 서버 `#settings-share` 채널에 존재하는 설정 공유 디스코드 봇입니다.

## 다른 사용자의 설정 가져오기 { #show }

지원하는 영웅 목록을 보기 위해서는 `/show` 명령어를 입력합니다.

특정 영웅에 대한 설정값들을 보려면 `/show <영웅 이름>` 명령어를 입력합니다.

```
1. AAAA#0 (ana) - 2 👍 / 0 👎
2. BBBB#0 (ana) - 0 👍 / 0 👎
3. CCCC#0 (ana) - 0 👍 / 0 👎
4. DDDD#0 (ana) - 0 👍 / 0 👎
```

이와 같이 영웅 설정 목록들이 뜰 것입니다.

만약 설정 목록이 길어서 여러 페이지로 나누어질 경우, :fontawesome-solid-arrow-left: 버튼과 :fontawesome-solid-arrow-right: 버튼으로 페이지를 탐색할 수 있습니다.

보려는 설정의 번호를 채팅창에 입력하면 봇이 설정 파일을 보여줍니다.
예시로, `1`을 입력하면 AAAA#0의 설정 파일을 보고 다운로드받을 수 있습니다.

👍 또는 👎 이모지 반응 버튼을 눌러 해당 사용자의 설정을 평가할 수 있습니다.

다운로드받은 설정 파일을 적용하는 방법에 대해서는 [설정 파일](index.md) 문서를 참고하세요.

## 내 설정 올리기 { #upload }

`/upload <영웅 이름>`을 채팅창에 쳐 놓은 상태에서 내 설정 파일을 끌어다 놓아 메시지에 첨부한 후 명령을 전송합니다.
