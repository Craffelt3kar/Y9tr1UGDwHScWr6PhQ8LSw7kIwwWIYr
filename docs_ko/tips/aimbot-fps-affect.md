---
description: 게임 fps(프레임률), 무조건 높다고, 무조건 낮다고 좋은 것이 아닙니다.
icon: svgrepo/fps-240
tips:
  - Aimbot
---

# 게임 fps와 에임봇 { #head }

## 게임 fps가 _과하게_ 높을 때 발생하는 문제점 { #fps-too-high }

1. 노블은 인터널 치트라서, 에임봇 사용 시 스무스 알고리즘이 _매 프레임마다_ 적용됩니다.  
    따라서, fps, 즉 초당 프레임 수가 증가하면 초당 스무스 알고리즘이 적용되는 횟수도 같이 증가하여, 결과적으로 **같은 설정값에서도 에임 속도가 빨라지는 등의 차이가 발생합니다.**  
    자세한 내용은 [아래 영상들을](#fps-affect-showcase) 참고하세요.

  * 만약 프레임 제한을 아예 풀어버리거나 과하게 높게 잡아서 게임 모드나 상황에 따라 fps가 요동칠 경우, 에임봇 스무스 속도도 같이 요동치게 됩니다.  
    예시로, 훈련장에서는 400fps, 사설방에서는 600fps, 빠른 대전에서는 200fps와 같이 게임 모드에 따라 프레임이 다르게 나올 경우  
    세 경우에 대해 모두 _따로_ 설정값들을 찾아서 각각 적용시켜 줘야 합니다.

2. 게임 fps가 너무 높아지면 스무스 알고리즘이 약간 맛이 갑니다.

  * 원래대로라면 '처음엔 천천히 가속 - 유지 - 목표에 가까워지면 감속' 사이클이 작동해야 하지만,  
    fps가 과하게 높아지면 '시작할 때 가속' 과정과 '목표에 가까워지면 감속' 과정이 거의 없어집니다.
    <figure><img src="../../../img/fps_smooth_graph.png" alt=""><figcaption><p>빨간색: fps가 적당할 때, 파란색: fps가 과하게 높을 때</p></figcaption></figure>
  * 하지만 발상을 전환해서, 이 점을 역으로 이용하여 게임 fps를 스무스 알고리즘을 세부적으로 트윅하는 수단으로서 사용할 수도 있습니다.  
    빠른 가속을 원한다면 fps 제한을 높게 걸거나 아예 fps 제한을 해제하고, 부드러운 가속 및 감속을 원한다면 fps 제한을 낮게 거는 식으로...

## 게임 fps가 _과하게_ 낮을 때 발생하는 문제점 { #fps-too-low }

fps가 낮을수록 에임봇이 부드러워지지만, 또 그렇다고 fps가 _너무_ 낮으면 에임봇이 목표를 조준한 뒤 정작 총을 쏘지 않는 상황이 발생할 수 있습니다.

이는 플릭 에임봇(과 캐스트 에임봇)에 연동된 오토샷(트리거봇)이 십자선에 목표가 닿았는지 검사하는 작업을 _매 프레임마다_ 수행하기 때문입니다.

이해를 돕기 위해 다음 예시를 봅시다:

<figure><img src="../../../img/fps_affect_lowfps_high.gif" alt=""><figcaption><p>게임 fps 가 적당할 때</p></figcaption></figure>

fps가 높을 때는 정상적으로 목표(푸른색 공)이 십자선(상자)에 닿고, 오토샷이 즉시 샷을 발사(상자가 초록색으로 변함)하는 것을 볼 수 있습니다.

<figure><img src="../../../img/fps_affect_lowfps_low.gif" alt=""><figcaption><p>게임 fps가 과하게 낮을 때</p></figcaption></figure>

fps가 과하게 낮을 경우, 목표가 십자선을 '지나갔지만', 실제로 십자선에 '닿은' 순간은 단 한번도 없었기에, 오토샷이 샷을 발사하지 않은 것을 볼 수 있습니다.

물론 이는 언제까지나 게임 fps가 과하게 낮을 때(약 60fps 미만) 아니면 발생하지 않는 문제입니다.

## 게임 fps와 스무스 설정값 { #fps-affect-showcase }

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/SuJoDBBa16Q" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/nJOn-OG3Aac" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>

<div class="video-wrapper">
  <iframe src="https://www.youtube.com/embed/Cv_aLxWWwlg" title="YouTube video player" frameborder="0" allowfullscreen></iframe>
</div>
