---
description: 노블 플릭(및 캐스트) 에임봇의 스무스 이해하기.
icon: fontawesome/solid/sliders
---

# 플릭 및 캐스트 에임봇 스무스 설정 팁 { #head }

!!! warning "아직 작성 중입니다."

## Classic, Fast 모드

플릭 또는 캐스트 에임봇에 대해서 고급 설정을 활성화했을 때
Smooth Mode를 Classic 또는 Fast 모드로 설정했다면 *Smooth*, *Smooth Accelerate*, *Smooth Balance* 세 가지 설정이 존재할 것입니다.

## Modern 모드

플릭 또는 캐스트 에임봇에 대해서 고급 설정을 활성화했을 때
Smooth Mode를 Modern 모드로 설정했다면 *Smooth*, *Smooth Accelerate* 두 가지 설정이 존재할 것입니다.
