---
description: 인젝트 도중, 또는 사용 도중 블루스크린이 뜨는 경우.
icon: svgrepo/bsod
tags:
  - Crash
  - BSOD
  - BlueScreen
  - Hang
---

# 블루스크린 { #head }

## 원인 1: 안티바이러스 또는 타사 안티 치트와의 충돌 { #anticheat }

노블의 본질을 생각해 보았을 때, 안티바이러스(백신) 또는 안티 치트와의 충돌은 어쩌면 당연한 것입니다.

다음 안티 치트들은 거의 항상 충돌을 일으키니, 반드시 제거해 주세요:

* EasyAntiCheat (EAC)
* BattlEye
* FaceIT Client
* ESEA
* Vanguard

## 원인 2: NobleShield { #nobleshield }

또 다른 원인으로는 NobleShield를 의심해 볼 수 있습니다.

해당 기능은 Windows 커널 모드에서 드라이버를 통해 게임이 노블을 감지하지 못하도록 하는 종합 우회 기능입니다.
하지만 기능 자체가 커널 모드에서 동작하기에, 조금이라도 잘못될 경우 블루스크린이 뜰 수 있습니다.

해당 기능은 Windows 20H2 이후 모든 버전, Windows 11 22H2 이하 모든 버전을 지원합니다.
사용 중인 Windows의 버전이 이 범위 밖에 있을 경우, NobleShield 사용 시 블루스크린 등의 오류가 발생할 수 있습니다.

비록 NobleShield 로드가 실패한 경우 노블 로더 측에서 블루스크린이 뜨지 않도록 조취를 취하긴 하지만, 이가 제대로 작동하지 않았을 수도 있습니다.

???+ tip "현재 Windows 버전 확인하기"
    사용 중인 Windows 버전은 Windows에 기본적으로 내장된 프로그램인 `winver.exe`를 실행시킴으로서 확인할 수 있습니다.

    실행 창(`Win+R`)을 띄운 후, `winver`을 입력하고 엔터 키를 누르면 됩니다.
